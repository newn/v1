 <?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../../includes/identity.php");

// Connect to database
include("../../includes/con_db.php");

// Get values for displaying texts on the page
include("../../includes/texts.php");

// Define person's access rights
include("../../includes/access.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>

<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $head_title ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Local css to this app -->
<link rel="stylesheet" href="local.css">
<!-- Bootstrap Toggle - http://www.bootstraptoggle.com -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
</head>
<body>

<!-- Primary Page Layout
================================================== -->

<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   	<a class="navmenu-brand" href="#"><?php echo $sidebar_brand ?></a>
     	<ul class="nav navmenu-nav">
		<?php
     		// Show the sidebar items
     		include("../../includes/sidebar-menu-items.php");
     		?>
     	</ul>
   </div>	

<!-- This is the main navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
       <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		<span class="sr-only">Toggle navigation</span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
       </div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
           <?php
		// Control the view of menu items for the logged person
		
		// Show the menu items
		include("../../includes/navbar-menu-items.php");
		
		?>
		</ul>
		<!-- Show user profile and logout option -->
		<ul class="nav navbar-nav navbar-right">
			<?php           
               // Show pending link if user is a student trainer
               if (in_array($grp_trainers, $groups)) {
               		echo "<li><a href='./'><span class='glyphicon glyphicon-folder-open'></span>&nbsp;&nbsp;".$nav_pending."</a></li>";
               }
               // Show the right menu items
               include("../../includes/navbar-right-menu-items.php");           
               ?>
		</ul>
	</div>
	</div>
   </nav>
<div  class="container">	
	<div class="row">
	<div class="col-xs-12 col-md-offset-3 col-md-6">
	<?php
	if ($gym_admin == "true" || $gym_manager == "true" || in_array($grp_trainers, $groups)) {
		// If form is completed by trainer for a user, insert to table
		if (isset($_POST['submit'])) {
			$equipments = implode(",", $_POST['equipments']);
			if (!empty($_POST['other_equip'])) { $other_equip = $_POST['other_equip']; } else { $other_equip = NULL; }
			$date = date("Y-m-d H:i:s");
			$med_clear = 0; // Initial value 0=No, 1=Yes
			$doc_letter = 0; // Initial value 0=NA
			$gymUserCrsid = $_POST['gymUserCrsid'];
			// Fetch values for the search keywords
			// Fetch details using the UniofCam lookup API
			$gymTrainer = file_get_contents("https://anonymous:@www.lookup.cam.ac.uk/api/v1/person/crsid/".$crsid."?format=json");
			$gymTrainer = json_decode($gymTrainer, TRUE);
			// Get the person's display name
			$gymTrainerName = $gymTrainer['result']['person']['displayName'];
			$searchKeywords = $_POST['gymUserName']." ".$_POST['gymUserCrsid']." ".$gymTrainerName." ".$crsid; // Insert keywords for searching
			if (isset($_POST['med_clear'])) { 
				$med_clear = $_POST['med_clear'];
				if (isset($_POST['doc_letter'])) { $doc_letter = $_POST['doc_letter']; } else { $doc_letter = 2; } // 0=NA, 1=Yes, 2=No
			}
			$query = "UPDATE gym_chklst SET tr_crsid= :tr_crsid, tr_name= :tr_name, med_clear= :med_clear, doc_letter= :doc_letter, equipments= :equipments, other_equip= :other_equip, completed= :completed, keywords= :keywords WHERE crsid= :crsid";
			$stmt = $db->prepare($query);
			$stmt->execute(array("tr_crsid" => $crsid,"tr_name" => $gymTrainerName,"med_clear" => $med_clear,"doc_letter" => $doc_letter,"equipments" => $equipments,"other_equip" => $other_equip,"completed" => $date,"keywords" => $searchKeywords,"crsid" => $gymUserCrsid));
		}
		// Display information if user already has a record
    	$query = "SELECT crsid,gym_assoc.name AS aname,gym_chklst.name AS uname,health_cond,other_cond FROM gym_chklst ";
    	$query .= "LEFT JOIN gym_assoc ON gym_assoc.id = gym_chklst.assoc ";
    	$query .= "WHERE completed IS NULL ORDER BY crsid ASC";
    	$stmt = $db->prepare($query);
    	$stmt->execute();
    	if ($stmt->rowCount() > 0) {
    		echo "<table class='table table-striped'>";
    		echo "<thead><tr><th>Name</th><th>Crsid</th><th>Status</th><th>&nbsp;</th></tr></thead>";
    		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    			/*$gymUserCrsid = $row['crsid'];
    			// Fetch details using the UniofCam lookup API
				$gymUser = file_get_contents("https://anonymous:@www.lookup.cam.ac.uk/api/v1/person/crsid/".$gymUserCrsid."?fetch=email&format=json");
				$gymUser = json_decode($gymUser, TRUE);
				// Get the person's display name
				$gymUserName = $gymUser['result']['person']['displayName'];
				// Get the gym_user's email
				$gymUserEmails = array();
				foreach($gymUser['result']['person']['attributes'] as $g_attr) {
				    if($g_attr['scheme'] == "email") {
				        $gymUserEmails[] = $g_attr['value'];
				    }
				}
				if (!empty($gymUserEmails)) { $gymUserEmail = $gymUserEmails[0]; } else { $gymUserEmail = $gymUserCrsid."@cam.ac.uk"; } */
    			echo "<tr><td><a href='mailto:".$row['crsid']."@cam.ac.uk?subject=Gym Induction'>".$row['uname']."</a></td><td>".$row['crsid']."</td><td>".$row['aname']."</td><td><button type='button' id='listBtn' class='btn btn-default btn-sm pull-right' data-displayname='".$row['uname']."' data-crsid='".$row['crsid']."' data-healthcond='".$row['health_cond']."' data-othercond='".$row['other_cond']."' data-toggle='modal' data-target='#pendingModal'>Open</button></td></tr>";
    		}
    		echo "</table>";
    	} else {
    		echo "<p>No pending items to show.</p>";
    	}
		?>
		<!-- Pending Modal -->
		<div class="modal fade" id="pendingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">		            
		            <form name="pendingForm" data-toggle="validator" method="post" action="<?php $_PHP_SELF; ?>" role="form">
		            <div class="modal-body">			                			                
		                <p><b><?php echo $pend_name ?>:</b> <span id="displayName"><!-- Name content here --></span></p>
		                <input type="hidden" id="gymUserCrsid" name="gymUserCrsid" value="">
		                <input type="hidden" id="gymUserName" name="gymUserName" value="">
		                <p><b><?php echo $dtl_health_cond ?>:</b> <span id="healthCond"></span></p>
		                <p><b><?php echo $dtl_other_cond ?>:</b> <span id="otherCond"></span></p>
		                <div class="form-group">
		                    <label class="checkbox-inline"><input type="checkbox" name="med_clear" value="1" data-toggle="toggle" data-on="Yes" data-off="No" data-size="small"> Medical clearance required?</label>			                    
		                </div>
		                <div class="form-group">
		                    <label class="checkbox-inline"><input type="checkbox" name="doc_letter" value="1" data-toggle="toggle" data-on="Yes" data-off="No" data-size="small"> If yes, is the doctor's letter given?</label>
		                </div>
		                <p><b><?php echo $pend_confirm ?></b></p>
		                <table class="table table-striped table-condensed">
		                <tbody>
		                <?php
						$query = 'SELECT * FROM gym_texts WHERE field LIKE "equip_%"';
						$stmt = $db->prepare($query);
						$stmt->execute();
						while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
							$field = $row['field'];
							echo "<tr><td><input type='checkbox' name='equipments[]' value='$field' data-toggle='toggle' data-on='Yes' data-off='No' data-size='small'></td><td>".$row['value']."</td></tr>";
							//echo "<div class='form-group'><label class='checkbox-inline'><input type='checkbox' name='equipments[]' value='$field' data-toggle='toggle' data-on='Yes' data-off='No' data-size='small'> ".$row['value']."</label></div>";
						}
						?>
						</tbody>
						</table>
						<div class="form-group"><textarea name="other_equip" rows="3" style="width: 100%;" placeholder="List other equipments here..."></textarea></div>
		                <div class="checkbox">
		                    <label><input type="checkbox" required /> <strong>Induction completed to satisfaction of the <span style="color: blue;">instructor</span>.</strong></label>
		                </div>
		                <div class="checkbox">
		                    <label><input type="checkbox" required /> <strong>Induction completed to satisfaction of the <span style="color: blue;">user</span>.</strong></label>
		                </div>			               			                
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
		                <button type="submit" id="submit" name="submit" class="btn btn-primary btn-sm">Submit</button>
		            </div>
		        	</form>
		        </div>
		    </div>
		</div>
		<script type="text/javascript">
		// Populate the pending modal with values
		$('.table').on('click','#listBtn',function(){
		    var displayname = $(this).data('displayname');
		    var crsid = $(this).data('crsid');
		    if ($(this).data('healthcond') == '') { var healthcond = "None"; }
		    else { var healthcond = $(this).data('healthcond'); }
		    if ($(this).data('othercond') == '') { var othercond = "None"; }
		    else { var othercond = $(this).data('othercond'); }
		    $("input#gymUserCrsid").val(crsid);
		    $("input#gymUserName").val(displayname);
		    $(".modal-body #displayName").html(displayname);
		    $(".modal-body #healthCond").html(healthcond);
		    $(".modal-body #otherCond").html(othercond);
		});
		</script>
	<?php
	}
	else {
		echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4>";
        echo "<br />";
        echo "<p>".$unauthorised_msg."</p>";
        echo "<br />";
        echo "<p><a href='/$paths[1]' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
	}
	?>
	</div>
	</div>     
</div>
<?php
// Close connections
$stmt = null;
$db = null;
?>
</body>
</html>