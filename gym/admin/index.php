 <?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../../includes/identity.php");

// Connect to database
include("../../includes/con_db.php");

// Get values for displaying texts on the page
include("../../includes/texts.php");

// Define person's access rights
include("../../includes/access.php");

// Close the db connection
mysqli_close($con);

	require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
    //Specfic table in database
    //$query = "SELECT * FROM gym_chklst";

    #the code for the class
    include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output

	#this one line of code is how you implement the class
	########################################################
	##

	$gym = new ajaxCRUD("Item", "gym_chklst", "id", "");

    ##
    ########################################################  
    $gym->defineRelationship("assoc", "gym_assoc", "id", "name");
    #the table fields have prefixes; i want to give the heading titles something more meaningful
    $gym->displayAs("id", "Details");
	$gym->displayAs("name", "Name");
    $gym->displayAs("tr_name", "Trainer");
    $gym->displayAs("assoc", "Status");
    $gym->displayAs("completed", "Completed");
    $gym->displayAs("keywords", "Search");

    #i could omit a field if I wanted
    $gym->omitField("crsid");
    $gym->omitField("tr_crsid");
    $gym->omitField("med_clear");
	$gym->omitField("doc_letter");
	$gym->omitField("health_cond");
	$gym->omitField("other_cond");
	$gym->omitField("equipments");
	$gym->omitField("other_equip");
	$gym->omitField("submitted");
    $gym->omitField("keywords");

    #i could disallow editing for certain, individual fields
    $gym->disallowEdit('id');
	$gym->disallowEdit('name');
    $gym->disallowEdit('tr_name');
    $gym->disallowEdit('completed');

    #i can order my table by whatever i want
    $gym->addOrderBy("ORDER BY id DESC");

    #i can use a where field to better-filter my table
    $gym->addWhereClause("WHERE (completed != '')");

    #i can disallow adding rows to the table
    $gym->disallowAdd();

    #set the number of rows to display (per page)
    $gym->setLimit(20);

	#set a filter box at the top of the table
    $gym->addAjaxFilterBox('keywords');

	#i can format the data in cells however I want with formatFieldWithFunction
	#this is arguably one of the most important (visual) functions
	$gym->formatFieldWithFunction('id', 'makeLink');
    //$gym->formatFieldWithFunction('crsid', 'displayName');
    //$gym->formatFieldWithFunction('trainer', 'displayTrainer');

	$gym->deleteText = "delete";
    
    # Conditional features based on user
    if ($gym_admin != "true") {
       $gym->disallowDelete();
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>

<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $head_title ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
</head>
<body>

<!-- Primary Page Layout
================================================== -->
	
<!-- This is the sidebar -->
    <div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
        <a class="navmenu-brand" href="../"><?php echo $sidebar_brand ?></a>
        <ul class="nav navmenu-nav">
            <?php
            // Show the sidebar items
            include("../../includes/sidebar-menu-items.php");
            ?>
        </ul>
    </div>
<!-- This is main the navbar -->
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
			<span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
            <?php
			// Control the view of menu items for the logged user
			
			// Show the menu items
            include("../../includes/navbar-menu-items.php");
			
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
                // Show the right menu items
                include("../../includes/navbar-right-menu-items.php");           
                ?>
			</ul>
		</div>
		</div>
    </nav>
	<div  class="container">	
		<div class="row">
			<div id="resultTbl" class="col-xs-12">
				<?php
				// my self-defined functions used for formatFieldWithFunction
                function makeLink($val) {
                    if (empty($val)) return "";
                        return "<button type='button' class='btn btn-link dtlBtn' data-id='$val' data-toggle='modal' data-target='#dtlModal'>View</button>";
                }
                /*function displayName($val) {
                    if (empty($val)) return "";
                        // Fetch details using the UniofCam lookup API
                        $gymUser = file_get_contents("https://anonymous:@www.lookup.cam.ac.uk/api/v1/person/crsid/".$val."?format=json");
                        $gymUser = json_decode($gymUser, TRUE);
                        // Get the person's display name
                        $gymUserName = $gymUser['result']['person']['displayName'];
                        return $gymUserName;
                }
                function displayTrainer($val) {
                    if (empty($val)) return "";
                        // Fetch details using the UniofCam lookup API
                        $gymTrainer = file_get_contents("https://anonymous:@www.lookup.cam.ac.uk/api/v1/person/crsid/".$val."?format=json");
                        $gymTrainer = json_decode($gymTrainer, TRUE);
                        // Get the person's display name
                        $gymTrainerName = $gymTrainer['result']['person']['displayName'];
                        return $gymTrainerName;
                }*/
                // actually show the table if user has access
				if ($gym_admin == "true" || $gym_manager == "true") {
                    
                $gym->showTable();

				# Output number of rows returned
				echo "Total returned rows: ";
				$gym->insertRowsReturned();
				echo "<br /><br />";

                }
                else {
                    echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4>";
                    echo "<br />";
                    echo "<p>".$unauthorised_msg."</p>";
                    echo "<br />";
                    echo "<p><a href='/$paths[1]' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
                }   
				?>	
			</div>
            <!-- Populate the details of entries to the modal -->
            <script type="text/javascript">                
                $('#resultTbl').on('click','.dtlBtn',function(){
                    var id = $(this).data('id');
                    $.ajax({
                        type: 'POST',
                        url: 'getDetails.php?id='+id,
                        success: function(data) {
                          $('.modal-body').html(data);
                        },
                        error:function(err){
                          alert("error"+JSON.stringify(err));
                        }
                    });
                });
            </script>
            <!-- Details Modal -->
            <div name="dtlModal" id="dtlModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body"><!-- content from getDetails.php goes here --></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    </div>    
                </div>
                </div>
            </div>
		</div>
		<br /><br />       
    </div>

</body>
</html>