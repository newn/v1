<?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../../includes/identity.php");

// Connect to database
include("../../includes/con_db.php");

// Get values for displaying texts on the page
include("../../includes/texts.php");

// Define person's access rights
include("../../includes/access.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Local css to this app -->
<link rel="stylesheet" href="../local.css">
<!-- Bootstrap Toggle - http://www.bootstraptoggle.com -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
</head>
<body>	
	<div class="row">
		<div class="col-xs-12">
			<?php
			//==================================================
			// Get the name of the current page to extract the id number to identify entry in mysql
            parse_str($_SERVER['QUERY_STRING']);
			// Show details
			if ($gym_admin == "true" || $gym_manager == "true") {
				?>
				<div class="table-responsive">
					<table class="table table-striped table-condensed">
						<tbody>
						<?php
						// Query table for result
						$query = 'SELECT * FROM gym_chklst WHERE id= :id';
						$stmt = $db->prepare($query);
						$stmt->execute(array("id" => $id));
						while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
							// Get gym user details
							$gymUser = file_get_contents("https://anonymous:@www.lookup.cam.ac.uk/api/v1/person/crsid/".$row['crsid']."?fetch=email&format=json");
                        	$gymUser = json_decode($gymUser, TRUE);
                        	// Get the gym_user's email
							$gymUserEmails = array();
							foreach($gymUser['result']['person']['attributes'] as $g_attr) {
							    if($g_attr['scheme'] == "email") {
							        $gymUserEmails[] = $g_attr['value'];
							    }
							}
							// Show the details
							echo "<tr><td id='title'>".$dtl_name.":</td><td><a href='mailto:$gymUserEmails[0]?subject=Gym query'>".$row['name']."</a></td></tr>";
							echo "<tr><td id='title'>".$dtl_crsid.":</td><td>".$row['crsid']."</td></tr>";
							echo "<tr><td id='title'>".$dtl_trainer.":</td><td>".$row['tr_name']."</td></tr>";
							if (!empty($row['health_cond'])) {
								echo "<tr><td id='title' colspan='2'>".$dtl_health_cond.":</td></tr>";
								$health_conds = explode(',', $row['health_cond']);
								foreach ($health_conds as $hc) {
									echo "<tr><td><input type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No' data-size='mini' checked disabled></td><td>".${$hc}."</td></tr>";
								}
							} else {
								echo "<tr><td id='title' colspan='2'>".$dtl_health_cond.":</td></tr>";
								echo "<tr><td colspan='2'>".$dtl_health_none."</td></tr>";
							}							
							if (!empty($row['other_cond'])) {
								echo "<tr><td id='title' colspan='2'>".$dtl_other_cond.":</td></tr>";
								echo "<tr><td colspan='2' style='word-wrap: break-word; width: 320px; white-space: normal;'>".$row['other_cond']."</td></tr>";
							}
							if ($row['med_clear'] == 0) { 
								$med_clear = "No";
								echo "<tr><td colspan='2'><span id='title'>".$dtl_med_clear.":</span> ".$med_clear."</td></tr>";
							} else { 
								$med_clear = "Yes";
								if ($row['doc_letter'] == 1) { $doc_letter = "Yes"; } else { $doc_letter = "No"; }
								echo "<tr><td colspan='2'><span id='title'>".$dtl_med_clear.":</span> ".$med_clear."</td></tr>";
								echo "<tr><td colspan='2'><span id='title'>".$dtl_doc_letter.":</span> ".$doc_letter."</td></tr>";
							}
							echo "<tr><td id='title' colspan='2'>".$dtl_equipments.":</td></tr>";
							//echo "<tr><td colspan='2' style='word-wrap: break-word; width: 320px; white-space: normal;'>";
							echo "<tr><td colspan='2'>";
							$equipments = explode(',', $row['equipments']);
							foreach ($equipments as $equip) {
								echo "- ".${$equip}."<br>";
							}
							echo "</td></tr>";
							if (!empty($row['other_equip'])) {
								echo "<tr><td id='title' colspan='2'>".$dtl_other_equip.":</td></tr>";
								echo "<tr><td colspan='2'>".$row['other_equip']."</td></tr>";
							}
							echo "<tr><td colspan='2'><span id='title'>".$dtl_submitted.":</span> ".$row['submitted']."</td></tr>";
							echo "<tr><td colspan='2'><span id='title'>".$dtl_completed.":</span> ".$row['completed']."</td></tr>";
						}
						?>
						</tbody>
					</table>
				</div>
			<?php
			}
			else {
				echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4>";
                   echo "<br />";
                   echo "<p>".$unauthorised_msg.".</p>";
                   echo "<br />";
                   echo "<p><a href='/$paths[1]' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
			}
			?>	
		</div>
	</div>    
</body>
</html>