 <?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../includes/identity.php");

// Connect to database
include("../includes/con_db.php");

// Get values for displaying texts on the page
include("../includes/texts.php");

// Define person's access rights
include("../includes/access.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>

<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $head_title ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- Local css to this app -->
<link rel="stylesheet" href="local.css">
<!-- Bootstrap Toggle - http://www.bootstraptoggle.com -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
</head>
<body>

<!-- Primary Page Layout
================================================== -->

<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   	<a class="navmenu-brand" href="#"><?php echo $sidebar_brand ?></a>
     	<ul class="nav navmenu-nav">
		<?php
     		// Show the sidebar items
     		include("../includes/sidebar-menu-items.php");
     		?>
     	</ul>
   </div>	

<!-- This is the main navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
       <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		<span class="sr-only">Toggle navigation</span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
       </div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
           <?php
		// Control the view of menu items for the logged person
		
		// Show the menu items
		include("../includes/navbar-menu-items.php");
		
		?>
		</ul>
		<!-- Show user profile and logout option -->
		<ul class="nav navbar-nav navbar-right">
			<?php           
               // Show pending link if user is a student trainer
				if (in_array($grp_trainers, $groups)) {
               		echo "<li><a href='pending/'><span class='glyphicon glyphicon-folder-open'></span>&nbsp;&nbsp;".$nav_pending."</a></li>";
            	}
               // Show the right menu items
               include("../includes/navbar-right-menu-items.php");           
               ?>
		</ul>
	</div>
	</div>
   </nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
		<?php
		// Display information if user already has a record
    	$query = "SELECT * FROM gym_chklst WHERE crsid = :crsid LIMIT 1";
    	$stmt = $db->prepare($query);
    	$stmt->execute(array(":crsid" => $crsid));
    	if ($stmt->rowCount() > 0) {
    		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    			$health_conds = explode(",", $row['health_cond']);
    			?>
    			<div class="col-xs-12 col-md-offset-3 col-md-6">
    				<p><strong><?php echo $frm_header ?>:</strong></p>
    				<table class="table table-striped">
    					<?php
    					$query_2 = 'SELECT * FROM gym_texts WHERE field LIKE "%infoq_%"';
						$stmt_2 = $db->prepare($query_2);
						$stmt_2->execute();
						while ($row_2 = $stmt_2->fetch(PDO::FETCH_ASSOC)) {
							if (in_array($row_2['field'], $health_conds)) {
								echo "<tr><td><input type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No' checked disabled></td><td>".$row_2['value']."</td></tr>";
							} else {
								echo "<tr><td><input type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No' disabled></td><td>".$row_2['value']."</td></tr>";
							}
						}
						if (!empty($row['other_cond'])) { echo "<tr><td colspan='2'><span id='title'>".$dtl_other_cond.":</span><br>".$row['other_cond']."</td></tr>"; }
						// If the induction is done and trainer has completed the form
						if (!empty($row['completed'])) {
							if ($row['med_clear'] == 0) {
								echo "<tr><td><input type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No' disabled></td><td>".$sum_med_clear."</td></tr>";
							} else {
								echo "<tr><td><input type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No' checked disabled></td><td>".$sum_med_clear."</td></tr>";
								if ($row['doc_letter'] == 1) {
									echo "<tr><td><input type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No' checked disabled></td><td>".$sum_doc_letter."</td></tr>";
								} else {
									echo "<tr><td><input type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No' disabled></td><td>".$sum_doc_letter."</td></tr>";
								}
							}
							echo "<tr><td id='title' colspan='2'>".$dtl_equipments.":</td></tr>";
							echo "<tr><td colspan='2' style='word-wrap: break-word; width: 320px; white-space: normal;'>";
							$equipments = explode(',', $row['equipments']);
							foreach ($equipments as $equip) {
								echo "- ".${$equip}."<br>";
							}
							echo "</td></tr>";
							if (!empty($row['other_equip'])) {
								echo "<tr><td colspan='2'><span id='title'>".$dtl_other_equip.":</span><br>".$row['other_equip']."</td></tr>";
							}
							echo "<tr><td><input type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No' checked disabled></td><td>Signed and completed by you and your trainer<br>on ".date('l, d M Y h:i:s',strtotime($row['completed']))."</td></tr>";
    					} else {
    						echo "<tr><td colspan='2'><div id='incomp' class='text-center'>".$dtl_form_incomplete."</div></td></tr>";
    					}
    					?>
    				</table>
    			</div>
    		<?php
    		}
    	}
		// If it's a new submission, insert to database
		elseif (isset($_POST['submit'])) {
			if (!empty($_POST['health_cond'])) { $health_cond = implode(",", $_POST['health_cond']); } else { $health_cond = ""; }
			// Fetch details using the UniofCam lookup API
			$gymUser = file_get_contents("https://anonymous:@www.lookup.cam.ac.uk/api/v1/person/crsid/".$crsid."?format=json");
			$gymUser = json_decode($gymUser, TRUE);
			// Get the person's display name
			$gymUserName = $gymUser['result']['person']['displayName'];
			// Insert values to table
			$query = "INSERT INTO gym_chklst (crsid,name,assoc,health_cond,other_cond) VALUES ( :crsid, :name, :assoc, :health_cond, :other_cond)";
			$stmt = $db->prepare($query);
			$stmt->execute(array("crsid" => $crsid,"name" => $gymUserName,"assoc" => $_POST['assoc'],"health_cond" => $health_cond,"other_cond" => $_POST['other_cond']));
			?>
			<!-- Modal -->
			<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-body">
			                <p><?php echo $frm_thankyou ?></p>
			                <p><b><?php echo $frm_confidential ?>.</b></p>
			            </div>
			            <div class="modal-footer">
			                <button id="reload" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			            </div>
			        </div>
			    </div>
			</div>
			<script type="text/javascript">
			$(document).ready(function () {
    			$('#confirmModal').modal('show');
    			// reload page
    			$('.modal-footer').on('click','#reload',function(){
  					window.location = window.location.pathname;
				})
			});
			</script>
		<?php	
		}
		else { ?>
			<form name="gymForm" data-toggle="validator" class="form-horizontal" method="post" action="<?php $_PHP_SELF; ?>" role="form">
			<div class="form-group form-group-lg form-inline">
				<div class="col-xs-12 col-md-offset-3 col-md-6">
					<p><?php echo $frm_form ?>.<br><b><?php echo $frm_confidential ?>.</b></p>
					<table class="table table-striped">
						<tbody>
						<?php
						$query = 'SELECT * FROM gym_texts WHERE field LIKE "%infoq_%"';
						$stmt = $db->prepare($query);
						$stmt->execute();
						while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
							$field = $row['field'];
							//echo "<tr><td><input type='checkbox' value='$field' required></td><td><input type='checkbox' value='no'></td><td>".$row['value']."</td></tr>";
							echo "<tr><td><input type='checkbox' name='health_cond[]' value='$field' data-toggle='toggle' data-on='Yes' data-off='No'></td><td>".$row['value']."</td></tr>";
						}
						?>
						<tr><td colspan="2">Please state:<br><textarea name="other_cond" rows="3" style="width: 100%;"></textarea></td></tr>
						<tr><td colspan="2">
							<strong>Choose your status below:</strong><br>
							<label class="radio-inline"><input type="radio" name="assoc" value="1" checked>Student Living in College (JCR)</label><br>
							<label class="radio-inline"><input type="radio" name="assoc" value="2">Student Living in College (MCR)</label><br>
							<label class="radio-inline"><input type="radio" name="assoc" value="3">Student Living Out of College (JCR)</label><br>
							<label class="radio-inline"><input type="radio" name="assoc" value="4">Student Living Out of College (MCR)</label><br>
							<label class="radio-inline"><input type="radio" name="assoc" value="5">Staff/Senior Member</label>
						</td></tr>
						</tbody>
					</table>
					<div><i><strong><?php echo $frm_ifyes ?>.</strong></i></div>
					<div id="warnNote" class="col-xs-12"><strong>WARNING:</strong>
						<br><?php echo $frm_warning ?>
					</div>
					<label class="checkbox-inline"><input type="checkbox" value="accept" required> <strong><?php echo $frm_accept ?>.</strong></label>
					<div id="submitBtn" class="text-center"><button type="submit" name="submit" id="submit" class="btn btn-primary active">Submit</button></div>
				</div>
			</div>
			</form>
		<?php
		}
		// Close connections
		$stmt_2 = null;
		$stmt = null;
		$db = null;
		?>
		</div>
	</div>
	<br /><br />       
    </div>
</body>
</html>