<?php
// Global variables to connect to databases
// Set the timezone
date_default_timezone_set("Europe/London");
// This should be edited to your institution database connection settings
$MYSQL_HOST = "localhost";
$MYSQL_LOGIN = "ofmsuser";
$MYSQL_PASS = "ofms243pass!";
$MYSQL_DB = "NEWN";

// This is the institution id from University lookup
$INST_ID = "NEWN";

// Define the institution admins, this group can edit the group privileges
$inst_admins = "newn-itdept";

// Dashboard headings, institution specific
$head_title = "Dashboard";
$banner = "Newnham College";
$sidebar_brand = "Newnham College";

// Default institution group
$default_inst_grp = "newn-members";

// Check if user is a member of the default institution group
$default_inst_grp_member = "";
foreach ($groups as $group) { if (strpos($default_inst_grp, $group) !== false) {$default_inst_grp_member = "true";} }
   
?>