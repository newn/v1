			<?php
            if (${$paths[2]."_admin"} == "true" || ${$paths[2]."_manager"} == "true") {
            echo "<li class='dropdown'>";
				echo "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><span class='glyphicon glyphicon-menu-hamburger'></span>&nbsp;&nbsp;Options <span class='caret'></span></a>";
				echo "<ul class='dropdown-menu' role='menu'>";
                // Available links depending on person
                if ($paths[2] == "srb" || $paths[2] == "shifts") {}
                else {
                    echo "<li><a href='".$url."/".$paths[1]."/".$paths[2]."/admin'><span class='glyphicon glyphicon-edit'></span>&nbsp;&nbsp;".$nav_manage."</a></li>";
                }
                if ($paths[2] == "parcels" || $paths[2] == "keys") {                        
                    echo "<li class='divider'></li>";
                    echo "<li><a href='".$url."/".$paths[1]."/".$paths[2]."/admin/archive.php'><span class='glyphicon glyphicon-list-alt'></span>&nbsp;&nbsp;".$nav_archive."</a></li>";                       
                }
                if ($paths[2] == "parcels") {                        
                    echo "<li class='divider'></li>";
                    echo "<li><a href='".$url."/".$paths[1]."/".$paths[2]."/sign.php'><i class='fa fa-pencil'></i>&nbsp;&nbsp;".$form_signed_for."</a></li>";                        
                }                
                if ($paths[2] == "keys") {                        
                    echo "<li class='divider'></li>";
                    echo "<li><a href='".$url."/".$paths[1]."/".$paths[2]."/admin/keylist.php'><i class='fa fa-key'></i>&nbsp;&nbsp;".$identify_keylist."</a></li>";
                    echo "<li class='divider'></li>";
                    echo "<li><a href='".$url."/".$paths[1]."/".$paths[2]."/admin/stafflist.php'><i class='fa fa-users'></i>&nbsp;&nbsp;".$identify_stafflist."</a></li>";                       
                }
                if ($paths[2] == "parking" || $paths[2] == "bike") {                        
                    echo "<li class='divider'></li>";
                    echo "<li><a href='".$url."/".$paths[1]."/export/?tbl=".$paths[2]."&path=".$paths[2]."'><span class='glyphicon glyphicon-export'></span>&nbsp;&nbsp;".$nav_export."</a></li>";                        
                }
                if ($paths[2] == "gym") {                        
                    echo "<li class='divider'></li>";
                    echo "<li><a href='".$url."/".$paths[1]."/".$paths[2]."/pending'><span class='glyphicon glyphicon-folder-open'></span>&nbsp;&nbsp;".$nav_pending."</a></li>";                        
                }
                if (${$paths[2]."_admin"} == "true") {                       
                    echo "<li class='divider'></li>";
                    echo "<li><a href='".$url."/".$paths[1]."/".$paths[2]."/admin/lang.php'><i class='fa fa-language'></i>&nbsp;&nbsp;Language</a></li>";                                     
                }
				echo "</ul>";
			echo "</li>";
            }
            ?>
            <li><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<?php echo $crsid ?></a></li>
            <li><a href="/logout.html"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Logout</a></li>