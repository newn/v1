<?php
// Get the path names
$paths = explode("/", $_SERVER['REQUEST_URI']);
// Set the root directory
$root = "//".$_SERVER['HTTP_HOST']."/".$paths[1];
?>
<!-- IE Compatibility Mode -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Mobile Specific Metas
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- FONT
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

<!-- CSS
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- Load customization for bootstrap -->
<?php echo "<link rel='stylesheet' href='".$root."/css/custom.css'>"; ?>

<!-- For the off-canvas sidebar -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
<?php echo "<link rel='stylesheet' href='".$root."/css/offcanvas_custom.css'>"; ?>

<!-- Theme for the people search results -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/flick/jquery-ui.css">

<!-- Font awesome for more icons and fonts -->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  
<!-- Favicon
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<?php echo "<link rel='icon' type='image/x-icon' href='".$root."/includes/favicon.ico'>"; ?>
  
<!-- Bootstrap core JavaScript
================================================== -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug 
<script src="//getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>-->

<!-- Validates form for required fields -->
<?php echo "<script type='text/javascript' src='".$root."/js/validator.js'></script>"; ?>

<!-- Functions for ajaxcrud -->
<?php echo "<script type='text/javascript' src='".$root."/ajaxcrud/javascript_functions.js'></script>"; ?>

<!-- For the off-canvas sidebar -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

<!-- For people search in both internal and uniofcam -->
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
<?php echo "<script type='text/javascript' src='".$root."/search/name_search.js'></script>"; ?>
<?php echo "<script type='text/javascript' src='".$root."/search/hide_search.js'></script>"; ?>
  
<!-- Initialize popovers
================================================== -->  
<script type="text/javascript">
	$(function () {
		$('[data-toggle="popover"]').popover();
	})
</script>