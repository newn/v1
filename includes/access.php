<?php
// Access rights definition
// First check if user already is on the user_details table, if not then insert details
$query = "SELECT crsid FROM user_details WHERE crsid = :crsid LIMIT 1";
$stmt = $db->prepare($query);
$stmt->execute(array(":crsid" => $crsid));
if ($stmt->rowCount() < 1) {
    if (!empty($emails[0])) { $email = $emails[0]; } else { $email = ""; }
    if (empty($displayName)) { $displayName = ""; }
    $query = "INSERT INTO user_details (crsid,name,email) VALUES ( :crsid, :name, :email)";
    $stmt = $db->prepare($query);
    $stmt->execute(array("crsid" => $crsid,"name" => $displayName,"email" => $email));
}
// Query table to fetch data and store all rows in array
$acc_stmt = $con->prepare("SELECT grp_name, lookup_grp FROM access_grps");
$acc_stmt->execute();
$acc_meta = $acc_stmt->result_metadata();

while ($field = $acc_meta->fetch_field()) {
  $acc_parameters[] = &$row[$field->name];
}
call_user_func_array(array($acc_stmt, 'bind_result'), $acc_parameters);
while ($acc_stmt->fetch()) {
  foreach($row as $key => $val) {
    $x[$key] = $val;
  }
  $grp_results[] = $x;
}
$acc_stmt->close();

// Match the user's lookup membership to the database table
$parcels_manager = "";
$parcels_admin = "";
foreach($grp_results as $grp) {
    if($grp['grp_name'] == "parcels_managers") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $parcels_manager = "true"; }
        }
    }
    if($grp['grp_name'] == "parcels_admins") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $parcels_admin = "true"; }
        }
    }
}
$keys_manager = "";
$keys_admin = "";
foreach($grp_results as $grp) {
    if($grp['grp_name'] == "keys_managers") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $keys_manager = "true"; }
        }
    }
    if($grp['grp_name'] == "keys_admins") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $keys_admin = "true"; }
        }
    }
}
$bike_manager = "";
$bike_admin = "";
foreach($grp_results as $grp) {
    if($grp['grp_name'] == "bike_managers") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $bike_manager = "true"; }
        }
    }
    if($grp['grp_name'] == "bike_admins") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $bike_admin = "true"; }
        }
    }
}
$parking_manager = "";
$parking_admin = "";
foreach($grp_results as $grp) {
    if($grp['grp_name'] == "parking_managers") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $parking_manager = "true"; }
        }
    }
    if($grp['grp_name'] == "parking_admins") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $parking_admin = "true"; }
        }
    }
}
$incident_manager = "";
$incident_admin = "";
foreach($grp_results as $grp) {
    if($grp['grp_name'] == "incident_managers") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $incident_manager = "true"; }
        }
    }
    if($grp['grp_name'] == "incident_admins") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $incident_admin = "true"; }
        }
    }
}
$accident_manager = "";
$accident_admin = "";
foreach($grp_results as $grp) {
    if($grp['grp_name'] == "accident_managers") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $accident_manager = "true"; }
        }
    }
    if($grp['grp_name'] == "accident_admins") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $accident_admin = "true"; }
        }
    }
}
$srb_manager = "";
$srb_admin = "";
foreach($grp_results as $grp) {
    if($grp['grp_name'] == "srb_managers") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $srb_manager = "true"; }
        }
    }
    if($grp['grp_name'] == "srb_admins") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $srb_admin = "true"; }
        }
    }
}
$shifts_manager = "";
$shifts_admin = "";
foreach($grp_results as $grp) {
    if($grp['grp_name'] == "shifts_managers") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $shifts_manager = "true"; }
        }
    }
    if($grp['grp_name'] == "shifts_admins") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $shifts_admin = "true"; }
        }
    }
}
$gym_manager = "";
$gym_admin = "";
foreach($grp_results as $grp) {
    if($grp['grp_name'] == "gym_managers") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $gym_manager = "true"; }
        }
    }
    if($grp['grp_name'] == "gym_admins") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $gym_admin = "true"; }
        }
    }
}
$callouts_manager = "";
$callouts_admin = "";
foreach($grp_results as $grp) {
    if($grp['grp_name'] == "callouts_managers") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $callouts_manager = "true"; }
        }
    }
    if($grp['grp_name'] == "callouts_admins") {
        foreach ($groups as $group) {
            if (strpos($grp['lookup_grp'], $group) !== false) { $callouts_admin = "true"; }
        }
    }
}

// Not part of ofms, just links to external apps
/*$sms_sender = "";
foreach ($groups as $group) {
    if ($group == "newn-sms-senders" || $group == "newn-itdept") { $sms_sender = "true"; }
}
$salto_user = "";
foreach ($groups as $group) {
    if ($group == "newn-porters" || $group == "newn-headporters" || $group == "newn-itdept") { $salto_user = "true"; }
}*/
?>