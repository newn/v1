			<?php			
			// Sidebar menu
            if (!empty($paths[2])) {
            	if (${$paths[2]."_admin"} == "true" || ${$paths[2]."_manager"} == "true") {
            	    if ($paths[3] !== "book") {
            	    	echo "<li><a href='".$url."/".$paths[1]."/".$paths[2]."/admin'><span class='glyphicon glyphicon-edit'></span>&nbsp;&nbsp;".$nav_manage."</a></li>";
            	    }
            		if ($paths[2] == "parcels" || $paths[2] == "keys") {
            	        echo "<li><a href='".$url."/".$paths[1]."/".$paths[2]."/admin/archive.php'><span class='glyphicon glyphicon-list-alt'></span>&nbsp;&nbsp;".$nav_archive."</a></li>";
            	    }
            	    if ($paths[2] == "parcels") {
            	        echo "<li><a href='".$url."/".$paths[1]."/".$paths[2]."/sign.php'><i class='fa fa-pencil'></i>&nbsp;&nbsp;".$form_signed_for."</a></li>";
            	    }
            	    if ($paths[2] == "gym") {
            	        echo "<li><a href='".$url."/".$paths[1]."/".$paths[2]."/pending'><span class='glyphicon glyphicon-folder-open'></span>&nbsp;&nbsp;".$nav_pending."</a></li>";
            	    }
            	}
            }
			echo "<li class='dropdown-header'><b>Menu:</b></li>";
			// Sidebar menu items
			echo "<li><a href='/".$paths[1]."'><span class='glyphicon glyphicon-dashboard'></span>&nbsp;&nbsp;Dashboard</a></li>";
			// Available links depending on person
			if ($keys_admin == "true" || $keys_manager == "true") {
				echo "<li><a href='/".$paths[1]."/keys'><i class='fa fa-key'></i>&nbsp;&nbsp;Keys Management</a></li>";
			}
			if ($parcels_admin == "true" || $parcels_manager == "true") {
				echo "<li><a href='/".$paths[1]."/parcels'><i class='fa fa-archive'></i>&nbsp;&nbsp;Parcels Management</a></li>";
			}
			if ($shifts_manager == "true" || $shifts_admin == "true") {
				echo "<li><a href='/".$paths[1]."/shifts' target='_blank'><i class='fa fa-calendar'></i>&nbsp;&nbsp;Porters Shift Schedule</a></li>";
			}
			if ($incident_manager == "true" || $incident_admin == "true") {
				echo "<li><a href='/".$paths[1]."/incident'><i class='fa fa-file-text'></i>&nbsp;&nbsp;Incident Reports</a></li>";
			}
			if ($callouts_manager == "true" || $callouts_admin == "true") {
				echo "<li><a href='/".$paths[1]."/callouts'><i class='fa fa-volume-control-phone'></i>&nbsp;&nbsp;Call-Out Reports</a></li>";
			}
			if ($student == "true" || $shifts_manager == "true" || $shifts_admin == "true") {
				echo "<li><a href='/".$paths[1]."/srb' target='_blank'><i class='fa fa-file-text'></i>&nbsp;&nbsp;Student Rooms Booking</a></li>";
			}
			// This link is visible to staff and students
			echo "<li><a href='/".$paths[1]."/bike'><i class='fa fa-bicycle'></i>&nbsp;&nbsp;Bike Registration</a></li>";
			// This link is visible to staff only
			if ($staff == "true") {
				echo "<li><a href='/".$paths[1]."/parking'><i class='fa fa-car'></i>&nbsp;&nbsp;Parking Management</a></li>";
			}
			if ($student == "true") {
				echo "<li><a href='http://itservices.newn.cam.ac.uk/formal-hall' target='_blank'><i class='fa fa-cutlery'></i>&nbsp;&nbsp;Formal Hall Booking</a></li>";
			}
			// This link is visible to staff only
			if ($staff == "true") {
				echo "<li><a href='http://apps.newn.cam.ac.uk/smfd'><i class='fa fa-cutlery'></i>&nbsp;&nbsp;SenMem Friday Dinners</a></li>";
			}
			if ($shifts_admin == "true") {
				echo "<li><a href='http://itservices.newn.cam.ac.uk/signage' target='_blank'><i class='fa fa-desktop'></i>&nbsp;&nbsp;Digital Signage System</a></li>";
			}
			if ($accident_manager == "true" || $accident_admin == "true") {
				echo "<li><a href='/".$paths[1]."/accident'><i class='fa fa-medkit'></i>&nbsp;&nbsp;Accident Reports</a></li>";
			}
			if ($student == "true" || $gym_manager == "true" || $gym_admin == "true") {
				echo "<li><a href='/".$paths[1]."/gym'><i class='fa fa-link'></i>&nbsp;&nbsp;Gym Users Checklist</a></li>";
			}
			?>
			<li class="dropdown-header"><b>Profile:</b></li>
            <li><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<?php echo $crsid ?></a></li>
            <li><a href="/logout.html"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Logout</a></li>



                