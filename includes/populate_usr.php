<?php
/* Section for testing
// Identify person accessing the page
require_once("identity.php");
// Connect to database
require_once("con_db.php");
// Define person's access rights
require_once("access.php");
$usr_crsid = "wc240";
echo $usr_crsid;
*/
$query = "SELECT crsid FROM user_details WHERE crsid = :crsid LIMIT 1";
$stmt = $db->prepare($query);
$stmt->execute(array("crsid" => $usr_crsid));
if ($stmt->rowCount() < 1) {
    $person = file_get_contents("https://anonymous:@www.lookup.cam.ac.uk/api/v1/person/crsid/".$usr_crsid."?fetch=all_attrs&format=json");
    $person = json_decode($person, TRUE);
    if (!empty($person['result']['person']['displayName'])) {
        $usr_displayName = $person['result']['person']['displayName'];
    }
    else {
        $usr_displayName = "";
    }
    $emails = array();
    foreach($person['result']['person']['attributes'] as $attr) {
        if($attr['scheme'] == "email") {
            $emails[] = $attr['value'];
        }
        elseif (!empty($emails)) { $email = $emails[0]; } else { $email = ""; }
    }
    $query2 = "INSERT INTO user_details (crsid,name,email) VALUES ( :crsid, :name, :email)";
    $stmt2 = $db->prepare($query2);
    $stmt2->execute(array("crsid" => $usr_crsid,"name" => $usr_displayName,"email" => $email));
}
?>