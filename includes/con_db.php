<?php
// Identify person accessing the page
include("conf.php");

// Connect if the user belongs to the institution using ofms, otherwise die
if($instid == $INST_ID || $collid == $INST_ID || $default_inst_grp_member == "true" ) {
  	// Do the actual database connection for the ofms
  	$con = mysqli_connect($MYSQL_HOST,$MYSQL_LOGIN,$MYSQL_PASS,$MYSQL_DB);
  	// Check connection
  	if (mysqli_connect_errno()) {
  	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  	}
  	// DB connection using PDO (this should be the preferred method)
	$dsn = "mysql:host=".$MYSQL_HOST.";dbname=".$MYSQL_DB;
  	try {
  		$db = new PDO($dsn, $MYSQL_LOGIN, $MYSQL_PASS);
  		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  	}
  	catch (PDOException $e) {
  		echo "Connection failed: ".$e->getMessage();
  	}
}
else {
  die("Sorry, you do not have access to use this web app.<br /><br />Please contact your system administrator.");
}
   
?>