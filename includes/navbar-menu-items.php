					
			<!-- Navbar menu items -->
            <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-th-large"></span>&nbsp;&nbsp;Your Apps <span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">                   
                <?php
                echo "<li><a href='/".$paths[1]."'><span class='glyphicon glyphicon-dashboard'></span>&nbsp;&nbsp;Dashboard</a></li>"; 
				// Available links depending on person
				if ($keys_admin == "true" || $keys_manager == "true") {
					echo "<li class='divider'></li>";
					echo "<li><a href='/".$paths[1]."/keys'><i class='fa fa-key'></i>&nbsp;&nbsp;Keys Management</a></li>";										
				}
				if ($parcels_admin == "true" || $parcels_manager == "true") {
					echo "<li class='divider'></li>";
					echo "<li><a href='/".$paths[1]."/parcels'><i class='fa fa-archive'></i>&nbsp;&nbsp;Parcels Management</a></li>";										
				}
				if ($shifts_manager == "true" || $shifts_admin == "true") {
					echo "<li class='divider'></li>";
					echo "<li><a href='/".$paths[1]."/shifts' target='_blank'><i class='fa fa-calendar'></i>&nbsp;&nbsp;Porters Shift Schedule</a></li>";										
				}
				if ($incident_manager == "true" || $incident_admin == "true") {
					echo "<li class='divider'></li>";
					echo "<li><a href='/".$paths[1]."/incident'><i class='fa fa-file-text'></i>&nbsp;&nbsp;Incident Reports</a></li>";										
				}
				if ($callouts_manager == "true" || $callouts_admin == "true") {
					echo "<li class='divider'></li>";
					echo "<li><a href='/".$paths[1]."/callouts'><i class='fa fa-volume-control-phone'></i>&nbsp;&nbsp;Call-Out Reports</a></li>";										
				}
				echo "<li class='divider'></li>";
				echo "<li><a href='/".$paths[1]."/bike'><i class='fa fa-bicycle'></i>&nbsp;&nbsp;Bike Registration</a></li>";
				if ($student == "true" || $shifts_manager == "true" || $shifts_admin == "true") {
					echo "<li class='divider'></li>";
					echo "<li><a href='/".$paths[1]."/srb' target='_blank'><i class='fa fa-calendar-check-o'></i>&nbsp;&nbsp;Student Rooms Booking</a></li>";										
				}
				if ($student == "true") {
					echo "<li class='divider'></li>";
					echo "<li><a href='http://itservices.newn.cam.ac.uk/formal-hall' target='_blank''><i class='fa fa-cutlery'></i>&nbsp;&nbsp;Formal Hall Booking</a></li>";										
				}									
				if ($staff == "true") {
					echo "<li class='divider'></li>";
					echo "<li><a href='/".$paths[1]."/parking'><i class='fa fa-car'></i>&nbsp;&nbsp;Parking Management</a></li>";									
				}
				if ($staff == "true") {
					echo "<li class='divider'></li>";
					echo "<li><a href='http://apps.newn.cam.ac.uk/smfd'><i class='fa fa-cutlery'></i>&nbsp;&nbsp;SenMem Friday Dinners</a></li>";									
				}
				if ($shifts_admin == "true") {
					echo "<li class='divider'></li>";
					echo "<li><a href='http://itservices.newn.cam.ac.uk/signage' target='_blank'><i class='fa fa-desktop'></i>&nbsp;&nbsp;Digital Signage</a></li>";										
				}
				if ($accident_manager == "true" || $accident_admin == "true") {
					echo "<li class='divider'></li>";
					echo "<li><a href='/".$paths[1]."/accident'><i class='fa fa-medkit'></i>&nbsp;&nbsp;Accident Reports</a></li>";										
				}
				if ($student == "true" || $gym_manager == "true" || $gym_admin == "true") {
					echo "<li class='divider'></li>";
					echo "<li><a href='/".$paths[1]."/gym'><i class='fa fa-link'></i>&nbsp;&nbsp;Gym Users Checklist</a></li>";										
				}
				?>
				</ul>
			</li>