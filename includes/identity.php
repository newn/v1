<?php
			
// NOTE: This only works if the parent directory is protected with Raven
// This is the file that fetches details about the user accessing the page

// Get crsid from raven session
if ($_SERVER['HTTP_HOST'] == "localhost") {
    //$crsid = "jtpo2"; // Newnham student
    $crsid = "ev243"; // Newnham staff
    //$crsid = "mp754"; // Student from an inst without account
    //$crsid = "fam32"; // Staff from another inst without account
    //$crsid = "amp41"; // Staff from another inst with account and admin
}
else { $crsid = $_SERVER['REMOTE_USER']; }

// Fetch details using the UniofCam lookup API
$person = file_get_contents("https://anonymous:@www.lookup.cam.ac.uk/api/v1/person/crsid/".$crsid."?fetch=all_attrs,all_groups&format=json");
$person = json_decode($person, TRUE);

// Get the person's display name
$displayName = $person['result']['person']['displayName'];

// Identify if user is staff or student
$staff = ""; if ($person['result']['person']['staff'] == "1") {$staff = "true";}
$student = ""; if ($person['result']['person']['student'] == "1") {$student = "true";}

// Get the person's email
$emails = array();
foreach($person['result']['person']['attributes'] as $attr) {
    if($attr['scheme'] == "email") {
    	$emails[] = $attr['value'];
    }
}

// Get the person's phone
$phones = array();
foreach($person['result']['person']['attributes'] as $attr) {
    if($attr['scheme'] == "universityPhone") {
    	$phones[] = $attr['value'];
    }
}

// Get the person's group memberships
$groups = array();
foreach($person['result']['person']['groups'] as $group) {
    $groups[] = $group['name'];
}

// Get the person's institution id
$instid = "";
foreach($person['result']['person']['attributes'] as $attr) {
    if($attr['scheme'] == "jdInstid") {
    	$instid = $attr['value'];
    }
}

// Get the person's college id
$collid = "";
foreach($person['result']['person']['attributes'] as $attr) {
    if($attr['scheme'] == "jdCollege") {
        $collid = $attr['value'];
    }
}

// Get the person's misAffiliation
$misaff = "";
foreach($person['result']['person']['attributes'] as $attr) {
    if($attr['scheme'] == "misAffiliation") {
        $misaff = $attr['value'];
    }
}

?>