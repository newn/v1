 /* Hides the input fields based on selected radio option */

$(document).ready(function(){
    $(".uni").hide();
    $(".inst").show();
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="inst"){
            $(".uni").hide();
            $("input[id='u_name']").val("");
            $("input[id='u_email']").val("");
            $("input[id='u_crsid']").val("");
            $(".inst").show();
        }
        if($(this).attr("value")=="uni"){
            $(".inst").hide();
            $("input[id='i_name']").val("");
            $("input[id='i_email']").val("");
            $("input[id='i_crsid']").val("");
            $(".uni").show();
        }
    });
});