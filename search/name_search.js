/* This autocomplete function shows suggestion of matches based on key entries */

$(function() {
    $("#i_name").autocomplete({
        /* source: "../search/autocomplete_inst.php", */
		source: "../search/autocomplete_inst.php",
        minLength: 1,
		select: function (event, ui) {
	        var item = ui.item;
	        if(item) {
                $("#i_email").val(item.email);
                $("#i_crsid").val(item.crsid);
                $("#i_phone").val(item.phone);
            }
        }
    })
});

$(function() {
    $("#u_name").autocomplete({
        source: "../search/autocomplete_uni.php",
		minLength: 1,
		select: function (event, ui) {
	        var item = ui.item;
	        if(item) {
                $("#u_email").val(item.u_email);
                $("#u_crsid").val(item.u_crsid);
                $("#u_phone").val(item.u_phone);
            }
         }
    })
});