<?php

// Search query string (replace with institution id)
$q=$_GET['term']."%20NEWN";

$return_arr = array();

$search = file_get_contents("https://anonymous:@www.lookup.cam.ac.uk/api/v1/person/search?query=".$q."&attributes=surname,registeredName,displayName,jdInstid,jdCollege&fetch=all_attrs&format=json");
$search = json_decode($search, TRUE);

foreach ($search['result']['people'] as $person) {   
    // Get the person's display name
    $name = $person['displayName'];
    // Get the person's crsid
    $crsid = $person['identifier']['value'];    
    // Get the person's main email
    $email = "";
    foreach($person['attributes'] as $attr) {
        if($attr['scheme'] == "email") {
            $email[] = $attr['value'];
        }
    }
    if(empty($email)) { $email = $crsid . "@cam.ac.uk"; } else {$email = $email[0];}
    // Get the person's phone
    $phone = "";
    foreach($person['attributes'] as $attr) {
        if($attr['scheme'] == "universityPhone") {
            $phone[] = $attr['value'];
        }
    }
    if(empty($phone)) { $phone = "None"; } else {$phone = $phone[0];}
    // Store values in a row
    $row_array['label'] = $name." (".$crsid.")";
    $row_array['value'] = $name;
    $row_array['email'] = $email;
    $row_array['crsid'] = $crsid;
    $row_array['phone'] = $phone;
    array_push( $return_arr, $row_array );

}

echo json_encode($return_arr),"\n";

?>