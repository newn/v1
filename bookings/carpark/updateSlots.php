<?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../../includes/identity.php");

// Connect to database
include("../../includes/con_db.php");

// Get values for displaying texts on the page
include("../../includes/texts.php");

// Define person's access rights
include("../../includes/access.php");

?>

<!DOCTYPE html>
<html lang="en">
</body>
<?php
// Get the query strings from url
parse_str($_SERVER['QUERY_STRING']);
// Insert values to table
if ($job == "insert") {
	// Get existing booked times to check for conflicts
	$query = 'SELECT * FROM bookings WHERE date_booked= :date_booked AND item_num= :item_num';
    $stmt = $db->prepare($query);
    $stmt->execute(array(":date_booked" => $date, ":item_num" => $item_num));
    if ($stmt->rowCount() > 0) {
    	$rows = $stmt->fetchAll();
    	$times_booked_arr = array(); // Booked times will be stored in this array
    	foreach ($rows as $row) {
    		$tTime = $row['time_booked'];
    		if ($row['duration'] == 0) {
    			$times_booked_arr[] = $tTime;
    		}
    		else {
    			$x = 0;
    			$q = $row['duration'];
    			while ($x <= $q) {
    				$times_booked_arr[] = $tTime;
    				$tTime = date('H:i:s', strtotime($tTime.'+30 minutes'));
    				$x++;
    			}
    		}
    	} 	
    }
    if (empty($times_booked_arr)) { $times_booked_arr[] = "24:00:00"; } // If query returns empty, just give the array an invalid time value
    // Get times to be booked
    $booking_times_arr = array(); // Times to be booked will be stored in this array
    if ($dur > 0) {
    	$x = 0;
    	$q = $dur;
    	$tTime = $time.":00";
    	while ($x <= $q) {
    		$booking_times_arr[] = $tTime;
    		$tTime = date('H:i:s', strtotime($tTime.'+30 minutes'));
    		$x++;
    	}
    }
    if ($dur == 0) {
    	$booking_times_arr[] = $time.":00";
    }
    $intersection = array_intersect($booking_times_arr, $times_booked_arr); // Compare the 2 arrays, if a value matches then booking will fail
    if (!empty($intersection)) {
    	echo "Sorry, the time you chose is not available.";
    }
    else {
    	// If checks are ok, insert the values
		$query = "INSERT INTO bookings (category,item_num,crsid,date_booked,time_booked,duration) VALUES ( :category, :item_num, :crsid, :date_booked, :time_booked, :duration)";
		$stmt = $db->prepare($query);
		$stmt->execute(array("category" => "carpark","item_num" => $item_num,"crsid" => $crsid,"date_booked" => $date,"time_booked" => $time,"duration" => $dur));
		// Echo success message
		echo "<img src='images/check.jpg' width='40px' hspace='10'> Your booking was successful!";
    }
}
// Delete values from table
if ($job == "delete") {
	$query = "DELETE FROM bookings WHERE date_booked = :date_booked AND time_booked = :time_booked AND item_num = :item_num AND duration = :duration";
	$stmt = $db->prepare($query);
	$stmt->execute(array(":date_booked" => $date, ":time_booked" => $time, ":item_num" => $item_num, ":duration" =>$dur));
}
// Close connections
$stmt = null;
$db = null;
?>
</body>
</html>