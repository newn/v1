<?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../../includes/identity.php");

// Connect to database
include("../../includes/con_db.php");

// Get values for displaying texts on the page
include("../../includes/texts.php");

// Define person's access rights
include("../../includes/access.php");

// Get query strings from url
parse_str($_SERVER['QUERY_STRING']);
// if date string from url is empty, then value is today
if (empty($date)) { $date = date('Y-m-d'); }

?>

<!DOCTYPE html>
<html lang="en">
<head>

<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $car_head_title ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Local css to this app -->
<link href="local.css" rel="stylesheet">
<!-- https://zertz.github.io/bootstrap-horizon/ -->
<link href="bootstrap-horizon.css" rel="stylesheet">
</head>
<body>
<!-- Primary Page Layout
================================================== -->

<!-- This is the sidebar -->
	<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   		<a class="navmenu-brand" href="#"><?php echo $car_sidebar_brand ?></a>
      	<ul class="nav navmenu-nav">
			<?php
      		// Show the sidebar items
      		include("../../includes/sidebar-menu-items.php");
      		?>
      	</ul>
    </div>	
	
	<!-- This is the main navbar -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
			<span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="./"><?php echo $car_banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
            <?php
			// Control the view of menu items for the logged person
			
			// Show the menu items
			include("../../includes/navbar-menu-items.php");
			
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
                // Show the right menu items
                include("../../includes/navbar-right-menu-items.php");           
                ?>
			</ul>
		</div>
		</div>
    </nav>
	<div  class="container">	
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 col-sm-5 col-md-3">
					<div class="form-group has-feedback" style="margin-bottom: 20px;">
    					<input type="text" class="form-control" name="start_time" id="start_time" value="<?php echo date('l, d M Y', strtotime($date)); ?>" readonly>
    					<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
  					</div>
  				</div>
  				<input type="hidden" name="alternate" id="alternate" value="<?php echo $date ?>">
				<script type="text/javascript">
					// http://jqueryui.com/datepicker
					$(function(){
						$("#start_time").datepicker({ 
							dateFormat: "DD, d M yy",
							altField: "#alternate",
      						altFormat: "yy-mm-dd",
      						minDate: 0
						});
						// Ajax call when page is loaded. NOTE: Disabled for now because it doesn't work in Chrome
						var value = $('#alternate').val();
					    $.ajax({
                	        type: 'POST',
                	        url: 'timeSlots.php?date='+value,
                	        success: function(data) {
                	         	$('.timeSlotBody').html(data);
                	        },
                	        beforeSend: function(){
					        	//show loading here
					        	$('#timeSlotBody').html("Loading...");
					    	},
                	        error:function(err){
                	         	alert("error"+JSON.stringify(err));
                	        }
                	    });
					});
					// Ajax call when date is changed
					$('#start_time').on("keyup change", function () {
                	    var value = $('#alternate').val();
                	    $.ajax({
                	        type: 'POST',
                	        url: 'timeSlots.php?date='+value,
                	        success: function(data) {
                	         	$('.timeSlotBody').html(data);
                	        },
                	        beforeSend: function(){
					        	//show loading here
					        	$('#timeSlotBody').html("Loading...");
					    	},
                	        error:function(err){
                	         	alert("error"+JSON.stringify(err));
                	        }
                	    });
                	});
				</script>
				<div class="timeSlotBody">
					<?php 
					//include('array_column.php'); // Needed this function for PHP versions below 5.5
					//include('initialSlot.php'); // Initial load of the time slots, will be overwritten after ajax call. NOTE: Ideally ajax call above but it's not working in Chrome ?>
					<!-- content from timeSlots.php goes here -->
				</div>
			</div>
		</div>
		<br /><br />       
    </div>

</body>
</html>