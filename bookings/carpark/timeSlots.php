<?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../../includes/identity.php");

// Connect to database
include("../../includes/con_db.php");

// Get values for displaying texts on the page
include("../../includes/texts.php");

// Define person's access rights
include("../../includes/access.php");

// Needed this function for PHP versions below 5.5
include("array_column.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Local css to this app -->
<link href="local.css" rel="stylesheet">
</head>
</body>
<?php
// Get query strings from url
parse_str($_SERVER['QUERY_STRING']);
// Define the start time if it's today
if ($date == date('Y-m-d')) { $start = date('H').":00"; } else { $start = "00:00"; }
// Define times
$end = "23:30";
$tStart = strtotime($start);
$tEnd = strtotime($end);
$tNow = $tStart;
// Define carpark number array
$carpark_nums = array('1','2','3','4');
// Loop through each carpark number to get times that are available or booked
foreach ($carpark_nums as $carpark_num) {
    ?>
    <div id="timeSlots" class="col-xs-12 cols-sm-12 col-md-12">
        <div id="car" class="col-xs-3 col-sm-1 col-md-1"><img src="<?php echo 'images/car'.$carpark_num.'.jpg' ?>"></div>
        <div id="times" class="row-horizon col-xs-12 col-sm-12 col-md-11">
            <?php
            // Query the database
            $query = 'SELECT * FROM bookings WHERE date_booked= :date_booked AND item_num= :item_num ORDER BY time_booked ASC';
            $stmt = $db->prepare($query);
            $stmt->execute(array(":date_booked" => $date, ":item_num" => $carpark_num));
            $rows = $stmt->fetchAll();            
            // If result returns rows, identify booked times
            if ($stmt->rowCount() > 0) {
                // Run through each time to see if it's available or booked
                while($tNow <= $tEnd) {                                       
                    $dur = 0;
                    $tTime = date("H:i",$tNow);
                    $found = array_search($tTime.":00", array_column($rows, 'time_booked')); // This determines if time is present on the results of the query
                    // If time is booked
                    if ($found !== false) {
                        foreach ($rows as $row) {
                            if ($tTime.":00" == $row['time_booked']) {
                                $dur = $row['duration'];
                                if ($crsid == $row['crsid']) {
                                    echo "<div class='col-xs-3 col-sm-2 col-md-1 tileMatch'><button type='button' class='btn btn-link' data-time='".$tTime."' data-num='".$carpark_num."' data-dur='".$dur."' data-toggle='modal' data-target='#cancelModal' style='padding: 5px;'>".$tTime."<br>Cancel</button></div>";
                                }
                                else { 
                                    echo "<div class='col-xs-3 col-sm-2 col-md-1 tileInactive'><button type='button' class='btn btn-link' data-time='".$tTime."' style='padding: 5px;'>".$tTime."<br>Booked</button></div>";
                                }
                            }
                        }
                    }
                    // If time is not booked
                    else {
                        echo "<div class='col-xs-3 col-sm-2 col-md-1 tileActive'><button type='button' class='btn btn-link' data-time='".$tTime."' data-num='".$carpark_num."' data-toggle='modal' data-target='#confirmModal' style='padding: 5px;'>".$tTime."<br>Book</button></div>";
                    }
                    // Set the duration before another loop
                    if ($dur == 0) {$dur = "30";} else {$dur = $dur * "60";}
                    $tNow = strtotime("+".$dur." minutes",$tNow);
                }         
            }
            // If result is empty make all times available
            else {
                while($tNow <= $tEnd){
                    $tTime = date("H:i",$tNow);
                    echo "<div class='col-xs-3 col-sm-2 col-md-1 tileActive'><button type='button' class='btn btn-link' data-time='".$tTime."' data-num='".$carpark_num."' data-toggle='modal' data-target='#confirmModal' style='padding: 5px;'>".$tTime."<br>Book</button></div>";
                    $tNow = strtotime('+30 minutes',$tNow);
                }
            }
            // Reset the current time for the next loop
            $tNow = $tStart;
            ?>
        </div>
    </div>
<?php
}
?>
<!-- Submit Modal -->
<div name="confirmModal" id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <div><strong>You want to book:</strong></div>
            <div>Carpark <span id="mNum"><!-- Asset number content goes here --></span></div>
            <div style="padding-bottom: 10px;"><span id="mDate"><!-- Date content goes here --></span> at <span id="mTime"><!-- Time content goes here --></span></div>
            <div>Duration: <select id="durSelect"><option value="0">30 minutes</option><option value="1">1 hour</option><option value="2">2 hours</option></select></div>
        </div>
        <div class="modal-footer">
            <button id="submit" type="button" class="btn btn-primary btn-sm">Submit</button><button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
        </div>    
    </div>
    </div>
</div>
<!-- Cancel Modal -->
<div name="cancelModal" id="cancelModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <div><strong>Cancel your booking:</strong></div>
            <div>Carpark <span id="mNum"><!-- Asset number content goes here --></span></div>
            <div style="padding-bottom: 10px;"><span id="mDate"><!-- Date content goes here --></span> at <span id="mTime"><!-- Time content goes here --></span></div>
            <div>Duration: <span id="mDur"><!-- Duration content goes here --></span></div>
            <input type="hidden" id="mDurVal" value="">
        </div>
        <div class="modal-footer">
            <button id="confirm" type="button" class="btn btn-danger btn-sm">Confirm</button><button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
        </div>    
    </div>
    </div>
</div>
<script type="text/javascript">
// Opens modals with booking details
// Available for bookings
$('.tileActive').on('click','.btn',function(){
    var time = $(this).data('time');
    var num = $(this).data('num');
    var date = "<?php echo date('l, d M Y', strtotime($date)); ?>";
    $(".modal-body #mDate").html(date);
    $(".modal-body #mTime").html(time);
    $(".modal-body #mNum").html(num);
});
// Booked times for cancellation
$('.tileMatch').on('click','.btn',function(){
    var time = $(this).data('time');
    var num = $(this).data('num');
    var dur = $(this).data('dur');
    var date = "<?php echo date('l, d M Y', strtotime($date)); ?>";
    $(".modal-body #mDate").html(date);
    $(".modal-body #mTime").html(time);
    $(".modal-body #mNum").html(num);
    if (dur == 0) {
        $(".modal-body #mDur").html('30 mins');
    } else {
        $(".modal-body #mDur").html(dur+' hour/s');
    }
    $("input#mDurVal").val(dur);
});
// Calls file with data values for db insert
$('.modal-footer').on('click','#submit',function(){
    var time = $("#mTime").html();
    var num = $("#mNum").html();
    var date = "<?php echo $date ?>";
    var dur = $("select#durSelect").val();
    $.ajax({
        type: 'POST',
        url: 'updateSlots.php?job=insert&date='+date+'&time='+time+'&item_num='+num+'&dur='+dur,
        success: function(data) {
            $('.modal-body').html(data);
            $('.modal-footer').html('<button id="reload" type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>');
        },
        error:function(err){
          alert("error"+JSON.stringify(err));
        }
    });
});
// Calls file with data values for db delete
$('.modal-footer').on('click','#confirm',function(){
    var time = $("#mTime").html();
    var num = $("#mNum").html();
    var date = "<?php echo $date ?>";
    var dur = $("input#mDurVal").val();
    $.ajax({
        type: 'POST',
        url: 'updateSlots.php?job=delete&date='+date+'&time='+time+'&item_num='+num+'&dur='+dur,
        success: function(data) {
            $('.modal-body').html('<img src="images/check.jpg" width="40px" align="left" hspace="10"> Your booking has now been cancelled!');
            $('.modal-footer').html('<button id="reload" type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>');
        },
        error:function(err){
          alert("error"+JSON.stringify(err));
        }
    });
});
// Reload the page to reflect the change on the booking
$('.modal-footer').on('click','#reload',function(){
//location.search = location.search.replace(/[^&$]*/i, 'date=<?php echo $date ?>');
var value = $('#alternate').val();
$.ajax({
    type: 'POST',
    url: 'timeSlots.php?date='+value,
    success: function(data) {
        $('.timeSlotBody').html(data);
    },
    beforeSend: function(){
        //show loading here
        $('#timeSlotBody').html("Loading...");
    },
    error:function(err){
        alert("error"+JSON.stringify(err));
    }
});
})
</script>
</body>
</html>