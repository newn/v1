<?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../../includes/identity.php");

// Connect to database
include("../../includes/con_db.php");

// Get values for displaying texts on the page
include("../../includes/texts.php");

// Define person's access rights
include("../../includes/access.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Local css to this app -->
<link href="local.css" rel="stylesheet">
</head>
</body>
<?php
// Get the name of the current page to extract the id number to identify entry in mysql
parse_str($_SERVER['QUERY_STRING']);
//
$times_booked = array();
$query = 'SELECT time_booked FROM bookings WHERE date_booked= :date_booked ORDER BY time_booked ASC';
$stmt = $db->prepare($query);
$stmt->bindValue(':date_booked', $date);
$stmt->execute();
$rows = $stmt->fetchAll();
foreach( $rows as $row) {
    $times_booked[] = $row['time_booked']; 
}
//
if ($date == date('Y-m-d')) { $start = date('H').":30"; } else { $start = "00:00"; }
$end = "23:30";

$tStart = strtotime($start);
$tEnd = strtotime($end);
$tNow = $tStart;
?>
<div class="col-md-12">
    <div class="col-md-1"><img src="car2.jpg" width="100%"></div>
    <div id="times" class="row-horizon col-md-11">
        <?php
        while($tNow <= $tEnd){
            $tTime = date("H:i",$tNow);
            if (in_array($tTime.":00", $times_booked)) {
                echo "<div class='col-md-1 tileInactive'><button type='button' class='btn btn-link' data-time='".$tTime."'>".$tTime."</button></div>";
            } else {
                echo "<div class='col-md-1 tileActive'><button type='button' class='btn btn-link' data-time='".$tTime."'>".$tTime."</button></div>";
            }
            $tNow = strtotime('+30 minutes',$tNow);
        }
        ?>
    </div>
</div>
<script type="text/javascript">
    $('#times').on('click','.btn',function(){
        var time = $(this).data('time');
        var date = "<?php echo $date ?>";
        $.ajax({
            type: 'POST',
            url: 'insertSlots.php?date='+date+'&time='+time,
            success: function(data) {
              $('.test-body').html(data);
            },
            error:function(err){
              alert("error"+JSON.stringify(err));
            }
        });
    });
</script>
</body>
</html>