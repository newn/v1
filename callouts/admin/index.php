<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Identify person accessing the page
include("../../includes/identity.php");
// Connect to database
include("../../includes/con_db.php");
// Get values for displaying texts on the page
include("../../includes/texts.php");
// Define person's access rights
include("../../includes/access.php");
// Close the db connection
mysqli_close($con);

require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
#the code for the class
include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
##
$callouts = new ajaxCRUD("Item", "callouts", "id", "");
##
########################################################
// Detect if mobile device, hide some column
if(!isset($_GET['screen'])) {
/* This code will be executed if screen resolution has not been detected.*/
    echo "<script language='JavaScript'>
    <!-- 
    document.location=\"$PHP_SELF?screen=done&w=\"+screen.width;
    //-->
    </script>";
}
else {    
    if(($_GET['w']<=480)) {
        $callouts->omitField("rep_crsid");
        $callouts->omitField("created");
    }
}
#i can define a relationship to another table
$callouts->defineRelationship("rep_crsid", "user_details", "crsid", "name");
$callouts->defineRelationship("oc_crsid", "user_details", "crsid", "name");
#the table fields have prefixes; i want to give the heading titles something more meaningful
$callouts->displayAs("id", "Callout No.");
$callouts->displayAs("rep_crsid", "Reported by");
$callouts->displayAs("subject", "Subject");
$callouts->displayAs("oc_crsid", "Person On-Call");
$callouts->displayAs("created", "Date");
#i could omit a field if I wanted
$callouts->omitField("details");
$callouts->omitField("hod_crsid");
#i could disallow editing for certain, individual fields
$callouts->disallowEdit('id');
$callouts->disallowEdit('rep_crsid');
$callouts->disallowEdit('subject');
$callouts->disallowEdit('oc_crsid');
$callouts->disallowEdit('created');
#i can order my table by whatever i want
$callouts->addOrderBy("ORDER BY created DESC");
#i can disallow adding rows to the table
$callouts->disallowAdd();
#set the number of rows to display (per page)
$callouts->setLimit($row_limit);
#set a filter box at the top of the table
$callouts->addAjaxFilterBox('subject');
#i can format the data in cells however I want with formatFieldWithFunction
#this is arguably one of the most important (visual) functions
$callouts->formatFieldWithFunction('id', 'makeLink');
$callouts->deleteText = "delete";
if ($callouts_admin !== "true") {
    #i can disallow deleting of rows from the table
    $callouts->disallowDelete();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
</head>
<body>
<!-- Primary Page Layout
================================================== -->   
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="../"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
        <?php
        // Control the view of menu items for the logged user       
        // Show the menu items
        include("../../includes/navbar-menu-items.php");       
        ?>
        </ul>
        <!-- Show user profile and logout option -->
        <ul class="nav navbar-nav navbar-right">
            <?php           
            // Show the right menu items
            include("../../includes/navbar-right-menu-items.php");           
            ?>
        </ul>
    </div>
    </div>
</nav>
<div  class="container">    
    <div class="row">
        <div class="col-xs-12">
            <?php
            // my self-defined functions used for formatFieldWithFunction
            function makeLink($val) {
                if (empty($val)) return "";
                    return "<a href='../report.php?id=$val'>$val</a>";
            }                
            // actually show the table if user has access
            if ($callouts_manager == "true" || $callouts_admin == "true") {                    
                $callouts->showTable();
                # Output number of rows returned
                echo "Total returned rows: ";
                $callouts->insertRowsReturned();
                echo "<br /><br />";
            }
            else {
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='/$paths[1]' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }   
            ?>  
        </div>
    </div>
    <br /><br />       
</div>
</body>
</html>