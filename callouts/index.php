<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Identify person accessing the page
require_once("../includes/identity.php");
// Connect to database
require_once("../includes/con_db.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
// Define person's access rights
require_once("../includes/access.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- Local css to this app -->
<link rel="stylesheet" href="local.css">
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   	<a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
		<?php
     	// Show the sidebar items
     	include("../includes/sidebar-menu-items.php");
     	?>
    </ul>
</div>	
<!-- This is the main navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
       <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
				<span class="sr-only">Toggle navigation</span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
       </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
    	    <?php
			// Control the view of menu items for the logged person
			// Show the menu items
			include("../includes/navbar-menu-items.php");			
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
    	        // Show the right menu items
    	        include("../includes/navbar-right-menu-items.php");           
    	        ?>
			</ul>
		</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<?php
			// Upload form values to database
			//==================================================
			if(isset($_POST['add'])) {
               	$datetime = date("Y-m-d H:i:s");
               	$query = "INSERT INTO callouts (rep_crsid,subject,oc_crsid,details,created) VALUES (:rep_crsid,:subject,:oc_crsid,:details,:created)";
				$stmt = $db->prepare($query);
				$stmt->execute(array("rep_crsid"=>$crsid,"subject"=>$_POST['subject'],"oc_crsid"=>$_POST['oc_crsid'],"details"=>$_POST['details'],"created"=>$datetime));
   				// Get id for email notification
				$query = "SELECT * FROM callouts WHERE created='$datetime' limit 1";
    			$stmt = $db->prepare($query);
    			$stmt->execute(array("created" => $datetime));
    			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    				$query2 ="SELECT name FROM user_details WHERE crsid = :crsid LIMIT 1";
    				$stmt2 = $db->prepare($query2);
    				$stmt2->execute(array("crsid" => $row['oc_crsid']));
    				while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
    					// Email notification
    					$subj = $notify_subj.": ".$row['subject'];
    					$message = "<b>".$callout_id.":</b> ".$row['id']."<br><b>".$notify_oc.":</b> ".$row2['name']."<br><b>".$callout_subj.":</b> ".$row['subject']."<br><br>Click <a href='".$url."/".$paths[1]."/".$paths[2]."/report.php?id=".$row['id']."'>here</a> to view and comment.";
    					$headers = 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						$headers .= "From:" . $notify_from . "\r\n";
						mail($notify_email,$subj,$message,$headers);
					}
    			}
				?>
				<!-- Show modal after submission -->
				<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-body">
				                <p><?php echo $callout_confirm ?></p>
				            </div>
				            <div class="modal-footer">
				                <button id="reload" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				            </div>
				        </div>
				    </div>
				</div>
				<script type="text/javascript">
				$(document).ready(function () {
   					$('#confirmModal').modal('show');
   					// reload page
   					$('.modal-footer').on('click','#reload',function(){
  						window.location = window.location.pathname;
					})
				});
				</script>
			<?php	
			}
			else {
				// Show the form if user is a member of staff
				if ($callouts_manager == "true" || $callouts_admin == "true") {
				?>
					<form name="myForm" data-toggle="validator" class="form-horizontal" method="post" action="<?php $_PHP_SELF; ?>" role="form">
					<div class="form-group form-group-lg">
						<label class="col-xs-12 col-sm-2 col-md-offset-1 col-md-2 control-label"><?php echo $form_oc ?>:</label>
						<div class="col-xs-12 col-sm-4 col-md-3">
						<?php
						$oc_array = explode(',', $oncall_group);
						echo '<select name="oc_crsid" class="form-control" required>';
						echo '<option value="" selected>Please select...</option>';
						foreach ($oc_array as $oc) {
							$query ="SELECT name FROM user_details WHERE crsid = :crsid LIMIT 1";
    						$stmt = $db->prepare($query);
    						$stmt->execute(array("crsid" => $oc));
    						while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
								echo '<option value="'.htmlspecialchars($oc).'">'. $row['name'] . "</option>\n";
							}
						}
						echo '</select>';
						?>
						</div>
					</div>
					<div class="form-group form-group-lg">
						<label class="col-xs-12 col-sm-2 col-md-offset-1 col-md-2 control-label"><?php echo $callout_subj ?>:</label>
						<div class=" col-xs-12 col-sm-9 col-md-4">
						<input type="text" name="subject" class="form-control" placeholder="<?php echo $callout_subj ?>" maxlength="100" required>
						</div>
					</div>
					<div class="form-group form-group-lg">
						<label class="col-xs-12 col-sm-2 col-md-offset-1 col-md-2 control-label"><?php echo $callout_details ?>:</label>
						<div class=" col-xs-12 col-sm-9 col-md-6">
						<textarea name="details" class="form-control" rows="6" style="height: auto;" placeholder="<?php echo $form_pl_details ?>" required></textarea>
						</div>
					</div>
					<div class="form-group form-group-lg">
						<label class="hidden-xs col-sm-2 col-md-offset-1 col-md-2 control-label">&nbsp;</label>
						<div class="col-xs-12 col-sm-10 col-md-9">
						<button type="submit" name="add" id="add" class="btn btn-primary active"><?php echo $form_submit ?></button>
						</div>
					</div>
					</form>
				<?php
				}
				else {
					// If user is not authorised, this message will appear
					echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                   	echo "<p><a href='../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
				}
			}
			?>
		</div>
	</div>
	<br /><br />       
</div>
</body>
</html>