<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Identify person accessing the page
require_once("../includes/identity.php");
// Connect to database
require_once("../includes/con_db.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
// Define person's access rights
require_once("../includes/access.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- Local css to this app -->
<link rel="stylesheet" href="local.css">
<!-- For the comments -->
<script type="text/javascript" src="comments.js"></script>
<script type="text/javascript">
$(function() {
    // This makes sure the search results display on top of the modal
    $("#i_name").autocomplete( "option", "appendTo", ".modal" );
});
</script>
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   	<a class="navmenu-brand" href="#"><?php echo $banner ?></a>
  	<ul class="nav navmenu-nav">
		<?php
  		// Show the sidebar items
  		include("../includes/sidebar-menu-items.php");
  		?>
  	</ul>
</div>	
<!-- This is the main navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		<span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
    </div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
        <?php
		// Control the view of menu items for the logged person		
		// Show the menu items
		include("../includes/navbar-menu-items.php");
		
		?>
		</ul>
		<!-- Show user profile and logout option -->
		<ul class="nav navbar-nav navbar-right">
			<?php           
            // Show the right menu items
            include("../includes/navbar-right-menu-items.php");           
            ?>
		</ul>
	</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12">
			<?php
			// Get query strings
            parse_str($_SERVER['QUERY_STRING']);
            // If user is added to access the report
			if(isset($_POST['add'])) {
				$usr_crsid = $_POST['crsid'];
                // First check if user is on the user_details table
                require_once("../includes/populate_usr.php");
				// Check for existing values in hod_crsid
                $query = "SELECT hod_crsid FROM callouts WHERE id= :id LIMIT 1";
                $stmt = $db->prepare($query);
                $stmt->execute(array("id" => $id));
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    if (!empty($row['hod_crsid'])) {
                        // Insert values to database
                        $query2 = "UPDATE callouts SET hod_crsid = CONCAT(hod_crsid,'".",".$usr_crsid."') WHERE id= :id";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("id" => $id));
                    }
                    else {
                        // Insert values to database
                        $query2 = "UPDATE callouts SET hod_crsid = '$usr_crsid' WHERE id= :id";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("id" => $id));
                    }
                }
           		// Fetch report subject
           		$query = "SELECT subject FROM callouts WHERE id= :id LIMIT 1";
           		$stmt = $db->prepare($query);
                $stmt->execute(array("id" => $id));
            	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $query2 ="SELECT * FROM user_details WHERE crsid = :crsid LIMIT 1";
                    $stmt2 = $db->prepare($query2);
                    $stmt2->execute(array("crsid" => $usr_crsid));
                    while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                        // Email added user
                        $subj = $notify_subj.": ".$row['subject'];
                        $message = $add_e_msg.":<br><br>Subject: <i>".$row['subject']."</i><br><br>".$add_e_link."<br><a href='".$url."/".$paths[1]."/".$paths[2]."/report.php?id=".$id."'>View report</a>";
                        $headers = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        $headers .= "From:" . $notify_from . "\r\n";
                        mail($row2['email'],$subj,$message,$headers);
                    }
            	}
			}
            // Query callouts table to display to page
            $query = "SELECT * FROM callouts WHERE id= :id LIMIT 1";
            $stmt = $db->prepare($query);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {                            	
            	if (strpos($row['hod_crsid'], $crsid) !== false || $callouts_manager == "true" || $callouts_admin == "true") {
            		?>
            		<div class="reportItems">
            			<div class="col-xs-12 col-sm-12 col-md-12 repRow">
            				<div class="col-xs-3 col-sm-2 col-md-2 repTitle"><?php echo $callout_id ?></div>
            				<div class="col-xs-9 col-sm-10 col-md-10 repValue"><?php echo $id ?></div>
            			</div>
            			<div class="col-xs-12 col-sm-12 col-md-12 repRow">
            				<div class="col-xs-3 col-sm-2 col-md-2 repTitle"><?php echo $callout_subj ?></div>
            				<div class="col-xs-9 col-sm-10 col-md-10 repValue"><?php echo $row['subject'] ?></div>
            			</div>
            			<div class="col-xs-12 col-sm-12 col-md-12 repRow">
            				<div class="col-xs-4 col-sm-2 col-md-2 repTitle"><?php echo $rep_by ?></div>
            				<div class="col-xs-8 col-sm-10 col-md-10 repValue">
                                <?php                                  
                                $query2 ="SELECT name FROM user_details WHERE crsid = :crsid LIMIT 1";
                                $stmt2 = $db->prepare($query2);
                                $stmt2->execute(array("crsid" => $row['rep_crsid']));
                                while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                    echo $row2['name']." <span style='font-size: 12px;' class='text-muted'> on ". $row['created']."</span>";
                                }
                                ?>
                            </div>
            			</div>
                        <div class="col-xs-12 col-sm-12 col-md-12 repRow">
                            <div class="col-xs-4 col-sm-2 col-md-2 repTitle"><?php echo $oncall_per ?></div>
                            <div class="col-xs-8 col-sm-10 col-md-10 repValue">
                                <?php                                  
                                $query2 ="SELECT name FROM user_details WHERE crsid = :crsid LIMIT 1";
                                $stmt2 = $db->prepare($query2);
                                $stmt2->execute(array("crsid" => $row['oc_crsid']));
                                while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                    echo $row2['name'];
                                }
                                ?>
                            </div>
                        </div>
            			<?php
                        if (!empty($row['hod_crsid']) || $callouts_admin == "true") {
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 repRow">
            				<div class="col-xs-4 col-sm-2 col-md-2 repTitle"><?php echo $rep_subto ?></div>
            				<div class="col-xs-8 col-sm-10 col-md-10 repValue">
            					<?php 
            					if (!empty($row['hod_crsid'])) {
                                    if (strpos($row['hod_crsid'], ",") !== false) {
                                        // Split the crsid of HoDs to an array
                                        $hod_crsid_arr = preg_split("/[\s,]+/", $row['hod_crsid']);
                                        foreach ($hod_crsid_arr as $hod_crsid) {
                                            $query2 ="SELECT name FROM user_details WHERE crsid = :crsid LIMIT 1";
                                            $stmt2 = $db->prepare($query2);
                                            $stmt2->execute(array("crsid" => $hod_crsid));
                                            while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                                $hod_names_arr[] = $row2['name'];
                                            }
                                        }
                                        $hod_names = implode(", ",$hod_names_arr);
                                    }
                                    else {
                                        $query2 ="SELECT name FROM user_details WHERE crsid = :crsid LIMIT 1";
                                        $stmt2 = $db->prepare($query2);
                                        $stmt2->execute(array("crsid" => $row['hod_crsid']));
                                        while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                            $hod_names = $row2['name'];
                                        }
                                    }
                                    // Display names
                                    if (!empty($hod_names)) { echo $hod_names." "; }
                                }
            					// Show add user link
                                if ($callouts_admin == "true") {
            						echo "[<a href='#' data-toggle='modal' data-target='#addModal'>add</a>]";
            					}
            					?>
            				</div>
            			</div>
            			<?php
                        }
                        ?>                      
            			<div class="col-xs-12 col-sm-12 col-md-12 repRow">
            				<div class="col-xs-12 col-sm-2 col-md-2 repTitle"><?php echo $callout_details ?></div>
            				<div class="col-xs-12 col-sm-10 col-md-10 repValue"><?php echo $row['details'] ?></div>
            			</div>
            		</div>
                    <!-- Start of pagination -->
                    <?php
                    if ($callouts_manager == "true" || $callouts_admin == "true") {
                        echo "<div id='page_nav' class='col-xs-12 col-sm-12 col-md-12'>";
                        $b_id = $id;
                        $query2 = "SELECT id FROM callouts WHERE id < :b_id ORDER BY id DESC LIMIT 1";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("b_id" => $b_id));
                        if ($stmt2->rowCount() > 0) {
                            while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                echo "<a href='./report.php?id=".$row2['id']."'><i class='fa fa-backward' aria-hidden='true'></i></a>";
                            }
                        }
                        else {
                            echo "<i class='fa fa-backward' aria-hidden='true'></i>";
                        }
                        //echo "&nbsp;&nbsp;";
                        echo "<a href='admin/'><i class='fa fa-home' aria-hidden='true'></i></a>";
                        $f_id = $id;
                        $query2 = "SELECT id FROM callouts WHERE id > :f_id ORDER BY id ASC LIMIT 1";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("f_id" => $f_id));
                        if ($stmt2->rowCount() > 0) {
                            while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                echo "<a href='./report.php?id=".$row2['id']."'><i class='fa fa-forward' aria-hidden='true'></i></a>";
                            }
                        }
                        else {
                            echo "<i class='fa fa-forward' aria-hidden='true'></i>";
                        }
                        echo "</div>";
                    }
                    ?>
                    <!-- End of pagination -->
            		<!-- Start add modal -->
            		<div id="addModal" class="modal fade" role="dialog">
  						<div class="modal-dialog modal-sm">
						<!-- Add modal content-->
							<div class="modal-content">
  								<div class="modal-header">
  									<button type="button" class="close" data-dismiss="modal">&times;</button>
  									<h4 class="modal-title">Add person</h4>
  								</div>
  								<div class="modal-body">
  								 <form class="form-inline" method="post" action="<?php $_PHP_SELF; ?>" role="form">                
                   					<div class="input-group">
                   					    <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
                   					    <input type="text" class="form-control" name="name" id="i_name" placeholder="Search for people">
                   					    <input type="hidden" name="crsid" id="i_crsid" class="form-control">    
                   					</div>                                  					
  								</div>
  								<div class="modal-footer">
  								 	<button type="submit" name="add" id="add" class="btn btn-default btn-sm">Add</button>
  								</div>
  								</form>
							</div>
  						</div>
					</div>
					<!-- End add modal -->						
					<div class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 20px;">
						<?php
						//Query table if there are comments
						$query2 = "SELECT * FROM callouts_comments WHERE page_id= :id ORDER BY date_ins ASC, hour_ins ASC";
						$stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("id" => $id));
						$nbrCom = $stmt2->rowCount();
						echo "<h4>Comments:</h4>";
						if ($nbrCom == 0) { // if 0 comment
							echo '<i>No comments at the moment!</i><br /><br />';
						}
						else {
							while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                $query3 = "SELECT name FROM user_details WHERE crsid = :crsid LIMIT 1";
                                $stmt3 = $db->prepare($query3);
                                $stmt3->execute(array("crsid" => $row2['crsid']));
                                while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<b>'.$row3['name'].'</b> <small class="text-muted">Posted on '.$row2['date_ins'].'</small>';
                                    echo '<br />';
                                    echo nl2br($row2['com_text']);
                                    echo '<hr></hr>';
                                }
							}
						}
						?>
						<!-- the comment posted will be displayed here -->
						<div id="newComment"></div>	
  						<!-- comment form -->
  						<form data-toggle="validator" action="#" method="post" role="form">
							<!-- hidden fields to pass the variables. The page_id corresponds to the id of the page -->
							<!-- This is useful if you add the comment system in a large number of pages so that only comments for that particular page shows up -->
							<input type="hidden" id="page_id" value="<?php echo $id ?>">
                            <input type="hidden" id="crsid" value="<?php echo $crsid ?>">
							<!--<input type="hidden" name="com_name" id="com_name" value="<?php echo $displayName ?>">
							<input type="hidden" name="com_email" id="com_email" value="<?php echo $emails[0] ?>">-->
							<div class="form-group">
								<label class="control-label">Post comment:</label>
								<div>
								<textarea name="com_text" id="com_text" class="form-control" rows="3" style="height: auto;" placeholder="Type your comment here...." required></textarea>
								</div>
							</div>
							<div class="form-group">
								<div>
								<button type="submit" name="submit" id="submit" class="submitComment btn btn-primary active">Submit</button>
								</div>
							</div>
						</form>
					</div>	
				<?php
            	}
            	else {
            		// If user is not authorised, this message will appear
					echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
               		echo "<p><a href='../' class='btn btn-primary active' role='button'>Dashboard</a></p>";
            	}
            }
			?>
		</div>
	</div>
	<br /><br />       
</div>
</body>
</html>