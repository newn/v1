// File js/tuto_comments.js
$(function() {
	//a function checking the validity of an email
	function validEmail(email) {
		var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
		return (email.match(r) == null) ? false : true;
	}
	
	$(".submitComment").click(function() {
		var com_name = $('#com_name').val(); // get name from comment name field
		var com_email = $('#com_email').val(); // get email from comment email field
		var com_text = $('#com_text').val(); // get text from comment text field
		var page_id = $('#page_id').val(); // get the page id from the hidden page_id field
		var dataFields = {'com_name': com_name, 'com_email': com_email, 'com_text': com_text, 'page_id': page_id}; // prepare datas string
		
		var checkName = $('#com_name').val().length; // get the length of the name field
		var checkEmail = validEmail(com_email); // check the email format with the above function validEmail()
		if(checkName < 3 || checkEmail == false || com_text=='') { // name must be at least 4 characters, email must be valid and text not empty (you may change all that)
			if (checkName < 3) { // if name below 4 characters
				$('#formGroupName').attr('class', 'form-group has-error'); // this is a Bootstrap CSS, I add has-error to the class so the field will turn red 
				$('#formGroupName span').text('minimum 4 characters'); // I add a help text in the <span> node
			} else {
				$('#formGroupName').attr('class', 'form-group has-success'); // this is a Bootstrap CSS, I add has-success to the class so the field will turn green 
				$('#formGroupName span').text(''); // remove help text in the <span> node
			}
			if (checkEmail == false) { // if email not valid
				$('#formGroupEmail').attr('class', 'form-group has-error'); // this is a Bootstrap CSS, I add has-error to the class so the field will turn red 
				$('#formGroupEmail span').text('wrong email format'); // I add a help text in the <span> node
			} else {
				$('#formGroupEmail').attr('class', 'form-group has-success'); // this is a Bootstrap CSS, I add has-success to the class so the field will turn green 
				$('#formGroupEmail span').text(''); // remove help text in the <span> node
			}
			if (com_text=='') {
				$('#formGroupText').attr('class', 'form-group has-error'); // this is a Bootstrap CSS, I add has-error to the class so the field will turn red 
				$('#formGroupText span').text('This field can\'t be empty'); // I add a help text in the <span> node
			} else {
				$('#formGroupText').attr('class', 'form-group has-success'); // this is a Bootstrap CSS, I add has-success to the class so the field will turn green 
				$('#formGroupText span').text(''); // remove help text in the <span> node
			}
		} else { // if everything valid
			$('#newComment').html('<img src="loader.gif" /> Processing...'); // loader image apprears in the <div id="newComment"></div>
			$.ajax({
				type: "POST",
				url: "comments.php", // call the php file ajax/tuto_blogcommentajax.php to insert new datas in the database
				data: dataFields, // send dataFields var
				timeout: 3000,
				success: function(dataBack){ // if success
					$('#newComment').html(dataBack); // return new datas and insert in the <div id="newComment"></div>
					$('#com_text').val(''); // clear the com_text field // I don't clear the name and email field if the guy wants to post another comment
					},
				error: function() { // if error
					$('#newComment').text('Problem!'); // display "Problem!" in the <div id="newComment"></div>
				}
			});
		}
		return false;
	});
});