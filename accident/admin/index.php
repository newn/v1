<?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../../includes/identity.php");

// Connect to database
include("../../includes/con_db.php");

// Get values for displaying texts on the page
include("../../includes/texts.php");

// Define person's access rights
include("../../includes/access.php");

// Close the db connection
mysqli_close($con);

    require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
    //Specific table in database
    //$query = "SELECT * FROM accident";

    #the code for the class
    include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output


    #this one line of code is how you implement the class
    ########################################################
    ##

    $tbl_accident = new ajaxCRUD("Item", "accident", "id", "");

    ##
    ########################################################

    // Detect if mobile device, hide some column
    if(!isset($_GET['screen'])) {
    /* This code will be executed if screen resolution has not been detected.*/
        echo "<script language='JavaScript'>
        <!-- 
        document.location=\"$PHP_SELF?screen=done&w=\"+screen.width;
        //-->
        </script>";
    }
    else {    
        if(($_GET['w']<=480)) {
            $tbl_accident->omitField("acc_where");
            $tbl_accident->omitField("acc_time");
        }
    }

    #the table fields have prefixes; i want to give the heading titles something more meaningful
    $tbl_accident->displayAs("id", "Accident No.");
    $tbl_accident->displayAs("acc_name", "Name ");
    $tbl_accident->displayAs("acc_where", "Place");
    $tbl_accident->displayAs("acc_date", "Date");
    $tbl_accident->displayAs("acc_time", "Time");

    #i could omit a field if I wanted
    #http://ajaxcrud.com/api/index.php?id=omitField
    $tbl_accident->omitField("acc_addr");
    $tbl_accident->omitField("acc_town");
    $tbl_accident->omitField("acc_postcode");
    $tbl_accident->omitField("acc_occ");
    $tbl_accident->omitField("acc_dept");
    $tbl_accident->omitField("other_name");
    $tbl_accident->omitField("other_addr");
    $tbl_accident->omitField("other_town");
    $tbl_accident->omitField("other_postcode");
    $tbl_accident->omitField("other_occ");
    $tbl_accident->omitField("other_dept");
    $tbl_accident->omitField("acc_how");
    $tbl_accident->omitField("acc_injury");
    $tbl_accident->omitField("acc_treatment");
    $tbl_accident->omitField("risk_ass");
    $tbl_accident->omitField("acc_riddor");
    $tbl_accident->omitField("riddor_date");
    $tbl_accident->omitField("add_crsid");
    $tbl_accident->omitField("created");

    #i could disallow editing for certain, individual fields
    $tbl_accident->disallowEdit('id');
    $tbl_accident->disallowEdit('acc_name');
    $tbl_accident->disallowEdit('acc_where');
    $tbl_accident->disallowEdit('acc_date');
    $tbl_accident->disallowEdit('acc_time');

    #i can order my table by whatever i want
    $tbl_accident->addOrderBy("ORDER BY created DESC");

    #i can disallow deleting of rows from the table
    #http://ajaxcrud.com/api/index.php?id=disallowDelete
    $tbl_accident->disallowDelete();

    #i can disallow adding rows to the table
    #http://ajaxcrud.com/api/index.php?id=disallowAdd
    $tbl_accident->disallowAdd();

    #add the sql where clause
    #$tbl_accident->addWhereClause("WHERE add_crsid LIKE '%$crsid%'");

    #set the number of rows to display (per page)
    $tbl_accident->setLimit(20);

    #set a filter box at the top of the table
    $tbl_accident->addAjaxFilterBox('acc_name');

	#i can format the data in cells however I want with formatFieldWithFunction
	#this is arguably one of the most important (visual) functions
	$tbl_accident->formatFieldWithFunction('id', 'makeLink');

	$tbl_accident->deleteText = "delete";

?>

<!DOCTYPE html>
<html lang="en">
<head>

<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $head_title ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Local css to this app -->
<link rel="stylesheet" href="../local.css">
<script type="text/javascript">
$(function() {
// This makes sure the search results display on top of the modal
$("#u_name").autocomplete( "option", "appendTo", ".modal" );
});
</script>
</head>
<body>

<!-- Primary Page Layout
================================================== -->
    
<!-- This is the sidebar -->
    <div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
        <a class="navmenu-brand" href="../"><?php echo $sidebar_brand ?></a>
        <ul class="nav navmenu-nav">
            <?php
            // Show the sidebar items
            include("../../includes/sidebar-menu-items.php");
            ?>
        </ul>
    </div>
<!-- This is main the navbar -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
            <?php
            // Control the view of menu items for the logged user
            
            // Show the menu items
            include("../../includes/navbar-menu-items.php");
            
            ?>
            </ul>
            <!-- Show user profile and logout option -->
            <ul class="nav navbar-nav navbar-right">
                <?php           
                // Show the right menu items
                include("../../includes/navbar-right-menu-items.php");           
                ?>
            </ul>
        </div>
        </div>
    </nav>
    <div  class="container">    
        <div class="row">
            <div id="resultTbl" class="col-xs-12">
                <?php
                // my self-defined functions used for formatFieldWithFunction
                function makeLink($val) {
                    if (empty($val)) return "";
                        return "<a href='../acc-report.php?id=$val'>$val</a>";
                }                
                // actually show the table if user has access
                if ($accident_manager == "true") {                    
                    $tbl_accident->showTable();
                    # Output number of rows returned
                    echo "Total returned rows: ";
                    $tbl_accident->insertRowsReturned();
                    echo "<br /><br />";
                }
                else {
                    echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;Unauthorised access!</h4>";
                    echo "<br />";
                    echo "<p>Sorry, you are not authorised to view this page.</p>";
                    echo "<br />";
                    echo "<p><a href='/' class='btn btn-primary active' role='button'>Dashboard</a></p>";
                }   
                ?>  
            </div>
        </div>
        <br /><br />       
    </div>

</body>
</html>