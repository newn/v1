<?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../includes/identity.php");

// Connect to database
include("../includes/con_db.php");

// Get values for displaying texts on the page
include("../includes/texts.php");

// Define person's access rights
include("../includes/access.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>

<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $head_title ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- Local css to this app -->
<link rel="stylesheet" href="local.css">
<!-- For the comments -->
<script type="text/javascript" src="comments.js"></script>
<script type="text/javascript">
$(function() {
    // This makes sure the search results display on top of the modal
    $("#i_name").autocomplete( "option", "appendTo", ".modal" );
});
</script>

</head>
<body>

<!-- Primary Page Layout
================================================== -->

<!-- This is the sidebar -->
	<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   		<a class="navmenu-brand" href="#"><?php echo $sidebar_brand ?></a>
      	<ul class="nav navmenu-nav">
			<?php
      		// Show the sidebar items
      		include("../includes/sidebar-menu-items.php");
      		?>
      	</ul>
    </div>	
	
	<!-- This is the main navbar -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
			<span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
            <?php
			// Control the view of menu items for the logged person
			
			// Show the menu items
			include("../includes/navbar-menu-items.php");
			
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
                // Show the right menu items
                include("../includes/navbar-right-menu-items.php");           
                ?>
			</ul>
		</div>
		</div>
    </nav>
	<div  class="container">	
		<div class="row">
			<div class="col-xs-12">
				<?php
				// Fetch query strings
                parse_str($_SERVER['QUERY_STRING']);
                // If user is added to access the accident report
				if(isset($_POST['add'])) {
					$addCrsid = $_POST['crsid'];
					// Check for existing values
                    $query = "SELECT add_crsid FROM accident where id = :id";
                    $stmt = $db->prepare($query);
                    $stmt->execute(array(":id" => $id));
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        if (isset($row['add_crsid'])) {
                            $query2 = "UPDATE accident SET add_crsid = CONCAT(add_crsid,'".",".$addCrsid."') WHERE id= :id";
                            $stmt2 = $db->prepare($query2);
                            $stmt2->execute(array(":id" => $id));                   
                        }
                        else {
                            $query2 = "UPDATE accident SET add_crsid = :add_crsid WHERE id= :id";
                            $stmt2 = $db->prepare($query2);
                            $stmt2->execute(array(":add_crsid" => $addCrsid, ":id" => $id));
                        }                     
                    }
					// Fetch email from crsid using Lookup API             		   
               	    $getEmail = file_get_contents("https://anonymous:@www.lookup.cam.ac.uk/api/v1/person/crsid/".$addCrsid."?fetch=email&format=json");
               	    $getEmail = json_decode($getEmail, TRUE);
               	    // Get the email addresses
					$addEmails = "";
					foreach($getEmail['result']['person']['attributes'] as $attr) {
					    if($attr['scheme'] == "email") {
					    	$addEmails[] = $attr['value'];
					    }
					}
               		// Fetch accident detail
               		$query = "SELECT acc_name FROM accident WHERE id= :id LIMIT 1";
               		$stmt = $db->prepare($query);
               		$stmt->bindValue(':id', $id);
					$stmt->execute();
                	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                		// Email the person added
                		$to = $addEmails[0];
                		$subj = "Accident Report: ".$row['acc_name'];
                		$message = $email_msg."<br><br>Person involved: <i>".$row['acc_name']."</i><br><br><a href='".$url."/".$paths[1]."/".$paths[2]."/acc-report.php?id=".$id."'>View report</a>";
                		$headers = 'MIME-Version: 1.0' . "\r\n";
                		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                		$headers .= "From:" . $acc_from . "\r\n";
                		//$headers .= "Cc:" . $cc . "\r\n";
                		mail($to,$subj,$message,$headers);
                	}
				}
                // Query accident table to display to page
                $query = "SELECT * FROM accident WHERE id= :id LIMIT 1";
                $stmt = $db->prepare($query);
				$stmt->bindValue(':id', $id);
				$stmt->execute();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {                            	
                	if (strpos($row['add_crsid'], $crsid) !== false || $accident_manager == "true") {
                		?>
                		<div class="reportItems">
                			<div class="col-xs-12 col-sm-12 col-md-12"><strong><?php echo $dtl_report ?></strong></div>
                            <div class="col-xs-3 col-sm-2 col-md-2 repTitle"><?php echo $dtl_report_id ?></div>
                            <div class="col-xs-9 col-sm-10 col-md-10 repValue"><?php echo $row['id'] ?></div>
                            <div class="col-xs-3 col-sm-2 col-md-2 repTitle"><?php echo $form_acc_name ?></div>
                            <div class="col-xs-9 col-sm-10 col-md-10 repValue"><?php echo $row['acc_name'] ?></div>
                            <div class="col-xs-3 col-sm-2 col-md-2 repTitle"><?php echo $form_acc_addr ?></div>
                            <div class="col-xs-9 col-sm-10 col-md-10 repValue"><?php echo $row['acc_addr'].", ".$row['acc_town'].", ".$row['acc_postcode'] ?></div>
                            <?php 
                            if(!empty($row['acc_occ'])) {
                                ?>
                                <div class="col-xs-3 col-sm-2 col-md-2 repTitle"><?php echo $form_acc_occ ?></div>
                                <div class="col-xs-9 col-sm-10 col-md-10 repValue"><?php echo $row['acc_occ'] ?></div>
                            <?php
                            }
                            if(!empty($row['acc_dept'])) {
                                ?>
                                <div class="col-xs-3 col-sm-2 col-md-2 repTitle"><?php echo $form_acc_dept ?></div>
                                <div class="col-xs-9 col-sm-10 col-md-10 repValue"><?php echo $row['acc_dept'] ?></div>
                            <?php
                            }
                            if(!empty($row['other_name'])) {
                                ?>
                                <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 10px;"><strong><?php echo $dtl_reported_by ?></strong></div>
                                <div class="col-xs-3 col-sm-2 col-md-2 repTitle"><?php echo $form_acc_name ?></div>
                                <div class="col-xs-9 col-sm-10 col-md-10 repValue"><?php echo $row['other_name'] ?></div>
                                <div class="col-xs-3 col-sm-2 col-md-2 repTitle"><?php echo $form_acc_addr ?></div>
                                <div class="col-xs-9 col-sm-10 col-md-10 repValue"><?php echo $row['other_addr'].", ".$row['other_town'].", ".$row['other_postcode'] ?></div>
                            <?php
                            }
                            if(!empty($row['other_occ'])) {
                                ?>
                                <div class="col-xs-3 col-sm-2 col-md-2 repTitle"><?php echo $form_acc_occ ?></div>
                                <div class="col-xs-9 col-sm-10 col-md-10 repValue"><?php echo $row['other_occ'] ?></div>
                            <?php
                            }
                            if(!empty($row['other_dept'])) {
                                ?>
                                <div class="col-xs-3 col-sm-2 col-md-2 repTitle"><?php echo $form_acc_dept ?></div>
                                <div class="col-xs-9 col-sm-10 col-md-10 repValue"><?php echo $row['other_dept'] ?></div>
                            <?php
                            }
                            ?>
                            <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 10px;"><strong><?php echo $form_acc_about ?></strong></div>
                            <div class="col-xs-3 col-sm-2 col-md-2 repTitle"><?php echo $dtl_datetime ?></div>
                            <div class="col-xs-9 col-sm-10 col-md-10 repValue"><?php echo date('d M Y', strtotime($row['acc_date']))." ".$row['acc_time'] ?></div>
                            <div class="col-xs-12 col-sm-12 col-md-2 repTitle"><?php echo $dtl_acc_where ?></div>
                            <div class="col-xs-12 col-sm-12 col-md-10 repValue"><?php echo $row['acc_where'] ?></div>
                            <div class="col-xs-12 col-sm-12 col-md-2 repTitle"><?php echo $dtl_acc_how ?></div>
                            <div class="col-xs-12 col-sm-12 col-md-10 repValue"><?php echo $row['acc_how'] ?></div>
                            <div class="col-xs-12 col-sm-12 col-md-2 repTitle"><?php echo $dtl_acc_injury ?></div>
                            <div class="col-xs-12 col-sm-12 col-md-10 repValue"><?php echo $row['acc_injury'] ?></div>
                            <?php
                            if(!empty($row['acc_treatment'])) {
                                ?>
                                <div class="col-xs-12 col-sm-12 col-md-2 repTitle"><?php echo $form_acc_treatment ?></div>
                                <div class="col-xs-12 col-sm-12 col-md-10 repValue"><?php echo $row['acc_treatment'] ?></div>
                            <?php
                            }
                            ?>
                            <div class="col-xs-12 col-sm-12 col-md-2 repTitle"><?php echo $dtl_risk_ass ?></div>
                            <div class="col-xs-12 col-sm-12 col-md-10 repValue"><?php if($row['risk_ass'] == 1) { echo "Yes"; } else { echo "No"; } ?></div>
                            <?php
                            if(!empty($row['acc_riddor'])) {
                                ?>
                                <div class="col-xs-12 col-sm-12 col-md-2 repTitle"><?php echo $dtl_acc_riddor ?></div>
                                <div class="col-xs-12 col-sm-12 col-md-10 repValue"><?php echo $row['acc_riddor']; if ($row['riddor_date'] !== "000-00-00") { echo "<br><br>Reported on: ".date('d M Y', strtotime($row['riddor_date'])); } ?></div>
                            <?php
                            }
                            ?>
                            <div class="col-xs-12 col-sm-2 col-md-2 repTitle"><?php echo $dtl_forwarded ?></div>
                            <div class="col-xs-12 col-sm-10 col-md-10 repValue">
                            <?php
                            // if add_crsid is not empty on the database then get their details, if enpty then value is "None"
                            if (!empty($row['add_crsid'])) {
                                // Split the crsid to an array
                                $add_crsid_arr = preg_split("/[\s,]+/", $row['add_crsid']);                         
                                // Get the display name for each of the crsid
                                $add_names_arr = "";
                                foreach ($add_crsid_arr as $add_crsid) {
                                    $getName = file_get_contents("https://anonymous:@www.lookup.cam.ac.uk/api/v1/person/crsid/".$add_crsid."?format=json");
                                    $getName = json_decode($getName, TRUE);
                                    $add_names_arr[] = $getName['result']['person']['displayName'];
                                }
                                // Combine the display names
                                $add_names = implode(", ",$add_names_arr);
                                // Echo the names
                                echo $add_names." ";
                                if ($accident_manager == "true") {
                                    echo "<a href='#' data-toggle='modal' data-target='#addModal'><i class='fa fa-plus-circle' aria-hidden='true'></i></a>";
                                }
                            }
                            else {
                                if ($accident_manager == "true") {
                                    echo "<a href='#' data-toggle='modal' data-target='#addModal'><i class='fa fa-plus-circle' aria-hidden='true'></i></a>";
                                }
                            }
                            ?>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 repTitle"><?php echo $dtl_submitted ?></div>
                            <div class="col-xs-12 col-sm-10 col-md-10 repValue"><?php echo date('d M Y H:i:s', strtotime($row['created'])) ?></div>
                		</div>
                        <!-- This is the pagination part -->
                        <?php
                        if ($accident_manager == "true") {
                            echo "<div id='page_nav' class='col-xs-12 col-sm-12 col-md-12'>";
                            $b_id = $id;
                            $query = "SELECT id FROM accident WHERE id < :b_id ORDER BY id DESC LIMIT 1";
                            $stmt = $db->prepare($query);
                            $stmt->bindValue(':b_id', $b_id);
                            $stmt->execute();
                            if ($stmt->rowCount() > 0) {
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    echo "<a href='./acc-report.php?id=".$row['id']."'><i class='fa fa-backward' aria-hidden='true'></i></a>";
                                }
                            }
                            else {
                                echo "<i class='fa fa-backward' aria-hidden='true'></i>";
                            }
                            //echo "&nbsp;&nbsp;";
                            echo "<a href='admin/'><i class='fa fa-home' aria-hidden='true'></i></a>";
                            $f_id = $id;
                            $query = "SELECT id FROM accident WHERE id > :f_id ORDER BY id ASC LIMIT 1";
                            $stmt = $db->prepare($query);
                            $stmt->bindValue(':f_id', $f_id);
                            $stmt->execute();
                            if ($stmt->rowCount() > 0) {
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    echo "<a href='./acc-report.php?id=".$row['id']."'><i class='fa fa-forward' aria-hidden='true'></i></a>";
                                }
                            }
                            else {
                                echo "<i class='fa fa-forward' aria-hidden='true'></i>";
                            }
                            echo "</div>";
                        }
                        ?>                       
                		<!-- Start add modal -->
                		<div id="addModal" class="modal fade" role="dialog">
  							<div class="modal-dialog modal-sm">
    						<!-- Add modal content-->
    							<div class="modal-content">
      								<div class="modal-header">
      									<button type="button" class="close" data-dismiss="modal">&times;</button>
      									<h4 class="modal-title">Add person</h4>
      								</div>
      								<div class="modal-body">
      								 <form class="form-inline" method="post" action="<?php $_PHP_SELF; ?>" role="form">                
                       					<div class="input-group">
                       					    <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
                       					    <input type="text" class="form-control" name="name" id="i_name" placeholder="Search for people">
                       					    <input type="hidden" name="crsid" id="i_crsid" class="form-control">    
                       					</div>                                  					
      								</div>
      								<div class="modal-footer">
      								 	<button type="submit" name="add" id="add" class="btn btn-default btn-sm">Add</button>
      								</div>
      								</form>
    							</div>
  							</div>
						</div>
						<!-- End add modal -->
                        <!-- Start comments section -->
                        <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 20px;">
                            <?php
                            //Query table if there are comments
                            $query1 = "SELECT * FROM accident_comments WHERE page_id= :id ORDER BY date_ins ASC, hour_ins ASC";
                            $stmt1 = $db->prepare($query1);
                            $stmt1->bindValue(':id', $id);
                            $stmt1->execute();
                            $nbrCom = $stmt1->rowCount();
                            echo "<h4>Comments:</h4>";
                            if ($nbrCom == 0) { // if 0 comment
                                echo '<i>No comments at the moment!</i><br /><br />';
                            }
                            else {
                                while ($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<b>'.$row['com_name'].'</b> <small class="text-muted">Posted on '.$row['date_ins'].'</small>';
                                    echo '<br />';
                                    echo nl2br($row['com_text']);
                                    echo '<hr></hr>';
                                }
                            }
                            ?>
                            <!-- the comment posted will be displayed here -->
                            <div id="newComment"></div>
        
                            <!-- comment form -->
                            <form data-toggle="validator" action="#" method="post" role="form">
                                <!-- hidden fields to pass the variables. The page_id corresponds to the id of the page -->
                                <!-- This is useful if you add the comment system in a large number of pages so that only comments for that particular page shows up -->
                                <input type="hidden" id="page_id" value="<?php echo $id ?>">
                                <input type="hidden" name="com_name" id="com_name" value="<?php echo $displayName ?>">
                                <input type="hidden" name="com_email" id="com_email" value="<?php echo $emails[0] ?>">
                                <div class="form-group">
                                    <label class="control-label">Post comment:</label>
                                    <div>
                                    <textarea name="com_text" id="com_text" class="form-control" rows="3" style="height: auto;" placeholder="Type your comment here...." required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div>
                                    <button type="submit" name="submit" id="submit" class="submitComment btn btn-primary active">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>	
					<?php
                	}
                	else {
                		// If user is not authorised, this message will appear
						echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4>";
                   		echo "<br />";
                   		echo "<p>".$unauthorised_msg."</p>";
                   		echo "<br />";
                   		echo "<p><a href='../' class='btn btn-primary active' role='button'>Dashboard</a></p>";
                	}
                }
                // Close the sql connection
                $stmt = null;
				$db = null;	
				?>
			</div>
		</div>
		<br /><br />       
    </div>

</body>
</html>