<?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../includes/identity.php");

// Connect to database
include("../includes/con_db.php");

// Get values for displaying texts on the page
include("../includes/texts.php");

// Define person's access rights
include("../includes/access.php");
 
if($_POST)
{
$com_name = strip_tags($_POST['com_name']); // get name
$com_email = strip_tags($_POST['com_email']); // get email
$com_text = strip_tags($_POST['com_text']);  // get text
$page_id = $_POST['page_id'];  // get the id of the current page
$date_ins = date("Y-m-d");
$hour_ins = date("H:i:s");
 
// Insert values to table
$query = "INSERT INTO accident_comments (com_name,com_email,com_text,page_id,date_ins,hour_ins) VALUES ( :com_name, :com_email, :com_text, :page_id, :date_ins, :hour_ins)";
$stmt = $db->prepare($query);
$stmt->execute(array("com_name" => $com_name,"com_email" => $com_email,"com_text" => $com_text,"page_id" => $page_id,"date_ins" => $date_ins,"hour_ins" => $hour_ins));

// Get users added for retrieving email from lookup
$query = "SELECT acc_name, add_crsid FROM accident WHERE id= :page_id";
$stmt = $db->prepare($query);
$stmt->bindValue(':page_id', $page_id);
$stmt->execute();
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $acc_name = $row['acc_name'];
    $add_crsid = $row['add_crsid'];
}
// Close the sql connection
$db = null;
// Split the crsid of users to an array
$add_crsid_arr = preg_split("/[\s,]+/", $add_crsid);
// Get the email from crsid
foreach ( $add_crsid_arr as $value ) {               		     
	$getEmail = file_get_contents("https://anonymous:@www.lookup.cam.ac.uk/api/v1/person/crsid/".$value."?fetch=email&format=json");
    $getEmail = json_decode($getEmail, TRUE);
	// Empty this array first
    $add_emails = "";
    foreach($getEmail['result']['person']['attributes'] as $attr) {
        if($attr['scheme'] == "email") {
            $add_emails[] = $attr['value'];
        }
    }
    // Store the crsids and emails in an array
    $add_emails_array[] = $add_emails[0]; 
}
$add_emails = implode(",",$add_emails_array);

// Email users for new post notification
$subj = $com_e_subj." - ".$acc_name;
$message = "<b>".$displayName."</b> ".$com_e_msg." ".$acc_name.".<br><br><i>&quot;".$com_text."&quot;</i><br><br><a href='".$url."/".$paths[1]."/".$paths[2]."/acc-report.php?id=$page_id'>Reply</a>";
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "From:" . $acc_from . "\r\n";
$headers .= "Bcc:".$com_e_cc.",".$add_emails."\r\n";
mail($com_e_to,$subj,$message,$headers);

}
 
echo '<div class="well well-sm">'; // return the new comment
echo "<b>$com_name</b> <small class='muted'>posted just now</small>";
echo "<br />";
echo nl2br($_POST['com_text']);
echo '</div>';
?>