<?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../includes/identity.php");

// Connect to database
include("../includes/con_db.php");

// Get values for displaying texts on the page
include("../includes/texts.php");

// Define person's access rights
include("../includes/access.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>

<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $head_title ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- Local css to this app -->
<link href="local.css" rel="stylesheet">
<!-- http://jonthornton.github.io/jquery-timepicker/ -->
<link href="timepicker/jquery.timepicker.css" rel="stylesheet">
<script src="timepicker/jquery.timepicker.js"></script>
</head>
<body>

<!-- Primary Page Layout
================================================== -->

<!-- This is the sidebar -->
	<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   		<a class="navmenu-brand" href="#"><?php echo $sidebar_brand ?></a>
      	<ul class="nav navmenu-nav">
			<?php
      		// Show the sidebar items
      		include("../includes/sidebar-menu-items.php");
      		?>
      	</ul>
    </div>	
	
	<!-- This is the main navbar -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
			<span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
            <?php
			// Control the view of menu items for the logged person
			
			// Show the menu items
			include("../includes/navbar-menu-items.php");
			
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
                // Show the right menu items
                include("../includes/navbar-right-menu-items.php");           
                ?>
			</ul>
		</div>
		</div>
    </nav>
	<div  class="container">	
		<div class="row">
			<div class="col-xs-12 col-md-offset-3 col-md-6">
				<?php
				// Upload form values to database
				//==================================================
				if(isset($_POST['submit'])) {
                	$cur_date = date("Y-m-d H:i:s");
                	$query = "INSERT INTO accident (acc_name,acc_addr,acc_town,acc_postcode,acc_occ,acc_dept,other_name,other_addr,other_town,other_postcode,other_occ,other_dept,acc_date,acc_time,acc_where,acc_how,acc_injury,acc_treatment,risk_ass,acc_riddor,riddor_date, created)
                	VALUES ( :acc_name, :acc_addr, :acc_town, :acc_postcode, :acc_occ, :acc_dept, :other_name, :other_addr, :other_town, :other_postcode, :other_occ, :other_dept, :acc_date, :acc_time, :acc_where, :acc_how, :acc_injury, :acc_treatment, :risk_ass, :acc_riddor, :riddor_date, :created)";
					$stmt = $db->prepare($query);
					$stmt->execute(array("acc_name" => $_POST['acc_name'], "acc_addr" => $_POST['acc_addr'], "acc_town" => $_POST['acc_town'], "acc_postcode" => $_POST['acc_postcode'], "acc_occ" => $_POST['acc_occ'], "acc_dept" => $_POST['acc_dept'], "other_name" => $_POST['other_name'], "other_addr" => $_POST['other_addr'], "other_town" => $_POST['other_town'], "other_postcode" => $_POST['other_postcode'], "other_occ" => $_POST['other_occ'], "other_dept" => $_POST['other_dept'], "acc_date" => $_POST['acc_date'], "acc_time" => $_POST['acc_time'], "acc_where" => $_POST['acc_where'], "acc_how" => $_POST['acc_how'], "acc_injury" => $_POST['acc_injury'], "acc_treatment" => $_POST['acc_treatment'], "risk_ass" => $_POST['risk_ass'], "acc_riddor" => $_POST['acc_riddor'], "riddor_date" => $_POST['riddor_date'], "created" => $cur_date));								
					// Email the accident managers
               		$query = "SELECT id, acc_name FROM accident WHERE created= :created LIMIT 1";
               		$stmt = $db->prepare($query);
               		$stmt->bindValue(':created', $cur_date);
					$stmt->execute();
                	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                		$subj = "Accident Report: ".$_POST['acc_name'];
                		$message = $email_msg_1."<br><br>Person involved: <i>".$_POST['acc_name']."</i><br><br><a href='".$url."/".$paths[1]."/".$paths[2]."/acc-report.php?id=".$row['id']."'>View report</a>";
                		$headers = 'MIME-Version: 1.0' . "\r\n";
                		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                		$headers .= "From:" . $acc_from . "\r\n";
                		//$headers .= "Cc:" . $cc . "\r\n";
                		mail($acc_email_to,$subj,$message,$headers);
                	}
					// Show success modal			
					echo "<script>
         				$(window).load(function(){
             				$('#successModal').modal('show');
         				});
    				</script>";
    				?>
    				<!-- Success Submission Modal -->
					<div name="successModal" id="successModal" class="modal fade" role="dialog">
					    <div class="modal-dialog modal-sm">
					    <div class="modal-content">
					        <div class="modal-body">
					            <!--<div style="margin-bottom: 10px;"><img src='images/check.jpg' width='30px' align='left' hspace='10'> Report submitted!</div>-->
					            <div class="media">
								  	<div class="media-left">
								    	<img class="media-object" src="images/check.jpg" width='30px'>
								  	</div>
								  	<div class="media-body">
								    	<h5 class="media-heading">Thank you!</h5>
								    	Report submitted successfully
								  	</div>
								</div>
					        </div>
					        <div class="modal-footer">
					            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
					        </div>    
					    </div>
					    </div>
					</div>
					<script type="text/javascript">
					// Reload page when modal is closed
					$('#successModal').on('hidden.bs.modal', function () {
						window.location.href = "./";
					})
					</script>
    			<?php	
				}
				else {
					// Show the form if user is a member of staff
					if ($accident_manager == "true") {
					?>
						<form name="myForm" data-toggle="validator" method="post" action="<?php $_PHP_SELF; ?>" role="form">
						<!-- Form for the person who had the accident -->
						<div id="details" class="col-xs-12"><strong><?php echo $form_acc_title ?>:</strong></div>
						<div class="col-md-8 form-group">
							<div><?php echo $form_acc_name ?>:</div>
							<input type="text" name="acc_name" class="form-control" placeholder="<?php echo $pl_acc_name ?>" maxlength="50" required>
						</div>
						<div class="col-md-12 form-group">
							<div><?php echo $form_acc_addr ?>:</div>
							<input type="text" name="acc_addr" class="form-control" placeholder="<?php echo $pl_acc_addr ?>" maxlength="50" required>
						</div>
						<div class="col-md-12 form-group">
							<div class="col-md-7 addr"><input type="text" name="acc_town" class="form-control" placeholder="<?php echo $pl_acc_town ?>" maxlength="20" required></div>
							<div class="col-md-3 addr"><input type="text" name="acc_postcode" class="form-control" placeholder="<?php echo $pl_acc_postcode ?>" maxlength="10" required></div>
						</div>
						<div class="col-md-6 form-group">
							<div><?php echo $form_acc_occ ?>:</div>
							<input type="text" name="acc_occ" class="form-control" placeholder="<?php echo $pl_acc_occ ?>" maxlength="50">
						</div>
						<div class="col-md-6 form-group">
							<div><?php echo $form_acc_dept ?>:</div>
							<input type="text" name="acc_dept" class="form-control" placeholder="<?php echo $pl_acc_dept ?>" maxlength="50">
						</div>
						<div class="col-xs-12 tickBox"><label class="checkbox-inline"><input type="checkbox" id="tickBox"> <i><strong><?php echo $form_acc_tick ?></strong></i></label></div>
						<!-- Form for the person who completed the form but not had the accident -->
						<div id="p_frm" style="display: none;">
							<div id="other_details" class="col-xs-12"><strong><?php echo $form_other_title ?>:</strong></div>
							<div class="col-md-8 form-group">
								<div><?php echo $form_acc_name ?>:</div>
								<div id="other_name"><input type="text" name="other_name" class="form-control" placeholder="<?php echo $pl_acc_name ?>" maxlength="50"></div>
							</div>
							<div class="col-md-12 form-group">
								<div><?php echo $form_acc_addr ?>:</div>
								<div id="other_addr"><input type="text" name="other_addr" class="form-control" placeholder="<?php echo $pl_acc_addr ?>" maxlength="50"></div>
							</div>
							<div class="col-md-12 form-group">
								<div id="other_town" class="col-md-7 addr"><input type="text" name="other_town" class="form-control" placeholder="<?php echo $pl_acc_town ?>" maxlength="20"></div>
								<div id="other_postcode" class="col-md-3 addr"><input type="text" name="other_postcode" class="form-control" placeholder="<?php echo $pl_acc_postcode ?>" maxlength="10"></div>
							</div>
							<div class="col-md-6 form-group">
								<div><?php echo $form_acc_occ ?>:</div>
								<input type="text" name="other_occ" class="form-control" placeholder="<?php echo $pl_acc_occ ?>" maxlength="50">
							</div>
							<div class="col-md-6 form-group">
								<div><?php echo $form_acc_dept ?>:</div>
								<input type="text" name="other_dept" class="form-control" placeholder="<?php echo $pl_acc_dept ?>" maxlength="50">
							</div>
						</div>
						<!-- Form about the accident -->
						<div id="about" class="col-xs-12"><strong><?php echo $form_acc_about ?>:</strong></div>
						<div class="col-md-6 form-group">
							<div><?php echo $form_acc_date ?>:</div>
							<input type="text" id="acc_date_picker" class="form-control" value="<?php echo date('l, d M Y', strtotime(date('Y-m-d'))); ?>">
							<input type="hidden" id="acc_date" name="acc_date" value="<?php echo date('Y-m-d'); ?>">
						</div>
						<div class="col-md-4 form-group">
							<div><?php echo $form_acc_time ?>:</div>
							<input type="text" id="acc_time" name="acc_time" class="form-control" value="<?php echo date('H:i'); ?>">
						</div>
						<div class="col-md-12 form-group">
							<div><?php echo $form_acc_where ?>:</div>
							<input type="text" name="acc_where" class="form-control" placeholder="<?php echo $pl_acc_where ?>" maxlength="100" required>
						</div>
						<div class="col-md-12 form-group">
							<div><?php echo $form_acc_how ?>:</div>
							<textarea name="acc_how" class="form-control" placeholder="<?php echo $pl_acc_how ?>" rows="4" required></textarea>
						</div>
						<div class="col-md-12 form-group">
							<div><?php echo $form_acc_injury ?>:</div>
							<textarea name="acc_injury" class="form-control" placeholder="<?php echo $pl_acc_injury ?>" rows="4" required></textarea>
						</div>
						<div class="col-md-12 form-group">
							<div><?php echo $form_acc_treatment ?>:</div>
							<textarea name="acc_treatment" class="form-control" placeholder="<?php echo $pl_acc_treatment ?>" rows="2"></textarea>
						</div>
						<input type="hidden" name="risk_ass" value="0"> <!-- This value will be sent if checkbox was not ticked -->
						<div class="col-xs-12 risk_ass"><label class="checkbox-inline"><input type="checkbox" name="risk_ass" value="1"> <?php echo $form_acc_risk_ass ?></label></div>
						<div class="col-md-12 form-group">
							<div><?php echo $form_acc_riddor ?>:</div>
							<textarea name="acc_riddor" class="form-control" placeholder="<?php echo $pl_acc_riddor ?>"></textarea>
						</div>
						<div class="col-md-6 form-group">
							<input type="text" id="riddor_date_picker" class="form-control" value="" placeholder="<?php echo $pl_riddor_date ?>">
							<input type="hidden" id="riddor_date" name="riddor_date">
						</div>
						<div class="col-xs-12 tickBox"><label class="checkbox-inline"><input type="checkbox" required> <strong><?php echo $form_tick_confirm ?></strong></label></div>
						<div class="col-xs-12 text-center"><button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button></div>
						</form>
						<script type="text/javascript">
						// Initiate the date and time pickers
						$(function(){
							// http://jqueryui.com/datepicker
							$("#acc_date_picker").datepicker({ 
								dateFormat: "DD, d M yy",
								altField: "#acc_date",
      							altFormat: "yy-mm-dd",
							});
							$("#riddor_date_picker").datepicker({ 
								dateFormat: "DD, d M yy",
								altField: "#riddor_date",
      							altFormat: "yy-mm-dd",
							});
							//http://jonthornton.github.io/jquery-timepicker
							$('#acc_time').timepicker({ 
								'scrollDefault': 'now',
								'timeFormat': 'H:i',
								'step': 15 
							});
						});
						// Tickbox behaviour for person reporting details
						$("input#tickBox").click(function(){
						    // Show or hide person details
						    $("#p_frm").toggle();
						    // Some fields need to be required
						    var pl_acc_name = "<?php echo $pl_acc_name ?>";
						    var pl_acc_addr = "<?php echo $pl_acc_addr ?>";
						    var pl_acc_town = "<?php echo $pl_acc_town ?>";
						    var pl_acc_postcode = "<?php echo $pl_acc_postcode ?>";
						    if($("input#tickBox").is(":checked")) {
						        // When the checkbox is checked
						        $("#other_name").html("<input type='text' id='other_name' name='other_name' class='form-control' placeholder='"+pl_acc_name+"' maxlength='50' required>");
						        $("#other_addr").html("<input type='text' name='other_addr' class='form-control' placeholder='"+pl_acc_addr+"' maxlength='50' required>");
						        $("#other_town").html("<input type='text' name='other_town' class='form-control' placeholder='"+pl_acc_town+"' maxlength='20' required>");
						        $("#other_postcode").html("<input type='text' name='other_postcode' class='form-control' placeholder='"+pl_acc_postcode+"' maxlength='10' required>");
						        
						    }
						    else {
						        // When the checkbox is not checked
						        $("#other_name").html("<input type='text' id='other_name' name='other_name' class='form-control' placeholder="+pl_acc_name+" maxlength='50'>");
						        $("#other_addr").html("<input type='text' name='other_addr' class='form-control' placeholder="+pl_acc_addr+" maxlength='50'>");
						        $("#other_town").html("<input type='text' name='other_town' class='form-control' placeholder='"+pl_acc_town+"' maxlength='20'>");
						        $("#other_postcode").html("<input type='text' name='other_postcode' class='form-control' placeholder='"+pl_acc_postcode+"' maxlength='10'>");
						    }
						});
						</script>
					<?php
					}
					else {
						// If user is not authorised, this message will appear
						echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4>";
                    	echo "<br />";
                    	echo "<p>".$unauthorised_msg."</p>";
                    	echo "<br />";
                    	echo "<p><a href='../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
					}
				}
				?>
			</div>
		</div>
		<br /><br />       
    </div>

</body>
</html>