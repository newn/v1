 <?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("includes/identity.php");

// Connect to database
include("includes/con_db.php");

// Get values for displaying texts on the page
//include("includes/texts.php");

// Define person's access rights
include("includes/access.php");

// Close the db connection
mysqli_close($con);

	require_once("ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
    //Specfic table in database
    $query = "SELECT * FROM access_grps";

    #the code for the class
    include ("ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output

	#this one line of code is how you implement the class
	########################################################
	##

	$tbl_access_grps = new ajaxCRUD("Item", "access_grps", "id", "");

    ##
    ########################################################  

    $tbl_access_grps->omitPrimaryKey();
    
    #the table fields have prefixes; i want to give the heading titles something more meaningful
    $tbl_access_grps->displayAs("grp_name", "Access Groups");
	$tbl_access_grps->displayAs("lookup_grp", "Lookup Groups");

    #i could disallow editing for certain, individual fields
    $tbl_access_grps->disallowEdit('grp_name');

    #set the number of rows to display (per page)
    $tbl_access_grps->setLimit(100);

    #disallow adding of rows
    $tbl_access_grps->disallowAdd();

	#disallow deleting of rows
    $tbl_access_grps->disallowDelete();

?>

<!DOCTYPE html>
<html lang="en">
<head>

<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title>Groups Management</title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("includes/headers.php");
?>
</head>
<body>

<!-- Primary Page Layout
================================================== -->
	
<!-- This is the sidebar -->
    <div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
        <a class="navmenu-brand" href="#"><?php echo $sidebar_brand ?></a>
        <ul class="nav navmenu-nav">
            <?php
            // Show the sidebar items
            include("includes/sidebar-menu-items.php");
            ?>
        </ul>
    </div>
<!-- This is main the navbar -->
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
			<span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
            <?php
			// Control the view of menu items for the logged user
			
			// Show the menu items
            include("includes/navbar-menu-items.php");
			
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
                // Show the right menu items
                include("includes/navbar-right-menu-items.php");           
                ?>
			</ul>
		</div>
		</div>
    </nav>
	<div  class="container">	
		<div class="row">
			<div class="col-xs-12">
				<?php
                $inst_admin = "";
                foreach ($groups as $group) {
                    if ($inst_admins == $group) { $inst_admin = "true"; }
                }
                if ($inst_admin == "true") {
                        // Show the table
                        $tbl_access_grps->showTable();
				        // Output number of rows returned
				        echo "Total returned rows: ";
				        $tbl_access_grps->insertRowsReturned();
				        echo "<br /><br />";
                }
                else {
                        echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;Unauthorised access!</h4>";
                        echo "<br />";
                        echo "<p>Sorry, you are not authorised to view this page.</p>";
                        echo "<br />";
                        echo "<p><a href='/$paths[1]' class='btn btn-primary active' role='button'>Dashboard</a></p>";
                    }   
				?>	
			</div>
		</div>
		<br /><br />       
    </div>

</body>
</html>