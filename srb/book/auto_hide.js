 /* Hides the fields based on selected radio option */

$(document).ready(function(){
        $(".ext").hide();
        $(".int").show();
        $('input[type="radio"]').click(function(){
            if($(this).attr("value")=="internal"){
                $(".ext").hide();
                $(".int").show();
            }
            if($(this).attr("value")=="external"){
                $(".int").hide();
                $(".ext").show();
            }
        });
});