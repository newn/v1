<?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../../includes/identity.php");

// Connect to database
include("../../includes/con_db.php");

// Get values for displaying texts on the page
include("../../includes/texts.php");

// Define person's access rights
include("../../includes/access.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>

<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $head_title ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
// Auto hide fields
echo "<script type='text/javascript' src='auto_hide.js'></script>";
?>
<!-- Local css -->
<link type="text/css" href="../css/local.css" rel="stylesheet" />
<!-- Load the date time picker -->
<script type="text/javascript" src="//mugifly.github.io/jquery-simple-datetimepicker/jquery.simple-dtpicker.js"></script>
<link type="text/css" href="//mugifly.github.io/jquery-simple-datetimepicker/jquery.simple-dtpicker.css" rel="stylesheet" />
</head>
<body>

<!-- Primary Page Layout
================================================== -->

<!-- This is the sidebar -->
	<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   		<a class="navmenu-brand" href="#"><?php echo $sidebar_brand ?></a>
      	<ul class="nav navmenu-nav">
			<?php
      		// Show the sidebar items
      		include("../../includes/sidebar-menu-items.php");
      		?>
      	</ul>
    </div>	
	
	<!-- This is the main navbar -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
			<span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
            <?php
			// Control the view of menu items for the logged person
			
			// Show the menu items
			include("../../includes/navbar-menu-items.php");
			
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
                // Show the right menu items
                include("../../includes/navbar-right-menu-items.php");           
                ?>
			</ul>
		</div>
		</div>
    </nav>
	<div  class="container">	
		<div class="row">
			<div class="col-xs-12">
				<?php
				// Email form to head porter
				//==================================================

				// Get values from form
				if(isset($_POST['book'])) {
					$msg_room = $_POST['room'];
					$msg_start_time = $_POST['start_time'];
					$msg_end_time = $_POST['end_time'];
					$msg_mobile = $_POST['mobile'];
					$msg_quantity = $_POST['quantity'];
					$msg_purpose = $_POST['purpose'];
					$msg_ext_title = $_POST['ext_title'];
					$msg_ext_speakers = $_POST['ext_speakers'];
					$msg_ext_contact = $_POST['ext_contact'];
					$msg_ext_org_details = $_POST['ext_org_details'];
					$msg_ext_purpose = $_POST['ext_purpose'];
					$msg_ext_qty_stud = $_POST['ext_qty_stud'];
					$msg_ext_qty_othcol = $_POST['ext_qty_othcol'];
					$msg_ext_qty_othuni = $_POST['ext_qty_othuni'];
					$msg_ext_qty_external = $_POST['ext_qty_external'];
	
					// Email notification
					//$to = "ev243@cam.ac.uk";
					$to = $submit_form_email;
					$cc = $submit_form_email_cc;
					$subj = $book_request_msg;
					if ($_POST['intext_radio'] == "internal") {
						$message = "<b>Request details: (INTERNAL)</b><br>Crsid: <i>".$crsid."</i><br>Name: <i>".$displayName."</i><br>Room: <i>".$msg_room."</i><br />Start: <i>".$msg_start_time."</i><br>
						End: <i>".$msg_end_time."</i><br>Mobile: <i>".$msg_mobile."</i><br>Attendees: <i>".$msg_quantity."</i><br>Purpose:<br><i>".nl2br($msg_purpose)."</i>";
					}
					if ($_POST['intext_radio'] == "external") {
						$message = "<b>Request details: (EXTERNAL)</b><br>Crsid: <i>".$crsid."</i><br>Name: <i>".$displayName."</i><br>Room: <i>".$msg_room."</i><br />Start: <i>".$msg_start_time."</i><br>
						End: <i>".$msg_end_time."</i><br>Mobile: <i>".$msg_mobile."</i><br>Proposed event: <i>".$msg_ext_title."</i><br>Speakers: <i>".$msg_ext_speakers."</i><br>
						Contact details: <i>".$msg_ext_contact."</i><br>Details of external organisation:<br><i>".nl2br($msg_ext_org_details)."</i><br>Proposed talk/activity details:<br><i>".nl2br($msg_ext_purpose)."</i><br>
						Attendees:<br><i>Student College members: ".$msg_ext_qty_stud."</i><br><i>Other College members: ".$msg_ext_qty_othcol."</i><br><i>Other University members: ".$msg_ext_qty_othuni."</i><br><i>External people: ".$msg_ext_qty_external."</i>";
					}
					$from = $emails[0];
					$headers = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= "From:" . $from . "\r\n";
					$headers .= "Cc:" . $cc . "\r\n";
					mail($to,$subj,$message,$headers);

					// Print summary of form
					Print "<label>".$book_submitted."</label>";
					Print "<p>".$book_req_msg."</p>";
					Print "<p>&nbsp;</p>";
					Print "<p><a href='../' class='btn btn-primary active' role='button'>".$srb_home."</a></p>";
	
					// Close the sql connection
					mysqli_close($con);
				}
				else { ?>
					<form name="bookForm" data-toggle="validator" class="form-horizontal" method="post" action="<?php $_PHP_SELF; ?>" role="form">
						<div class="form-group form-group-lg form-inline">
							<label class="col-sm-3 col-md-4 control-label"><?php echo $book_room; ?></label>
							<div class="col-sm-6 col-md-4">
							<?php
							$room_array = explode(', ', $rooms);
							echo '<select name="room" class="form-control" required>';
							echo '<option value="" selected>Please select...&nbsp;&nbsp;&nbsp;&nbsp;</option>';
							foreach ($room_array as $room) {
								echo '<option value="'.htmlspecialchars($room).'">'. htmlspecialchars($room) . "</option>\n";
							}
							echo '</select>';					
							?>						
							</div>
						</div>
						<div class="form-group form-group-lg form-inline datepciker">
							<label class="col-sm-3 col-md-4 control-label"><?php echo $date_picker ?></label>
							<div class="col-sm-6 col-md-4">
							<input type="text" name="start_time" id="start_time" class="form-control" style="width: 50%"><input type="text" name="end_time" id="end_time" class="form-control" style="width: 50%">
							<br /><i><b>Time Restrictions:</b><br><u>Bar</u>: 09:00-18:00 only<br><u>BBQ Area</u>: 12:00-20:00 only</i>
							</div>
						</div>
						<div class="form-group form-group-lg">
							<label class="col-sm-3 col-md-4 control-label"><?php echo $mobile ?></label>
							<div class="col-sm-6 col-md-4">
							<input type="text" name="mobile" class="form-control" placeholder="<?php echo $mobile_ph ?>" maxlength="15" required>
							</div>
						</div>
						<div class="form-group form-group-lg">
							<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
							<div class="col-sm-6 col-md-4 radio">
  							<label><input type="radio" name="intext_radio" id="intext_radio" value="internal" checked>Internal&nbsp;&nbsp;</label>
							<label><input type="radio" name="intext_radio" id="intext_radio" value="external">External</label>
							</div>
						</div>
						<div class="int">
							<div class="form-group">
								<label class="col-sm-3 col-md-4 control-label"><?php echo $quantity ?></label>
								<div class="col-sm-6 col-md-4">
								<input type="text" name="quantity" class="form-control" value="1" maxlength="2" style="width: 30%">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 col-md-4 control-label"><?php echo $book_purpose ?></label>
								<div class="col-sm-6 col-md-4">
								<textarea name="purpose" class="form-control" rows="3" style="height: auto;" placeholder="<?php echo $book_purpose_ph ?>"></textarea>
								</div>
							</div>
						</div>
						<div class="ext">
							<div class="form-group">
								<label class="col-sm-3 col-md-4 control-label"><?php echo $ext_title ?></label>
								<div class="col-sm-6 col-md-4">
								<input type="text" name="ext_title" class="form-control" placeholder="<?php echo $ext_title_ph ?>" maxlength="100">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 col-md-4 control-label"><?php echo $ext_speakers ?></label>
								<div class="col-sm-6 col-md-4">
								<input type="text" name="ext_speakers" class="form-control" placeholder="<?php echo $ext_speakers_ph ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 col-md-4 control-label"><?php echo $ext_contact ?></label>
								<div class="col-sm-6 col-md-4">
								<input type="text" name="ext_contact" class="form-control" placeholder="<?php echo $ext_contact_ph ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 col-md-4 control-label"><?php echo $ext_org_details ?></label>
								<div class="col-sm-6 col-md-4">
								<textarea name="ext_org_details" class="form-control" rows="3" style="height: auto;" placeholder="<?php echo $ext_org_details_ph ?>"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 col-md-4 control-label"><?php echo $ext_purpose ?></label>
								<div class="col-sm-6 col-md-4">
								<textarea name="ext_purpose" class="form-control" rows="3" style="height: auto;" placeholder="<?php echo $ext_purpose_ph ?>"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 col-md-4 control-label"><?php echo $ext_quantity ?></label>
								<div class="col-sm-6 col-md-4">
									<div class="form-group form-group-sm row">
                						<div class="col-xs-3 col-md-2">
                    						<input type="text" class="form-control" name="ext_qty_stud" value="0" maxlength="2">
                						</div>
                						<p><?php echo $ext_qty_stud ?></p>
                						<div class="col-xs-3 col-md-2">
                    						<input type="text" class="form-control" name="ext_qty_othcol" value="0" maxlength="2">
                						</div>
                						<p><?php echo $ext_qty_othcol ?></p>
                						<div class="col-xs-3 col-md-2">
                    						<input type="text" class="form-control" name="ext_qty_othuni" value="0" maxlength="2">
                						</div>
                						<p><?php echo $ext_qty_othuni ?></p>
                						<div class="col-xs-3 col-md-2">
                    						<input type="text" class="form-control" name="ext_qty_external" value="0" maxlength="2">
                						</div>
                						<p><?php echo $ext_qty_external ?></p>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group form-group-lg">
							<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
							<div class="col-sm-6 col-md-4">
								<div class="checkbox-inline"><input type="checkbox" value="" required> I've read and accept the <a href="#" data-toggle="modal" data-target="#tcModal">Terms and Conditions</a>.</div>
							</div>
						</div>
						<div class="form-group form-group-lg">
							<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
							<div class="col-sm-6 col-md-4">
							<button type="submit" name="book" id="book" class="btn btn-primary active"><?php echo $book_submit ?></button>
							</div>
						</div>
						<script type="text/javascript">
							$(function(){
			 					// -- Example Only - Set the date range --
								// var d = new Date();
								// d.setDate(10);
								// $('#start_time').val(d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " 1:00 pm");
								// Example Only - Set the end date to 7 days in the future so we have an actual 
								// d.setDate(d.getDate() + 7);
								// $('#end_time').val(d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " 13:45 ");
								// -- End Example Only Code --				
								// Attach a change event to end_time - 
								// NOTE we are using '#ID' instead of '*[name=]' selectors in this example to ensure we have the correct field
								$('#end_time').change(function() {
								    $('#start_time').appendDtpicker({
									    "futureOnly": true,
                				        "closeOnSelected": true,
                				        "dateFormat": "DD/MM/YYYY hh:mm",
                				        "minTime":"07:00",
                				        "maxTime":"23:00",
                				        //maxDate: $('#end_time').val() // when the end time changes, update the maxDate on the start field
								    });
								});		
								$('#start_time').change(function() {
								    $('#end_time').appendDtpicker({
									    "futureOnly": true,
                				        "closeOnSelected": true,
                				        "dateFormat": "DD/MM/YYYY hh:mm",
                				        "minTime":"07:00",
                				        "maxTime":"23:00",
                				        //minDate: $('#start_time').val() // when the start time changes, update the minDate on the end field
								    });
								});						
								// trigger change event so datapickers get attached
								$('#end_time').trigger('change');
								$('#start_time').trigger('change');
							});
						</script>
					</form>
				<?php
				}
				?>
			</div>
		</div>
		<br /><br />       
    </div>
	<!-- T&C Modal -->
    <div name="tcModal" id="tcModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Terms and Conditions</h4>
            </div>
            <div class="modal-body">
                <p>All rooms and meeting places of the College have a designated owner. In all cases, the designated owner has the responsibility of ensuring that the rooms and meeting places are used appropriately, and may specify in writing particular terms and conditions relating to the use of that room or meeting place, which may include terms and conditions relating to any particular meeting or activity, if appropriate. In light of this new booking forms have been prepared (both online and paper) which include the following information:</p>                     
            	<p>
            		<ol>
            			<li>No College event can take place in a room or meeting place without prior approval by the designated owner (or clearly-indicated nominated members of staff with delegated authority). Such decisions are made in the light of information provided by an event organiser (who must be a specific person, acting on their own behalf or on behalf of an organisation responsible for the College event) through mechanisms approved by the nominated members of staff.</li>
            			<li>The person booking the room or space must be present throughout the event.</li>
            			<li>Conditions apply to all room bookings for College events, including:
            				<ul>
            					<li>The College reserves the right to seek additional information before confirming a booking.</li>
            					<li>The College event organiser (i.e. the named person making the booking) agrees as a condition of submitting the room booking request to notify the College if any of the details submitted change.</li>
            					<li>The College reserves the right to review its decision on allowing a College event to proceed if any of the information provided changes.</li>
            					<li>The deliberate provision of false or incomplete information by the College event organiser may be addressed under the disciplinary procedures of the College, if appropriate, or otherwise invalidate the booking.</li>
            				</ul>	
            			</li>
            			<li>An initial room booking should be made through the published process, and will not be considered complete unless it includes the following information, as a minimum:
            				<ul>
            					<li>name and contact details of the College event organiser;</li>
            					<li>title of the proposed College event;</li>
            					<li>names and contact details of formal presenters or speakers at the College event, if any;</li>
            					<li>details of any College organisation or society represented or publicised at the event;</li>
            					<li>brief description of proposed talks and/or activities;</li>
            					<li>dates and times of the proposed College event;</li>
            					<li>if a College event at an external venue, details of the venue including contact information for the venue’s facilities manager or equivalent;</li>
            					<li>projected number of attendees, including:
            						<ul>
            							<li>student members of the College;</li>
            							<li>other members of the College;</li>
            							<li>other members of the University;</li>
            							<li>people external to the College and the University.</li>
            						</ul>
            					</li>
            				</ul>
            			</li>
            			<li>The designated owner (or clearly-indicated nominated members of staff with delegated authority) will use this information to assess the likelihood of a range of risks: this will include the assessment of risks specifically relating to the protection of freedom of speech and the Colleges’ responsibility in preventing crime (including the promotion of illegal discrimination or terrorism).</li>
            			<li>Activities likely to be considered inappropriate to be conducted on College premises or at events organised by the College at an external venue include:
            				<ul>
            					<li>internal or external speakers giving talks which directly or indirectly promote violence towards members of the College or the general public, or which may advance the radicalisation of College members (as it is defined in the College’s statement on freedom of speech);</li>
            					<li>internal or external speakers whose presence or activity, in the view of the College, carries a reasonable likelihood of risk to the health or safety of its members or of the general public;</li>
            					<li>physical activities where there has not been due regard for the safety of participants and onlookers;</li>
            					<li>activities where the College has been advised by the police that they represent a high risk at the specified time or location proposed.</li>
            				</ul>
            			</li>
            		</ol>
            	</p>
            	<p>The College is strongly committed to the principle of freedom of speech and expression and has published a statement to that effect: this is referenced in the booking form (online and paper) for those seeking to book College rooms for an internal event, or booking a venue other than at the College site for a College event; students who take the lead in managing College student societies (including but not limited to the JCR and MCR) will have this information brought to their attention.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>    
        </div>
        </div>
    </div>
</body>
</html>