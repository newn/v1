Porters Shift Schedule

http://itservices.newn.cam.ac.uk/cal_porters
*****

Software:
http://www.luxsoft.eu/index.php?pge=dtail
*****

Staff name List:
\htdocs\cal_porters\pages\eventform1.php
*****

Admin user:
admin / 4t****
*****

Single sign-on:
\htdocs\cal_porters\index.php
*****

The page is raven protected, the username is passed on to a variable
$lcUser = $_SERVER['AAPRINCIPAL'];
*****

To change wordings:
\htdocs\cal_porters\lang\ui-english.php
\htdocs\cal_porters\lang\ai-english.php
\htdocs\cal_porters\lang\ug-english.php
*****

Title change:
\htdocs\stud_rb\canvas\header1.php (STUDENT ROOMS BOOKING SYSTEM)
\htdocs\stud_rb\canvas\headerm.php
*****

Controlling the repetition of event/schedule:
\htdocs\cal_porters\pages\eventform1.php
*****

Request Holiday button:
\htdocs\cal_porters\canvas\header1.php
\htdocs\cal_porters\canvas\headerm.php
*****

Remove Help button:
\htdocs\cal_porters\canvas\header1.php -- "hdr_help"
\htdocs\cal_porters\canvas\headera.php
*****

Employee List:
\htdocs\cal_porters\employee.txt
\htdocs\cal_porters\pages\eventform1.php
\htdocs\cal_porters\pages\timesheet.php
*****

Timesheet:
\htdocs\cal_porters\pages\timesheet.php
This page is a modfied version of the search page to get a user's time shifts within a time period.

Permit page:
\htdocs\cal_porters\index.php
'23' => array ('pages/timesheet.php','a','a','a','1','0','Porters TimeSheet','y','')

\htdocs\cal_porters\canvas\header1.php
\htdocs\cal_porters\canvas\headerm.php
Where the button needs to be accesssed.

*****

Holiday Balance
\htdocs\cal_porters\pages\holiday_bal.php

Holiday Balance button:
\htdocs\cal_porters\canvas\header1.php
\htdocs\cal_porters\canvas\headerm.php

Permit page:
\htdocs\cal_porters\index.php
'24' => array ('pages/holiday_bal.php','a','a','a','1','0','Porters Holiday Balance','y','')
*****

Holiday Edit
\htdocs\cal_porters\pages\holiday_bal.php

Holiday Edit button:
\htdocs\cal_porters\canvas\header1.php
\htdocs\cal_porters\canvas\headerm.php

Permit page:
\htdocs\cal_porters\index.php
'25' => array ('pages/holiday_edit.php','a','a','a','1','0','Porters Holiday Balance','','')
*****
*****
*****
*****
*****
*****
*****
*****

NEW THEME:

\htdocs\test_luxcal\css\lctheme.php

\htdocs\test_luxcal\css\css.php
FROM: div.navBar {clear:both; padding:0px 10px; line-height:20px; background:".BGND3."; border:".BORD1.";}
TO: div.navBar {clear:both; padding:0px 10px; line-height:20px; background:".BGND3.";}





