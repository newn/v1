<?php

// Identify person accessing the page
include("../includes/identity.php");
// Connect to database
include("../includes/con_db.php");
// Define person's access rights
include("../includes/access.php");

// Get the path names
$paths = explode("/", $_SERVER['REQUEST_URI']);
  
// Extract strings from url
parse_str($_SERVER['QUERY_STRING']);

// Restrict access to this feature
//if (${$path."_admin"} == "true" || ${$path."_manager"} == "true") {
    
    // where clause
    /*$tbl_where = "";
    if (!empty($tbl_where)) {
        $tbl_where = $_POST['tbl_where'];
    }*/
    
    // filename for export
    $filename = 'export_'.$tbl.'_'.date('d-m-Y').'.xls';
     
    // retrive data which you want to export
    $query = "SELECT * FROM ".$tbl;
    /*if ($tbl_where == "") {
        $query = "SELECT * FROM ".$tbl;
    }
    else {
        $query = "SELECT * FROM ".$tbl." WHERE schedule='$tbl_where'";
    }*/
    $header = '';
    $data ='';
     
    $export = mysqli_query($con,$query) or die(mysqli_error($con));
     
    // extract the field names for header 
     
    while ($fieldinfo=mysqli_fetch_field($export))
    {
        $header .= $fieldinfo->name."\t";
    }
     
    // export data 
    while( $row = mysqli_fetch_row( $export ) )
    {
        $line = '';
        foreach( $row as $value )
        {                                            
            if ( ( !isset( $value ) ) || ( $value == "" ) )
            {
                $value = "\t";
            }
            else
            {
                $value = str_replace( '"' , '""' , $value );
                $value = '"' . $value . '"' . "\t";
            }
            $line .= $value;
        }
        $data .= trim( $line ) . "\n";
    }
    $data = str_replace( "\r" , "" , $data );
     
    if ( $data == "" )
    {
        $data = "\nNo Record(s) Found!\n";                        
    }
     
    // allow exported file to download forcefully
    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=".$filename."");
    header("Pragma: no-cache");
    header("Expires: 0");
    print "$header\n$data";

    // Close the db connection
    mysqli_close($con);

//}
//else {
//    die("You do not have access to use this feature.");
//}
 
?>