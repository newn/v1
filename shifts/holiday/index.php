<?php

header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");

// Identify person accessing the page
include("../../includes/identity.php");

// Connect to database
include("../../includes/con_db.php");

// Get values for displaying texts on the page
include("../../includes/texts.php");

// Define person's access rights
include("../../includes/access.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>

<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $head_title ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Load the date time picker -->
<script type="text/javascript" src="../date_time_picker/jquery.simple-dtpicker.js"></script>
<link type="text/css" href="../date_time_picker/jquery.simple-dtpicker.css" rel="stylesheet" />
</head>
<body>

<!-- Primary Page Layout
================================================== -->

<!-- This is the sidebar -->
	<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   		<a class="navmenu-brand" href="#"><?php echo $sidebar_brand ?></a>
      	<ul class="nav navmenu-nav">
			<?php
      		// Show the sidebar items
      		include("../../includes/sidebar-menu-items.php");
      		?>
      	</ul>
    </div>	
	
	<!-- This is the main navbar -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
			<span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
            <?php
			// Control the view of menu items for the logged person
			
			// Show the menu items
			include("../../includes/navbar-menu-items.php");
			
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
                // Show the right menu items
                include("../../includes/navbar-right-menu-items.php");           
                ?>
			</ul>
		</div>
		</div>
    </nav>
	<div  class="container">	
		<div class="row">
			<div class="col-xs-12">
				<?php
				// Email form to head porter
				//==================================================

				// Get values from form
				if(isset($_POST['req'])) {
					$start_date = $_POST['start_date'];
					$end_date = $_POST['end_date'];
					$paid = $_POST['paid'];
					$unpaid = $_POST['unpaid'];
	
					// Email notification
					//$to = "ev243@cam.ac.uk";
					$to = $req_form_email;
					$cc = $req_form_email_cc;
					$subj = $req_subj;
					$message = "
                		<table align='center' style='margin-top:40px'>
                		<tr><td align='center' colspan='2'><img border='0' height='60' src='http://apps.newn.cam.ac.uk/public/images/crest-white.png'></td></tr>
                		<tr><td colspan='2'>&nbsp;</td></tr>
                		<tr><td align='center' colspan='2'>NEWNHAM COLLEGE, CAMBRIDGE, CB3 9DF</td></tr>
                		<tr><td align='center' colspan='2'><strong>HOLIDAY REQUEST</strong></td></tr>
                		<tr><td align='center' colspan='2'><hr /><br /></td></tr>
                		<tr><td width='250px'>&nbsp;&nbsp;Name:</td><td><b>$displayName</b></td></tr>
                		<tr><td width='250px'>&nbsp;&nbsp;CrsID:</td><td><b>$crsid</b></td></tr>
                		<tr><td width='250px'>&nbsp;&nbsp;Employee No.:</td><td><b>&nbsp;</b></td></tr>
                		<tr><td width='250px'>&nbsp;&nbsp;Department:</td><td><b>Porters Lodge</b></td></tr>
                		<tr><td colspan='2'>&nbsp;</td></tr>
                		<tr><td width='250px'>&nbsp;&nbsp;Requested dates:</td><td><b>$start_date to $end_date</b></td></tr>
                		<tr><td colspan='2'>&nbsp;</td></tr>
                		<tr><td width='250px'>&nbsp;&nbsp;Number of paid days:</td><td><b>$paid</b></td></tr>
                		<tr><td width='250px'>&nbsp;&nbsp;Number of unpaid days:</td><td><b>$unpaid</b></td></tr>
                		<tr><td colspan='2'>&nbsp;<br /><br /><br /></td></tr>
                		<tr><td align='center' colspan='2'>________________________<br />&nbsp;Approved by Manager / HoD<br /><br /><br /></td>
                		<tr><td align='left' colspan='2'><div style='border:1px solid #000000; padding:0px; margin-left:10px; margin-right:10px;'><font size='2'>Additional section for HoD:</font><br /><br /><br /><br /><br /><br /><br /><br /></div></td>
                		</tr>
                		</table>
                	";
					$from = $emails[0];
					$headers = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= "From:" . $from . "\r\n";
					$headers .= "Cc:" . $cc . "\r\n";
					mail($to,$subj,$message,$headers);

					// Print summary of form
					Print "<label>".$req_submitted."</label>";
					Print "<p>".$req_msg."</p>";
	
					// Close the sql connection
					mysqli_close($con);
				}
				else { ?>
					<form name="reqForm" data-toggle="validator" class="form-horizontal" method="post" action="<?php $_PHP_SELF; ?>" role="form">
						<div class="form-group form-group-lg form-inline">
							<label class="col-sm-3 col-md-4 control-label"><?php echo $date_picker ?></label>
							<div class="col-sm-6 col-md-4">
							<input type="text" name="start_date" id="start_date" class="form-control" style="width: 50%"><input type="text" name="end_date" id="end_date" class="form-control" style="width: 50%">
							</div>
						</div>
						<div class="form-group form-group-lg">
							<label class="col-sm-3 col-md-4 control-label"><?php echo $paid ?></label>
							<div class="col-sm-6 col-md-4">
							<input type="text" name="paid" class="form-control" value="1" maxlength="3"  style="width: 30%" required>
							</div>
						</div>
						<div class="form-group form-group-lg">
							<label class="col-sm-3 col-md-4 control-label"><?php echo $unpaid ?></label>
							<div class="col-sm-6 col-md-4">
							<input type="text" name="unpaid" class="form-control" value="0" maxlength="3"  style="width: 30%" required>
							</div>
						</div>
						<div class="form-group form-group-lg">
							<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
							<div class="col-sm-6 col-md-4">
							<button type="submit" name="req" id="req" class="btn btn-primary active"><?php echo $req_submit ?></button>
							</div>
						</div>
						<script type="text/javascript">
							$(function(){
			 					// -- Example Only - Set the date range --
								// var d = new Date();
								// d.setDate(10);
								// $('#start_date').val(d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " 1:00 pm");
								// Example Only - Set the end date to 7 days in the future so we have an actual 
								// d.setDate(d.getDate() + 7);
								// $('#end_date').val(d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " 13:45 ");
								// -- End Example Only Code --				
								// Attach a change event to end_date - 
								// NOTE we are using '#ID' instead of '*[name=]' selectors in this example to ensure we have the correct field
								$('#end_date').change(function() {
								    $('#start_date').appendDtpicker({
									    "futureOnly": true,
                				        "closeOnSelected": true,
                				        "dateOnly": true,
                				        "dateFormat": "DD/MM/YYYY",
                				        //maxDate: $('#end_date').val() // when the end time changes, update the maxDate on the start field
								    });
								});		
								$('#start_date').change(function() {
								    $('#end_date').appendDtpicker({
									    "futureOnly": true,
                				        "closeOnSelected": true,
                				        "dateOnly": true,
                				        "dateFormat": "DD/MM/YYYY",
                				        //minDate: $('#start_date').val() // when the start time changes, update the minDate on the end field
								    });
								});						
								// trigger change event so datapickers get attached
								$('#end_date').trigger('change');
								$('#start_date').trigger('change');
							});
						</script>
					</form>
				<?php
				}
				?>
			</div>
		</div>
		<br /><br />       
    </div>

</body>
</html>