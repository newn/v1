<?php

$file = 'employee1.txt';
$searchfor = '=';

// the following line prevents the browser from parsing this as HTML.
// header('Content-Type: text/plain');

// get the file contents, assuming the file to be readable (and exist)
$contents = file_get_contents($file);
// escape special characters in the query
$pattern = preg_quote($searchfor, '/');
// finalise the regular expression, matching the whole line
$pattern = "/^.*$pattern.*\$/m";
// search, and store all matching occurences in $matches
if(preg_match_all($pattern, $contents, $matches)){
   echo "Found matches:\n";
   echo implode("\n", $matches[0]);
}
else{
   echo "No matches found";
}

$string = 'Paul McElroy = 30,
Light Blue = 12,
Lime Green = 13,
Sensei Gray = 14';


$string = explode("\n",$contents);
foreach($string as $row)
{
    preg_match('/^(\D+)\s=\s(\d+)/', trim($row), $matches);
    echo $matches[1];//Dark Green
    echo $matches[2];//11
    echo $matches[3];//No
    echo $matches[4];//20
}

?>