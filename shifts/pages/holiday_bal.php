<script type="text/javascript">
function pickvalue()
{
document.getElementById('schText').value = document.getElementById('schText1').value
}
</script>

<?php
/*
= text search script =

� Copyright 2009-2014 LuxSoft - www.LuxSoft.eu

This file is part of the LuxCal Web Calendar.

The LuxCal Web Calendar is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

The LuxCal Web Calendar is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the LuxCal 
Web Calendar. If not, see: http://www.gnu.org/licenses/.
*/

//sanity check
if (!defined('LCC')) { exit('not permitted ('.substr(basename(__FILE__),0,-4).')'); } //launch via script only

//initialize
$schText = (isset($_POST["schText"])) ? $_POST["schText"] : ""; //search text
$eF = isset($_POST['eF']) ? $_POST['eF'] : array(0); //field filter
if (isset($_POST['eF1'])) $eF[] = 1;
if (isset($_POST['eF2'])) $eF[] = 2;
if (isset($_POST['eF3'])) $eF[] = 3;
$catName = (isset($_POST["catName"])) ? $_POST["catName"] : "Holiday"; //category filter
$holYear = ($_POST["holYear"]);
if ($holYear == "2013-2014") {$fromDda = "2013-10-01"; $tillDda = "2014-09-30"; $file = file_get_contents('./employee_2013-14.txt');}
if ($holYear == "2014-2015") {$fromDda = "2014-10-01"; $tillDda = "2015-09-30"; $file = file_get_contents('./employee_2014-15.txt');}
if ($holYear == "2015-2016") {$fromDda = "2015-10-01"; $tillDda = "2016-09-30"; $file = file_get_contents('./employee_2015-16.txt');}
if ($holYear == "2016-2017") {$fromDda = "2016-10-01"; $tillDda = "2017-09-30"; $file = file_get_contents('./employee_2016-17.txt');}
if ($holYear == "2017-2018") {$fromDda = "2017-10-01"; $tillDda = "2018-09-30"; $file = file_get_contents('./employee_2017-18.txt');}
if ($holYear == "2018-2019") {$fromDda = "2018-10-01"; $tillDda = "2019-09-30"; $file = file_get_contents('./employee_2018-19.txt');}
/* functions */

function catList($selCat) {
	global $xx;
	
	$where = ' WHERE status >= 0'.($_SESSION['uid'] == 1 ? " AND public > 0" : '');
	$rSet = dbQuery("SELECT name, color, background FROM [db]categories{$where} ORDER BY sequence");
	if ($rSet !== false) {
		echo "<option value='*'>{$xx['sch_all_cats']}&nbsp;</option>\n";
		while ($row = mysql_fetch_assoc($rSet)) {
			$row["name"] = stripslashes($row["name"]);
			$selected = ($selCat == $row["name"]) ? " selected='selected'" : '';
			$catColor = ($row['color'] ? "color:".$row['color'].";" : '').($row['background'] ? "background-color:".$row['background'].";" : '');
			echo "<option value=\"{$row["name"]}\"".($catColor ? " style='{$catColor}'" : '')."{$selected}>{$row["name"]}</option>\n";
		}
	}
}

function searchForm() {
	global $xx, $set, $schText, $eF, $catName, $fromDda, $tillDda, $file;

	//Get the list of employees 
        $list = file('./employee1.txt');
        $options = '';
           foreach ($list as $row) {
              preg_match('/^(\D+)\s=\s(\d+)/', trim($row), $matches);
              $emp = $matches[1];
              $options .= '<option value="'.$emp.'">'.$emp.'</option>';
              }
              $select = '<select name="schText1" id="schText1" onchange="pickvalue()">'.$options.'</select>';

        echo "<form action='index.php?lc' method='post'>
		<table class='fieldBox'>
		<tr><td class='legend' colspan='2'>&nbsp;Define Report&nbsp;</td></tr>
		<tr>\n<td class='label'>Employee:</td>
		<td><input type='hidden' name='schText' id='schText' value=\"{$schText}\">
                {$select}
                </td>\n</tr>
		<tr><td colspan='2'><hr></td></tr>
		<input type='hidden' id='eF0' name='eF[]' value='1'>";

	// echo "<tr><td class='label'>{$xx['sch_event_cat']}:</td><td><select name='catName'>\n";
	// catList($catName);
	// echo "</select></td></tr>\n";
	echo "<tr>\n<td class='label'>Holiday Year:</td>
               <td><select name='holYear' id='holYear'>
               <option value=''>Select Year</option>
               <option value='2013-2014'>2013-2014</option>
               <option value='2014-2015'>2014-2015</option>
               <option value='2015-2016'>2015-2016</option>
               <option value='2016-2017'>2016-2017</option>
               <option value='2017-2018'>2017-2018</option>
               <option value='2018-2019'>2018-2019</option>
               </select></td></tr>";
        echo "</table>
		<input type='submit' name='search' value=\"{$xx['sch_search']}\">
		</form>
		<div style='clear:right'></div>
		<script>$I(\"schText\").focus();</script>";
}

function validateForm() {
	global $xx, $schText, $fromDda, $tillDda;
	
	$schText = trim(str_replace('%', '', $schText),'&');
	if (strlen(str_replace('_', '', $schText)) < 2) { return $xx['sch_invalid_search_text']; }
	if ($fromDda === false) { return $xx['sch_bad_start_date']; }
	if ($tillDda === false) { return $xx['sch_bad_end_date']; }
	return '';
}
	
function searchText() {
	global $xx, $set, $schText, $eF, $catName, $fromDda, $tillDda;
	
	//set event filter
	$schTextEsc = '%'.mysql_real_escape_string($schText).'%';
	$schTextEsc = str_replace('&', '%', $schTextEsc);
	//prepare description filter
	$filter = '';
	if ($catName != '*') { $filter .= " AND c.name = '".mysql_real_escape_string($catName)."'"; }
	$filter .= " AND (";
	if (in_array(0, $eF) or in_array(1, $eF)) { $filter .= "e.title LIKE '{$schTextEsc}'"; } //Title
	if (in_array(0, $eF) or in_array(2, $eF)) { $filter .= ((substr($filter, -1) == '(') ? '' : ' OR ')."e.venue LIKE '{$schTextEsc}'"; } //venue
	if (in_array(0, $eF) or in_array(3, $eF)) { $filter .= ((substr($filter, -1) == '(') ? '' : ' OR ')."e.description LIKE '{$schTextEsc}'"; } //description
	if (in_array(0, $eF) or in_array(4, $eF)) { $filter .= ((substr($filter, -1) == '(') ? '' : ' OR ')."e.xfield1 LIKE '{$schTextEsc}'"; } //extra field 1
	if (in_array(0, $eF) or in_array(5, $eF)) { $filter .= ((substr($filter, -1) == '(') ? '' : ' OR ')."e.xfield2 LIKE '{$schTextEsc}'"; } //extra field 2
	$filter = substr($filter,5).")";
	
	//set event date range
	$sDate = ($fromDda) ? $fromDda : date('Y-m-d',time()-31536000); //-1 year
	$eDate = ($tillDda) ? $tillDda : date('Y-m-d',time()+31536000); //+1 year

	retrieve($sDate,$eDate,'',$filter); //grab events

	//display header
	$fields = '';
	if (in_array(0, $eF) or in_array(1, $eF)) { $fields = ' + '.$xx['sch_title']; }
	foreach (str_split($set['evtTemplGen']) as $fieldNr) {
		if (strpos('1345',$fieldNr) !== false) {
			switch ($fieldNr) {
			case '1': 
				if (in_array(0, $eF) or in_array(2, $eF)) { $fields .= ' + '.$xx['sch_venue']; } break;
			case '3':
				if (in_array(0, $eF) or in_array(3, $eF)) { $fields .= ' + '.$xx['sch_description']; } break;
			case '4':
				if (in_array(0, $eF) or in_array(4, $eF)) { $fields .= ' + '.($set['xField1'] ? "{$set['xField1']}" : $xx['sch_extra_field1']); } break;
			case '5':
				if (in_array(0, $eF) or in_array(5, $eF)) { $fields .= ' + '.($set['xField2'] ? "{$set['xField2']}" : $xx['sch_extra_field2']); }
			}
		}
	}
	$fields = substr($fields,3);

	echo "<div class='subHead'>
		<form id='event' name='event' action='index.php?lc' method='post'>
		<input type='hidden' name='schText' value=\"{$schText}\">\n";
	foreach ($eF as $key => $value) { echo "<input type='hidden' name='eF[]' value=\"{$value}\">\n";	}
	echo "<input type='hidden' name='catName' value=\"{$catName}\">
		<input type='hidden' name='fromDda' value='".IDtoDD($fromDda)."'>
		<input type='hidden' name='tillDda' value='".IDtoDD($tillDda)."'>
		<input class='floatR noPrint' type='submit' name='newSearch' value=\"{$xx['sch_new_search']}\">
		</form>
		Employee: <b>{$schText}</b><br>
		{$xx['sch_event_cat']}: <b>".($catName != '*' ? $catName : $xx['sch_all_cats'])."</b><br>
		{$xx['sch_occurring_between']}: <b>".makeD($sDate,2)." - ".makeD($eDate,2)."</b>
		</div>\n";
}

function showMatches() {
	global $privs, $set, $xx, $evtList, $schText, $file;

	//display matching events
	echo '<div class="tsheet">'."\n";
	if ($evtList) {
		$match = '%('.str_replace(array('_','&'), array('.','[^<>]+?'), $schText).')(?![^<]*>)%i'; //convert to regex (?!: neg.look-ahead condition)
		$evtDone = array();
		$lastDate = '';
		echo "<table class='tsheet' width='150px'><tr><td><h6>Date/s of Holiday</h6></td></tr>";
                foreach($evtList as $date => &$events) {
			foreach ($events as $evt) {
				if (!$evt['mde'] or !in_array($evt['eid'],$evtDone)) { //!mde or mde not processed
					$evtDone[] = $evt['eid'];
					$evtDate = $evt['mde'] ? makeD($evt['sda'],5)." - ".makeD($evt['eda'],5) : makeD($date,5);
					$evtTime = $evt['ald'] ? $xx['vws_all_day'] : ITtoDT($evt['sti']).($evt['eti'] ? ' - '.ITtoDT($evt['eti']) : '');
                                        // echo $lastDate != $evtDate ? "<br><h6><a href='index.php?lc&amp;cP=2&amp;cD=$date' title=\"{$xx['sch_calendar']}\">{$evtDate}</a></h6>\n" : "<br>\n";
                                        echo "<tr>
						<td width='30%'>{$evtDate}</td>";
					echo "</tr>";
					$lastDate = $evtDate;
				}
                        }
                }
                echo "</table>";
                echo "<br /><br /><br />";
                //$contents = file_get_contents('./employee1.txt');
                $contents = $file;
                $pattern = preg_quote($schText, '/');
                $pattern = "/^.*$pattern.*\$/m";
                if(preg_match_all($pattern, $contents, $num)){
                $hol = implode("\n", $num[0]);
                }
                $string = explode("\n",$hol);
                foreach($string as $row)
                {
                  preg_match('/^(\D+)\s=\s(\d+)/', trim($row), $num1);
                  $holTotal = $num1[2];
                }
                $holUsed = count($evtList);
                $holBal = $holTotal - $holUsed;
                echo "<table class='tsheet' width='150px'>
                <tr><td>Hol+Lieu</td><td>Used</td><td>Balance</td></tr>
                <tr><td>$holTotal</td><td>$holUsed</td><td>$holBal</td></tr>
                </table>";
                echo "<br /><br />";
                // $test = $evt['cnm'];
                // echo $test;
                echo "<br />";

	} else {
		echo $xx['sch_no_results']."\n";
	}
	echo "</div>\n";
}

//control logic

$msg = ''; //init
if (isset($_POST["search"])) {
	$msg = validateForm();
}
echo "<p class='error'>{$msg}</p>\n";
if (isset($_POST["search"]) and !$msg) {
	searchText(); //search
	echo "<div class='scrollBoxSh'>\n";
	showMatches(); //show results
	echo "</div>\n";
} else {
	echo "<div class='scrollBoxAd'>
		<div class='centerBox'>\n";
	searchForm(); //define search
	echo "</div>\n</div>\n";
}
?>
