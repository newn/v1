<script type="text/javascript">
function pickvalue()
{
document.getElementById('schText').value = document.getElementById('schText1').value
}
</script>

<?php
/*
= text search script =

� Copyright 2009-2014 LuxSoft - www.LuxSoft.eu

This file is part of the LuxCal Web Calendar.

The LuxCal Web Calendar is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

The LuxCal Web Calendar is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the LuxCal 
Web Calendar. If not, see: http://www.gnu.org/licenses/.
*/

//sanity check
if (!defined('LCC')) { exit('not permitted ('.substr(basename(__FILE__),0,-4).')'); } //launch via script only


if ($privs < 2) { //block access to public
    echo "<p class='error'>You are not authorized to perform this action</p>"; exit;
}

// When update button is clicked, this function will update the record
if(isset($_POST['Update'])){ Update(); }

function Update()
    {
       $empName = $_POST['empName'];
       $empHol = $_POST['empHol'];
       $empNum = $_POST['empNum'];
       $content = $_POST['content'];
       //file_put_contents('./employee_2013-14.txt', str_replace($empName." = ".$empHol, $empName." = ".$empNum, file_get_contents('./employee_2013-14.txt')));
       file_put_contents($content, str_replace($empName." = ".$empHol, $empName." = ".$empNum, file_get_contents($content)));
}

function searchForm() {

	// Go to Holiday Year
	$holYear = ($_POST["holYear"]);
        if ($holYear == "2013-2014") {$file = file('./employee_2013-14.txt'); $content = './employee_2013-14.txt';}
        if ($holYear == "2014-2015") {$file = file('./employee_2014-15.txt'); $content = './employee_2014-15.txt';}
        if ($holYear == "2015-2016") {$file = file('./employee_2015-16.txt'); $content = './employee_2015-16.txt';}
        if ($holYear == "2016-2017") {$file = file('./employee_2016-17.txt'); $content = './employee_2016-17.txt';}
        if ($holYear == "2017-2018") {$file = file('./employee_2017-18.txt'); $content = './employee_2017-18.txt';}
        if ($holYear == "2018-2019") {$file = file('./employee_2018-19.txt'); $content = './employee_2018-19.txt';}
        echo "<form action='index.php?lc&cP=25' method='post'>
             <table class='fieldBox'>
             <tr>\n<td class='label'>Holiday Year:</td>
               <td><select name='holYear' id='holYear'>
               <option value='' selected>Select Year</option>
               <option value='2013-2014'>2013-2014</option>
               <option value='2014-2015'>2014-2015</option>
               <option value='2015-2016'>2015-2016</option>
               <option value='2016-2017'>2016-2017</option>
               <option value='2017-2018'>2017-2018</option>
               <option value='2018-2019'>2018-2019</option>
               </select>
               <input type='submit'>
               </td></tr>
             </table>
             </form><br /><br />";
        
        //Get the list of employees and their holiday totals
        // $list = file('./employee_2013-14.txt');
        $list = $file;
        $options = '';
           foreach ($list as $row) {
              preg_match('/^(\D+)\s=\s(\d+)/', trim($row), $matches);
              $emp = $matches[1];
              $empHol = $matches[2];
              $options .= "<tr><td>$emp</td><td>$empHol</td><td><form action='index.php?lc&cP=25' method='post'>&nbsp;&nbsp;&nbsp;&nbsp;<input type='hidden' name='content' value=\"{$content}\"><input type='hidden' name='empName' value=\"{$emp}\"><input type='hidden' name='empHol' value=\"{$empHol}\"><input type='text' size='2' maxlenght='2' name='empNum' value=''></td><td><input type='submit' name='Update' value=\"Update\" onclick=\"Update()\"></form></td></tr>";
              }
        echo "<table class='fieldBox'>
		<tr><td class='legend' colspan='4'>&nbsp;Edit Holiday Balance&nbsp;</td></tr>
		<tr>\n<td>Employee:</td><td>Total:</td><td>&nbsp;&nbsp;&nbsp;&nbsp;New Total:</td><td>&nbsp;</td></tr>
		{$options}
		<tr><td colspan='4'><hr></td></tr>";
        echo "</table>
		<div style='clear:right'></div>";
}

//control logic

echo "<div class='scrollBoxAd'>
           <div class='centerBox'>\n";
	   searchForm(); //define search
echo "</div>\n</div>\n";

?>
