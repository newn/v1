<?php
/*
= LuxCal event calendar admin interface language file =

La traduction française a été réalisée par Fabiou (fabiou.dec@gmail.com).
N'hésitez pas à lui faire part de vos remarques si vous constatez des erreurs ou des oublis dans la traduction.

© Copyright 2009-2014 LuxSoft - www.LuxSoft.eu
This file is part of the LuxCal Web Calendar.
*/

//LuxCal ui language file version
define("LAI","3.2.2");

/* -- Admin Interface texts -- */

$ax = array(

//general
"none" => "Aucun",
"all" => "Tous",
"back" => "Retour",
"close" => "Fermer",
"always" => "Toujours",
"at_time" => "@", //date and time separator (e.g. 11-09-2014 @ 10:45)
"year" => "Année",
"month" => "Mois",
"week_day" => "Semaine/Jour",
"upcoming" => "A venir",

//settings.php - En-tête de paragraphe + general
"set_general_settings" => "Calendrier",
"set_navbar_settings" => "Barre de navigation",
"set_event_settings" => "Evénements",
"set_user_settings" => "Utilisateurs",
"set_email_settings" => "Paramètres email",
"set_perfun_settings" => "Fonctions périodiques<br>(applicable uniquement si cron actif)",
"set_minical_settings" => "Mini Calendrier (applicable uniquement si utilisé)",
"set_sidebar_settings" => "Barre latérale autonome<br>(applicable uniquement si utilisé)",
"set_view_settings" => "Vues",
"set_dt_settings" => "Dates et Heures",
"set_save_settings" => "Sauver les paramètres",
"set_missing_invalid" => "Paramètres manquants ou invalides (fond accentué)",
"set_settings_saved" => "Sauvegarde effectuée",
"set_save_error" => "Erreur Base de données - Sauvegarde des paramètres du calendrier impossible",
"hover_for_details" => "Mettre le curseur sur le texte pour avoir les descriptions complètes",
"default" => "défaut",
"enabled" => "actif",
"disabled" => "inactif",
"no" => "non",
"yes" => "oui",
"or" => "ou",
"minutes" => "minutes",
"pixels" => "pixels",
"no_way" => "Vous n'avez pas les droits d'accès pour effectuer cette commande",

//settings.php - general Settings. Les apostrophes dans les traductions ..._text ci-dessous doivent être échappées par un antislash (\')
"calendarVersion_label" => "Calendar version",
"calendarVersion_text" => "Version of the installed calendar software.",
"calendarTitle_label" => "Titre du calendrier",
"calendarTitle_text" => "Affiche le nom du calendrier au dessus de la page et l\'utilise dans les notifications d\'email.",
"calendarUrl_label" => "URL du calendrier",
"calendarUrl_text" => "Adresse du calendrier de votre site Web. Ex: http://www.monsite.com/LuxCal/.",
"calendarEmail_label" => "Adresse email du calendrier",
"calendarEmail_text" => "Cette adresse email est utilisée pour transmettre et recevoir les emails de notification.<br>Format: \'adresse mail\' ou \'nom&#8249;adresse mail&#8250;\'. \'nom\' peut être le nom du calendrier.",
"backLinkUrl_label" => "Lien vers la page parent",
"backLinkUrl_text" => "URL de la page parent. Si spécifié, un bouton retour sera affiché à gauche de la barre de navigation et pointera sur cette URL.<br>Par exemple, pour retourner à la page qui a lancé le calendrier.",
"timeZone_label" => "Fuseau horaire",
"timeZone_text" => "Fuseau horaire du calendrier, utilisé pour calculer l\'heure.",
"see" => "voir",
"notifSender_label" => "Expéditeur des emails de notification",
"notifSender_text" => "Quand les emails de rappel sont envoyés, l\'adresse email expéditeur peut être celle du calendrier ou celle de l\'utilisateur qui a créé l\'événement.",
"rssFeed_label" => "Lien flux RSS",
"rssFeed_text" => "Si activé: pour les utilisateurs ayant au moins le droit de visibilité, un lien de flux RSS sera visible dans le pied de page du calendrier et sera ajouté dans l\'entête des pages HTML du calendrier.",
"calendar" => "calendrier",
"user" => "utilisateur",
"php_mail" => "courriel PHP",
"smtp_mail" => "courriel SMTP",

//settings.php - navigation bar settings. Les apostrophes dans les traductions ..._text ci-dessous doivent être échappées par un antislash (\')
"navButText_label" => "Texte sur boutons de navigation",
"navButText_text" => "Si activé: les boutons de la barre de navigation seront affichés avec du texte. Si désactivé, les boutons seront affichés avec des icônes.",
"navBar_label" => "Barre de navigation",
"navBar_text" => "Si activé: Un bouton [A faire] / [A venir] sera affiché sur la barre de navigation, et permettra d\'ouvrir la liste des événements [A faire] / [A venir].",
"navTodoList_label" => "Bouton [A faire]",
"navUpcoList_label" => "Bouton [A venir]",
"optionsPanel_label" => "Menu Options",
"optionsPanel_text" => "Affichage des menus dans le menu Options.<br>Le filtre utilisateurs peut être utilisé pour afficher seulement les événements créés par un ou plusieurs utilisateur(s).<br>Le filtre catégorie peut être utilisé pour afficher seulement les événements appartenant à une ou plusieurs catégorie(s).<br>Le menu langage est utilisé pour sélectionner le langage de l\'utilisateur. (applicable uniquement si plusieurs langages sont installés)",
"userMenu_label" => "Filtre utilisateur",
"catMenu_label" => "Filtre catégorie",
"langMenu_label" => "Menu langage",
"defaultView_label" => "Vue par défaut au démarrage",
"defaultView_text" => "Choix possible:<br>- Année<br>- Mois<br><br>- Mois de travail (Jours ouvrés)<br>- Semaine<br><br>- Semaine de travail (Jours ouvrés)<br>- Jour<br>- Prochain(s) événement(s)<br>- Modifications<br>Choix recommandé: Mois ou Prochain(s) événement(s).",
"language_label" => "Langage par défaut",
"language_text" => "Les fichiers ai-{langage}.php, ui-{langage}.php, ug-{langage}.php et ug-layout.png doivent être présents dans le répertoire lang/. {langage} = nom du langage à utiliser. Les noms des fichiers doivent être en minuscules !",

//settings.php - event settings. Les apostrophes dans les traductions ..._text ci-dessous doivent être échappées par un antislash (\')
"privEvents_label" => "Utiliser les événements privés",
"privEvents_text" => "Les événements privésPrivate events are only sont seulement visibles à l\'utilisateur qui a créé l\'événement.<br>Actif: Les utilisateurs peuvent créer des événements privés.<br>Défaut: lors de l\'ajout d\'événements, la case à cocher \'privé\' dans la fenêtre événement sera cochée par défaut.<br>Toujours: lors de l\'ajout d\'événements, ils seront systématiquement privés, la case à cocher \'privé\' dans la fenêtre événement ne sera pas affichée.",
"details4All_label" => "Afficher le détail des événements aux utilisateurs",
"details4All_text" => "Inactif: le détail de l\'événement est seulement visible par le créateur et les utilisateurs ayant un droit d\'accès \'Ajouter/Editer/Supprimer tous\'.<br>Actif: le détail de l\'événement est visible par le créateur et les autres utilisateurs.<br>Connecté: le détail de l\'événement est visible par le créateur et les utilisateurs qui sont connectés.",
"evtDelButton_label" => "Voir le bouton [Supprimer] dans la fenêtre événement",
"evtDelButton_text" => "Désactivé: le bouton [Supprimer] dans la fenêtre événement ne sera pas visible. Les utilisateurs avec un droit d\'édition ne pourront pas effacer les événements.<br>Actif: le bouton [Supprimer] dans la fenêtre événement sera visible à tous les utilisateurs.<br>Gestionnaire: le bouton [Supprimer] dans la fenêtre événement ne sera visible qu\'aux utilisateurs ayant un droit \'Gestionnaire\'.",
"eventColor_label" => "Couleur de l'événement basé sur",
"eventColor_text" => "Chaque événement, affiché dans les différentes vues, peut être associé à la couleur de son créateur ou à la couleur de la catégorie de l\'événement.",
"xField1_label" => "Nom du champ optionnel 2",
"xField2_label" => "Nom du champ optionnel 3",
"xFieldx_text" => "Name of an optional text field. If this field is included in the event template hereafter, the field will be added as a free format text field to the Event window form and to the events displayed in all calendar views and pages.<br>The specified name can be max. 15 characters long. E.g. \'Email address\', \'Website\', \'Phone number\'.",
"logged_in" => "connecté",
"manager_only" => "gestionnaire",
"owner_color" => "le créateur",
"cat_color" => "la catégorie",

//settings.php - user account settings. Les apostrophes dans les traductions ..._text ci-dessous doivent être échappées par un antislash (\')
"selfReg_label" => "S'enregistrer",
"selfReg_text" => "Permet aux utilisateurs de s\'enregistrer eux-mêmes et d\'accéder au calendrier.",
"selfRegPrivs_label" => "Droits enregistrement utilisateur",
"selfRegPrivs_text" => "Droits d\'accès au calendrier pour l\'enregistrement des utilisateurs par eux-mêmes.<br>- Voir: Seulement consulter<br>- Ajouter/Editer: Ajouter/Editer ses propres événements<br>- Ajouter/Editer tous: Ajouter/Editer tous les événements",
"selfRegNot_label" => "Notification enregistrement utilisateur",
"selfRegNot_text" => "Envoi d\'une notification aux adresses email du calendrier pour prévenir qu\'un nouvel utilisateur s\'est enregistré.",
"restLastSel_label" => "Restore last user selections",
"restLastSel_text" => "The last user session (the Option Panel settings) will be saved and when the user revisits the calendar later, the values will be restored.",
"cookieExp_label" => "'Se souvenir de moi', expiration du cookie en jours",
"cookieExp_text" => "Nombre de jours avant que le cookie lié à l\'option \'Se souvenir de moi\' (au moment de la connexion) expire.",
"view" => "voir",
"post_own" => "ajouter/éditer",
"post_all" => "ajouter/éditer tous",
"manager" => "post/gestionnaire",

//settings.php - Paramètres des vues. Les apostrophes dans les traductions ..._text ci-dessous doivent être échappées par un antislash (\')
"evtTemplGen_label" => "Event template - General views",
"evtTemplGen_text" => "The event fields to be displayed in the various calendar views can be specified by means of a sequence of numbers.<br>If a number is specified in the sequence, the corresponding field will be displayed.",
"evtTemplUpc_label" => "Event template - Upcoming view",
"evtTemplUpc_text" => "The event fields to be displayed in the Upcoming Events views can be specified by means of a sequence of numbers.<br>If a number is specified in the sequence, the corresponding field will be displayed.",
"popBoxFields_label" => "Hover box event fields",
"popBoxFields_text" => "The event fields to be displayed in the hover box with event details in the various calendar views can be specified by means of a sequence of numbers. <br>If a number is specified in the sequence, the corresponding field will be displayed.<br>If no fields are specified at all, no hover box will be displayed.",
"templFields_text" => "Meaning of the numbers:<br>1: Venue field<br>2: Event category field<br>3: Description field<br>4: Extra field 1 (see below)<br>5: extra field 2 (see below)<br>6: Email notification data (only if a notification has been requested)<br>7: Date/time added/edited and the associated user(s).<br>The order of the numbers determine the order of the displayed fields.",
"popBoxShow_label" => "Hover box with event details",
"popBoxShow_text" => "If enabled, a box with event details will be displayed when hovering an event in the selected views. The event details will not be displayed if no hover box event fields (see above) have been specified.",
"yearStart_label" => "Mois de début dans la vue Année",
"yearStart_text" => "L\'affichage de la vue Année commencera toujours par le mois dont la valeur aura été choisie (1 - 12) et le nombre de mois affiché dépendra toujours de la valeur encodée dans le nombre de colonnes et de rangées. Le changement d\'affichage se fera lors du passage du 1er jour du mois choisi de l\'année suivante.<br>La valeur 0 a une fonction particulière: le mois débutant l\'année sera fonction de la date du jour et tombera dans la première rangée des mois.",
"colsToShow_label" => "Colonnes affichées dans la vue Année",
"colsToShow_text" => "Nombre de mois affiché dans chaque rangée dans la vue Année.<br>Choix recommandé: 3 ou 4.",
"rowsToShow_label" => "Rangées affichées dans la vue Année",
"rowsToShow_text" => "Nombre de rangée de 4 mois affichée sur une année.<br>Choix recommandé: 4, ce qui permet d\'afficher 16 mois d\'affilée.",
"weeksToShow_label" => "Semaines affichées dans la vue Mois",
"weeksToShow_text" => "Nombre de semaine affichée par mois.<br>Choix recommandé: 10, qui affiche 2 mois 1/2.<br>The values 0 and 1 have a special meaning:<br>0: display exactly 1 month - blank leading and trailing days.<br>1: display exactly 1 month - display events on leading and trailing days.",
"workWeekDays_label" => "Jours ouvrables de la semaine",
"workWeekDays_text" => "Affichage des jours ouvrables de la semaine dans les vues \"Mois jours ouvrables\" et \"Semaine jours ouvrables\".<br>Indiquer le numéro de chaque jour qui doit être visible. Numéros valides :<br>1 = Lundi<br>2 = Mardi<br>....<br>7 = Dimanche<br>ex. 12345 : Lundi - Vendredi",
"lookaheadDays_label" => "Jours affichés des prochains événements",
"lookaheadDays_text" => "Nombre de jour à afficher dans la vue Prochain(s) événement(s), dans la liste \'A Faire\' et dans les RSS feeds.",
"dwStartHour_label" => "Heure de début dans les vues Jour et Semaine",
"dwStartHour_text" => "Choix de l\'heure du début d\'une journée d\'événement.<br>La valeur par défaut est 6 pour un début de journée à 6h, ce qui évite de gaspiller l\'espace reprenant les heures de la nuit.<br>The time picker, used to enter a time, will also start at this hour.",
"dwEndHour_label" => "End hour in Day/Week view",
"dwEndHour_text" => "Hour at which a normal day of events ends.<br>E.g., setting this value to 18 will avoid wasting space in Week/Day view for the quiet time between 18:00 and midnight.<br>The time picker, used to enter a time, will also end at this hour.",
"dwTimeSlot_label" => "Tranche horaire dans les vues Jour et Semaine",
"dwTimeSlot_text" => "Nombre de minutes dans la tranche horaire pour les vues Jour et Semaine. Cette valeur, ainsi que l\'heure de début et l\'heure de fin (voir ci-dessus) déterminera le nombre de lignes affiché dans les vues Jour et Semaine.",
"dwTsHeight_label" => "Hauteur de ligne de la tranche horaire",
"dwTsHeight_text" => "Hauteur d\'affichage de la tranche horaire dans les vues Jour et Semaine en pixels.",
"showLinkInMV_label" => "Afficher le lien URL dans la vue Mois",
"showLinkInMV_text" => "Si le choix est oui, le lien URL écrit dans la description de l\'événement est affiché dans la vue Mois",
"monthInDCell_label" => "Month in each day cell",
"monthInDCell_text" => "Display in month view the 3-letter month name for each day",

//settings.php - Paramètres des dates/heures. Les apostrophes dans les traductions ..._text ci-dessous doivent être échappées par un antislash (\')
"dateFormat_label" => "Format de date (jj mm aaaa)",
"dateFormat_text" => "Text string defining the format of event dates in the calendar views and input fields.<br>Possible characters: y = year, m = month and d = day.<br>Non-alphanumeric character can be used as separator and will be copied literally.<br>Examples:<br>y-m-d: 2013-10-31<br>m.d.y: 10.31.2013<br>d/m/y: 31/10/2013",
"dateFormat_expl" => "e.g. y.m.d: 2013.10.31",
"MdFormat_label" => "Format de date (jj mois)",
"MdFormat_text" => "Text string defining the format of dates consisting of month and day.<br>Possible characters: M = month in text, d = day in digits.<br>Non-alphanumeric character can be used as separator and will be copied literally.<br>Examples:<br>d M: 12 April<br>M, d: July, 14",
"MdFormat_expl" => "e.g. M, d: July, 14",
"MdyFormat_label" => "Format de date (dd mois aaaa)",
"MdyFormat_text" => "Text string defining the format of dates consisting of day, month and year.<br>Possible characters: d = day in digits, M = month in text, y = year in digits.<br>Non-alphanumeric character can be used as separator and will be copied literally.<br>Examples:<br>d M y: 12 April 2013<br>M d, y: July 8, 2013",
"MdyFormat_expl" => "e.g. M d, y: July 8, 2013",
"MyFormat_label" => "Format de date (mois aaaa)",
"MyFormat_text" => "Text string defining the format of dates consisting of month and year.<br>Possible characters: M = month in text, y = year in digits.<br>Non-alphanumeric character can be used as separator and will be copied literally.<br>Examples:<br>M y: April 2013<br>y - M: 2013 - July",
"MyFormat_expl" => "e.g. M y: April 2013",
"DMdFormat_label" => "Format de date (jour jj mois)",
"DMdFormat_text" => "Text string defining the format of dates consisting of weekday, day and month.<br>Possible characters: WD = weekday in text, M = month in text, d = day in digits.<br>Non-alphanumeric character can be used as separator and will be copied literally.<br>Examples:<br>WD d M: Friday 12 April<br>WD, M d: Monday, July 14",
"DMdFormat_expl" => "e.g. WD - d M: Mardi - 6 avril",
"DMdyFormat_label" => "Format de date (jour dd mois yyyy)",
"DMdyFormat_text" => "Text string defining the format of dates consisting of weekday, day, month and year.<br>Possible characters: WD = weekday in text, M = month in text, d = day in digits, y = year in digits.<br>Non-alphanumeric character can be used as separator and will be copied literally.<br>Examples:<br>WD d M y: Friday 13 April 2013<br>WD - M d, y: Monday - July 16, 2013",
"DMdyFormat_expl" => "e.g. WD, M d, y: Monday, July 16, 2013",
"timeFormat_label" => "Time format (hh mm)",
"timeFormat_text" => "Text string defining the format of event times in the calendar views and input fields.<br>Possible characters: h = hours, H = hours with leading zeros, m = minutes, a = am/pm (optional), A = AM/PM (optional).<br>Non-alphanumeric character can be used as separator and will be copied literally.<br>Examples:<br>h:m: 18:35<br>h.m a: 6.35 pm<br>H:mA: 06:35PM",
"timeFormat_expl" => "e.g. h:m: 22:35 and h:mA: 10:35PM",
"weekStart_label" => "Premier jour de la semaine",
"weekStart_text" => "Jour qui débute la semaine.",
"weekNumber_label" => "Afficher les n° des semaines",
"weekNumber_text" => "Permet d\'afficher ou non les numéros des semaines dans les vues Année, Mois et Semaine",
"time_format_us" => "12 heures AM/PM",
"time_format_eu" => "24 heures",
"sunday" => "Dimanche",
"monday" => "Lundi",
"time_zones" => "FUSEAUX HORAIRES",
"dd_mm_yyyy" => "jj-mm-aaaa",
"mm_dd_yyyy" => "mm-jj-aaaa",
"yyyy_mm_dd" => "aaaa-mm-jj",

//settings.php - email settings. Les apostrophes dans les traductions ..._text ci-dessous doivent être échappées par un antislash (\')
"mailServer_label" => "Serveur de mail",
"mailServer_text" => "PHP mail is suitable for unauthenticated mail in small numbers. For greater numbers of mail or when authentication is required, SMTP mail should be used.<br>Using SMTP mail requires an SMTP mail server. The configuration parameters to be used for the SMTP server must be specified hereafter. If mail is disabled, the Send mail section in the Event window will be suppressed.",
"smtpServer_label" => "SMTP server name",
"smtpServer_text" => "If SMTP mail is selected, the SMTP server name should be specified here. For example gmail SMTP server: smtp.gmail.com.",
"smtpPort_label" => "SMTP port number",
"smtpPort_text" => "If SMTP mail is selected, the SMTP port number should be specified here. For example 25, 465 or 587. Gmail for example uses port number 465.",
"smtpSsl_label" => "SSL (Secure Sockets Layer)",
"smtpSsl_text" => "If SMTP mail is selected, select here if the secure sockets layer (SSL) should be enabled. For gmail: enabled",
"smtpAuth_label" => "SMTP authentication",
"smtpAuth_text" => "If SMTP authentication is selected, the username and password specified hereafter will be used to authenticate the SMTP mail.",
"smtpUser_label" => "SMTP username",
"smtpUser_text" => "If SMTP mail is selected, specify here the SMTP user name. For gmail this is the part of your email address before the @.",
"smtpPass_label" => "SMTP password",
"smtpPass_text" => "If SMTP mail is selected, specify here the SMTP password.",

//settings.php - periodic function settings. Les apostrophes dans les traductions ..._text ci-dessous doivent être échappées par un antislash (\')
"cronSummary_label" => "Résumé du cron job à l'Administrateur",
"cronSummary_text" => "Envoi un résumé du cron job à l\'administrateur du calendrier.<br>Il faut seulement l\'activer si un cron job est activé pour le calendrier.",
"chgEmailList_label" => "Adresse email pour les modifications",
"chgEmailList_text" => "Adresse email pour recevoir les modifications faites aux événements.<br>Séparer les adresses email par des points-virgules.",
"chgNofDays_label" => "Jours précédents pour les modifications",
"chgNofDays_text" => "Nombre de jours précédents à afficher pour les événements modifiés.<br>Si la valeur est à 0, aucun e-mail reprenantles modifications ne sera envoyé.",
"icsExport_label" => "Exportation journalière des événements iCal",
"icsExport_text" => "Si activé: Tous les événements dans la fourchette -1 semaine jusqu\'à +1 an sera exporté au format iCalendar dans un fichier .ics dans le dossier \'files\'.<br>Le nom du fichier sera le nom du calendrier avec les blancs remplacés par des tirets bas. Les anciens fichiers seront écrasés par les nouveaux.",
"eventExp_label" => "Expiration des événements, en jours",
"eventExp_text" => "Nombre de jours après la date d\'expiration de l\'événement avant qu\'ìl soit effacé automatiquement.<br>Si zéro ou si pas de tâche cron active, aucun événement ne sera automatiquement effacé.",
"maxNoLogin_label" => "Nombre de jour max. entre 2 connexions",
"maxNoLogin_text" => "Si un utilisateur ne s\'est pas connecté dans l\'intervalle du nombre de jour, le compte de l\'utilisateur est automatiquement supprimé sauf si la valeur est à 0.",

//settings.php - mini calendar / side bar settings. Les apostrophes dans les traductions ..._text ci-dessous doivent être échappées par un antislash (\')
"miniCalView_label" => "Vue du mini calendrier",
"miniCalView_text" => "Possible views for the mini calendar are:<br>- Full Month<br>- Work Month *)<br>- Full Week<br>- Work Week *)<br>*) For work week days see on this page: Vues - Jours ouvrables de la semaine",
"miniCalPost_label" => "Ajouter dans le mini calendrier",
"miniCalPost_text" => "Seulement si le mini calendrier est utilisé!<br>Si vous choisissez actif, l\'utilisateur peut:<br>- ajouter un nouvel événement en cliquant sur le jour du calendrier<br>- éditer/supprimer un événement en cliquant sur l\'événement<br>Note: The access rights of the Public User will be applicable.",
"popFieldsMcal_label" => "Event fields - mini calendar hover box",
"popFieldsMcal_text" => "The event fields to be displayed in an overlay when the user hovers an event in the mini calendar can be specified by means of a sequence of numbers.<br>If no fields are specified at all, no hover box will be displayed..<br>If no fields are specified at all, no hover box will be displayed",
"mCalUrlFull_label" => "Mini cal URL to full calendar",
"mCalUrlFull_text" => "When clicking the month at the top of the mini calendar, to go to the full calendar, the user will be directed to this URL.<br>If not specified, the full calendar will open in a new window.<br>This URL is in particular useful when the full calendar is embedded in an existing user page.",
"popFieldsSbar_label" => "Event fields - sidebar hover box",
"popFieldsSbar_text" => "The event fields to be displayed in an overlay when the user hovers an event in the stand-alone sidebar can be specified by means of a sequence of numbers.<br>If no fields are specified at all, no hover box will be displayed.",
"showLinkInSB_label" => "Show links in sidebar",
"showLinkInSB_text" => "Display URLs from the event description as a hyperlink in the upcoming events sidebar",
"sideBarDays_label" => "Days to look ahead in sidebar",
"sideBarDays_text" => "Number of days to look ahead for events in the sidebar.",

//login.php
"log_log_in" => "Connexion",
"log_remember_me" => "Connexion auto",
"log_register" => "S'enregistrer",
"log_change_my_data" => "Modifier mes données",
"log_change" => "Modifier",
"log_un_or_em" => "Nom d’utilisateur ou adresse email",
"log_un" => "Nom d’utilisateur",
"log_em" => "Adresse email",
"log_pw" => "Mot de passe",
"log_new_un" => "Nouveau nom d’utilisateur",
"log_new_em" => "Nouveau email",
"log_new_pw" => "Nouveau mot de passe",
"log_pw_msg" => "Voici votre détails pour vous connecter à",
"log_pw_subject" => "Votre %% mot de passe", //%% will be replaced by calendar name
"log_npw_subject" => "Votre nouveau %% mot de passe", //%% will be replaced by calendar name
"log_npw_sent" => "Votre nouveau mot de passe a été envoyé",
"log_registered" => "Enregistrement réussi - Votre mot de passe a été envoyé par email",
"log_not_registered" => "Enregistrement non réussi",
"log_un_exists" => "Nom d’utilisateur existe déjà",
"log_em_exists" => "Adresse email existe déjà",
"log_un_invalid" => "Nom d’utilisateur non valide (min 2 caractères : A-Z, a-z, 0-9, and _-.) ",
"log_em_invalid" => "Adresse email non valide",
"log_un_em_invalid" => "Nom d’utilisateur/adresse email non valide",
"log_un_em_pw_invalid" => "Nom d'utilisateur/adresse email ou mot de passe non valide",
"log_no_un_em" => "Entrez votre nom d'utilisateur/adresse email",
"log_no_un" => "Entrez votre nom d'utilisateur",
"log_no_em" => "Entrez votre adresse email",
"log_no_pw" => "Entrez votre mot de passe",
"log_no_rights" => "Connexion refusé : Vous n'avez pas les droits d'accès - Contacter l'Adminitrateur.",
"log_send_new_pw" => "Envoyer nouveau mot de passe",
"log_if_changing" => "Uniquement en cas de modification",
"log_no_new_data" => "Aucunes données à modifier",
"log_invalid_new_un" => "Nouveau nom d'utilisateur non valide (min 2 caractères : A-Z, a-z, 0-9, and _-.) ",
"log_new_un_exists" => "Nouveau nom d'utilisateur existe déjà",
"log_invalid_new_em" => "Nouvelle adresse email non valide",
"log_new_em_exists" => "Nouvelle adresse email existe déjà",
"log_ui_language" => "Interface langue de l'utilisateur",
"log_new_reg" => "Enregistrement nouvel utilisateur",
"log_date_time" => "Date / heure",

//categories.php
"cat_list" => "Liste des Catégories",
"cat_edit" => "Editer",
"cat_delete" => "Supprimer",
"cat_add_new" => "Ajouter une nouvelle catégorie",
"cat_add" => "Ajouter",
"cat_edit_cat" => "Editer la catégorie ",
"cat_name" => "Nom de la catégorie",
"cat_sequence" => "Ordre d'affichage",
"cat_in_menu" => "dans le menu",
"cat_text_color" => "Couleur du texte",
"cat_background" => "Couleur du fond",
"cat_select_color" => "Choix de la couleur",
"cat_save" => "Sauver les modifications",
"cat_added" => "Ajouter",
"cat_updated" => "Sauvegarde effectuée",
"cat_deleted" => "Supprimer",
"cat_invalid_color" => "Format de couleur non valide (#XXXXXX - X=valeur-HEX)",
"cat_not_added" => "Ajout annulé",
"cat_not_updated" => "Sauvegarde annulée",
"cat_not_deleted" => "Suppression annulée",
"cat_nr" => "#",
"cat_repeat" => "Périodicité",
"cat_every_day" => "chaque jour",
"cat_every_week" => "chaque semaine",
"cat_every_month" => "chaque mois",
"cat_every_year" => "chaque année",
"cat_approve" => "Les événements nécessitent une approbation",
"cat_public" => "Public",
"cat_check_mark" => "Case à cocher",
"cat_label" => "étiquette",
"cat_mark" => "texte",
"cat_name_missing" => "Le nom de la catégorie est manquant",
"cat_mark_label_missing" => "Etiquette manquante",

//users.php
"usr_list_of_users" => "Liste des utilisateurs",
"usr_id" => "User ID",
"usr_name" => "Nom d'utilisateur",
"usr_email" => "Adresse email",
"usr_password" => "Mot de passe",
"usr_rights" => "Droits",
"usr_edit_user" => "Edition du profil",
"usr_add" => "Ajout nouvel utilisateur",
"usr_edit" => "Editer",
"usr_delete" => "Supprimer",
"usr_view" => "Voir",
"usr_post_own" => 'ajouter/éditer',
"usr_post_all" => 'ajouter/éditer tout',
"usr_manager" => "gestionnaire",
"usr_admin" => "Admin",
"usr_login_0" => "Premier login",
"usr_login_1" => "Dernier login",
"usr_login_cnt" => "Connexions",
"usr_add_profile" => "Ajouter",
"usr_upd_profile" => "Sauver le profil",
"usr_not_found" => "Utilisateur non trouvé",
"usr_if_changing_pw" => "A préciser seulement si vous voulez changer de mot de passe",
"usr_admin_rights" => "Fonctions d'administrateur",
"usr_no_rights" => "Pas de droits d'accès",
"usr_view_calendar" => "Voir le Calendrier",
"usr_post_events_own" => "Ajout/Editer",
"usr_post_events_all" => "Ajout/Editer Tous",
"usr_manager_rights" => "Ajout/Editer + gestionnaire",
"usr_pw_not_updated" => "Sauvegarde du mot de passe annulée",
"usr_added" => "Ajouter",
"usr_updated" => "Sauvegarde effectuée",
"usr_deleted" => "Supprimer",
"usr_not_added" => "Ajout annulé",
"usr_not_updated" => "Modification annulée",
"usr_not_deleted" => "Suppression annulée",
"usr_cred_required" => "Nom d'utilisateur/adresse email et mot de passe obligatoire",
"usr_uname_exists" => "Nom d’utilisateur déjà existant",
"usr_email_exists" => "Adresse email existe déjà",
"usr_un_invalid" => "Nom d'utilisateur non valide (min 2 caractères : A-Z, a-z, 0-9, et _-.) ",
"usr_em_invalid" => "Adresse email non valide",
"usr_cant_delete_yourself" => "Vous ne pouvez pas vous supprimer",
"usr_background" => "Couleur du Fond",
"usr_select_color" => "Sélectionner la Couleur",
"usr_invalid_color" => "Format de la couleur invalide (#XXXXXX - X = HEX-valeur)",

//database.php
"mdb_dbm_functions" => "Choix des fonctions",
"mdb_noshow_tables" => "Pas d'accès aux tables",
"mdb_noshow_restore" => "Cannot find backup file",
"mdb_compact" => "Compacter la base de données",
"mdb_compact_table" => "Compactage de la table",
"mdb_compact_error" => "Erreur",
"mdb_compact_done" => "Terminé",
"mdb_purge_done" => "Suppression définitive des événements effacés",
"mdb_repair" => "Vérifier et réparer la base de données",
"mdb_check_table" => "Vérif. table",
"mdb_ok" => "OK",
"mdb_nok" => "Pas OK",
"mdb_nok_repair" => "Pas OK, réparer",
"mdb_backup" => "Sauvegarder la base de données",
"mdb_backup_table" => "Sauvegarde de la table",
"mdb_backup_file" => "Fichier de sauvegarde",
"mdb_backup_done" => "Terminé",
"mdb_records" => "records",
"mdb_restore" => "Restore database",
"mdb_restore_table" => "Restore table",
"mdb_inserted" => "records inserted",
"mdb_db_restored" => "Database restored.",
"mdb_run_upgrade" => "Warning:<br>Backup file didn't match calendar version.<br>Run upgradexxx.php script.",
"mdb_events" => "Events",
"mdb_delete" => "delete",
"mdb_undelete" => "undelete",
"mdb_between_dates" => "occurring between",
"mdb_deleted" => "Events deleted",
"mdb_undeleted" => "Events undeleted",
"mdb_file_saved" => "Fichier sauvegardé.",
"mdb_file_name" => "Nom du fichier:",
"mdb_start" => "Démarrer",
"mdb_no_function_checked" => "Aucune fonction sélectionnée",
"mdb_write_error" => "Erreur d'écriture du fichier de sauvegarde.<br>Vérifier les permissions du répertoire 'files/'",

//import/export.php
"iex_file" => "Sélectionner un fichier",
"iex_file_name" => "Nom du fichier iCal",
"iex_file_description" => "Description du fichier iCal",
"iex_filters" => "Filtres",
"iex_upload_ics" => "Importer un fichier iCal",
"iex_create_ics" => "Créer le fichier iCal",
"iex_upload_csv" => "Importer un fichier CSV",
"iex_upload_file" => "Importer le fichier",
"iex_create_file" => "Exporter le fichier",
"iex_download_file" => "Charger le fichier",
"iex_fields_sep_by" => "Champs séparés par",
"iex_birthday_cat_id" => "ID de la Catégorie",
"iex_default_cat_id" => "ID de la Catégorie par défaut",
"iex_if_no_cat" => "Laisser vide si la catégorie n'existe pas",
"iex_import_events_from_date" => "Importer les événements en date du",
"iex_see_insert" => "voir liste à droite",
"iex_no_file_name" => "Nom de fichier manquant",
"iex_inval_field_sep" => "Séparateur de champs invalide ou inexistant",
"iex_no_begin_tag" => "Fichier iCal invalide (DEBUT-vide)",
"iex_date_format" => "Format date événement",
"iex_time_format" => "Format heure événement",
"iex_number_of_errors" => "Nombre d'erreur dans la liste",
"iex_bgnd_highlighted" => "fond accentué",
"iex_verify_event_list" => "Vérifier la liste des événements, corriger les erreurs et cliquer",
"iex_add_events" => "Ajouter les événements dans la base de données",
"iex_select_ignore_birthday" => "Cocher la case \"ID Anniversaire\" et \"Supprimer\" si nécessaire",
"iex_select_ignore" => "Cocher la case \"Supprimer\" pour ignorer un événement",
"iex_title" => "Titre",
"iex_venue" => "Lieu",
"iex_owner" => "Utilisateur",
"iex_category" => "Catégorie",
"iex_date" => "Date début",
"iex_end_date" => "Date fin",
"iex_start_time" => "Heure début",
"iex_end_time" => "Heure fin",
"iex_description" => "Description",
"iex_repeat" => "Répéter",
"iex_birthday" => "ID Anniversaire",
"iex_ignore" => "Supprimer",
"iex_events_added" => "Evénements ajoutés",
"iex_events_dropped" => "Evénements sautés (déjà présents)",
"iex_db_error" => "Erreur de base de données",
"iex_ics_file_error_on_line" => "Erreur fichier iCal ligne",
"iex_between_dates" => "Occurence entre",
"iex_changed_between" => "Ajouté/modifié entre",
"iex_select_date" => "Sélectionner la date",
"iex_select_start_date" => "Sélectionner la date du début",
"iex_select_end_date" => "Sélectionner la date de fin",
"iex_all_cats" => "toutes",
"iex_all_users" => "tous",
"iex_no_events_found" => "Pas d'événement trouvé",
"iex_file_created" => "Fichier créé",
"iex_write error" => "Erreur d'écriture du fichier d'export.<br>Vérifier les permissions du répertoire 'files/'",

//lcalcron.php
"cro_sum_header" => "RESUME DU CRON JOB",
"cro_sum_trailer" => "FIN DU RESUME",
"cro_evc_sum_title" => "EVENTS EXPIRED",
"cro_nr_evts_deleted" => "Number of events deleted",
"cro_not_sum_title" => "RAPPELS D'EMAIL",
"cro_not_sent_to" => "Rappels envoyés à",
"cro_no_not_dates_due" => "Pas de notification de date du",
"cro_all_day" => "Toute la journée",
"cro_mailer" => "Notification",
"cro_subject" => "Sujet",
"cro_event_due_in" => "Le prochain événement est dans",
"cro_event_due_today" => "The following event is due today",
"cro_due_in" => "est dans",
"cro_due_today" => "Dûs pour aujourd'hui",
"cro_days" => "Jour(s)",
"cro_date_time" => "Date / heure",
"cro_title" => "Titre",
"cro_venue" => "Lieu",
"cro_description" => "Description",
"cro_category" => "Catégorie",
"cro_status" => "Status",
"cro_open_calendar" => "Ouvrir calendrier",
"cro_chg_sum_title" => "MODIFICATION DANS LE CALENDRIER",
"cro_nr_changes_last" => "Nombre de modification",
"cro_report_sent_to" => "Rapport envoyé à",
"cro_no_report_sent" => "Pas de rapport envoyé",
"cro_usc_sum_title" => "VERIFICATION DU COMPTE UTILISATEUR",
"cro_nr_accounts_deleted" => "Nombre de compte utilisateur supprimé",
"cro_no_accounts_deleted" => "Pas de compte utilisateur supprimé",
"cro_ice_sum_title" => "EXPORTED EVENTS",
"cro_nr_events_exported" => "Nombre d'événements exportés dans le format iCalendar dans le fichier",

//explanations
"xpl_manage_db" =>
"<h4>Instructions pour gérer la Base de données</h4>
<p>Sur cette page, les fonctions suivantes peuvent être choisies :</p>
<h5>Vérifier et réparer la base de données</h5>
<p>Cette fonction permet de scanner le calendrier, de vérifier les incohérences et de les réparer.</p>
<p>Si vous ne remarquez pas d'incohérence dans votre calendrier, cette fonction
peut être lancée une seule fois par an.</p>
<h5>Compacter la base de données</h5>
<p>Lorsqu'un utilisateur efface un événement, il est marqué 'effacé', mais il
n'est pas supprimé de la base de données. La fonction 'Compacter la base de données'
le supprime physiquement et libére l'espace qu'il occupait.</p>
<h5>Sauvegarder la base de données</h5>
<p>Cette fonction permet de créer une sauvegarde de toute la base de données (tables, 
structures et données) dans un format '.sql'. La sauvegarde est enregistrée dans le 
répertoire <strong>files/</strong> avec comme nom : 
<kbd>cal-backup-aaammjj-hhmmss.sql</kbd> (où 'aaaammjj' = année, mois, et 
jour, et hhmmss = heure, minutes et secondes.</p>
<p>Ce fichier de sauvegarde peut être utilisé pour recréer les tables, les structures et les données 
de la base de données, via the restore function described below or by using for instance the 
<strong>phpMyAdmin</strong> tool, which is provided by most web hosts.</p>
<h5>Restore database</h5>
<p>The restore function will use the latest backup file in the 'files' folder 
created by the back up function described above. Note that when using multiple 
calendars sharing the same database, the latest created backup file will be used 
irrespective of the source calendar.</p>
<p>When restoring the database, ALL CURRENTLY PRESENT DATA WILL BE LOST!</p>
<p>After restoring a backup file of a previous LuxCal version, you must run the 
latest upgradexxx.php script to ensure the database tables are up to date!</p>
<h5>Events</h5>
<p>This function will delete or undelete events which are occurring between the 
specified dates. If a date is left blank, there is no date limit; so if both 
dates are left blank, ALL EVENTS WILL BE DELETED!</p><br>
<p>IMPORTANT: When the database is compacted (see above), the events which are 
permanently removed from the database cannot be undeleted anymore!</p>",

"xpl_import_csv" =>
"<h4>Instructions pour importer un fichier CSV</h4>
<p>Ce formulaire est utilisé pour importer dans votre calendrier LuxCal, un fichier 
<strong>CSV</strong> (Comma Separated Values) contenant des événements créés par 
un autre calendrier comme par ex. MS Outlook.</p>
<p>L'ordre des colonnes dans le fichier CSV doit être impérativement comme suit : 
titre, lieu, id catégorie (voir ci-dessous), date début, date fin, heure début, 
heure fin et description. La 1ère ligne du fichier CSV, utilisée par le nom des 
colonnes, est ignorée.</p>
<h5>Exemples de fichier CSV</h5>
<p>Des exemples de fichier CSV se trouvent dans le répertoire 'files/' de LuxCal.</p>
<h5>Format de date et heure</h5>
<p>Le format de la date et de l'heure de l'événement côté gauche doit être le même 
que le format de la date et de l'heure du fichier CSV.</p>
<h5>Liste des Catégories</h5>
<p>Le calendrier utilise un numéro d'identification (ID) spécifique à chaque 
catégorie. Les ID des catégories dans le fichier CSV doivent correspondre aux 
catégories utilisées dans le calendrier LuxCal ou à défaut être vide.</p>
<p>Si dans la prochaine étape, vous voulez affecter des événements en tant que 
\"anniversaire\", la catégorie <strong>ID Anniversaire</strong> doit être 
identique à l'ID de la catégorie ci-dessous.</p>
<p class='hilight'>Attention: Do not import more than 100 events at a time!</p>
<p>Voici les catégories actuellement définies dans votre calendrier:</p>",

"xpl_import_ical" =>
"<h4>Instructions pour importer un fichier iCalendar</h4>
<p>Ce formulaire est utilisé pour importer dans votre calendrier LuxCal, un fichier 
<strong>iCalendar</strong> contenant des événements.</p>
<p>Le contenu du fichier à importer doit rencontrer le standard de l'Internet 
Engineering Task Force 
[<u><a href='http://tools.ietf.org/html/rfc5545' target='_blank'>RFC5545 standard</a></u>].</p>
<p>Seuls les événements sont importés; les autres composants iCal (A faire, 
Jounal, Libre/Occupé, Alarme et zone de temps) sont ignorés.</p>
<h5>Exemples de fichier iCal</h5>
<p>Des exemples de fichier iCalendar se trouvent dans le répertoire 'files/' de 
LuxCal.</p>
<h5>Liste des Catégories</h5>
<p>Le calendrier utilise un numéro d'identification (ID) spécifique à chaque 
catégorie. Les ID des catégories dans le fichier iCalendar doivent correspondre 
aux catégories utilisées dans le calendrier LuxCal ou à défaut être vide.</p>
<p class='hilight'>Attention: Do not import more than 100 events at a time!</p>
<p>Voici les catégories actuellement définies dans votre calendrier:</p>",

"xpl_export_ical" =>
"<h4>Instructions pour exporter vers un fichier iCalendar</h4>
<p>Ce formulaire est utilisé pour extraire et exporter les événements du calendrier 
LuxCal dans un fichier <strong>iCalendar</strong>.</p>
<p>Le <b>nom du fichier iCal</b> (sans extension) est facultatif. Le fichier créé est sauvegardé 
dans le répertoire \"files/\" du serveur avec un nom de fichier spécifique 
ou avec le nom du calendrier. L'extension du fichier sera <b>.ics</b>.
Si un fichier existe déjà dans le répertoire \"files/\" du serveurr avec le même nom, il sera 
écrasé par le nouveau fichier.</p>
<p>La <b>description du fichier iCal</b> (ex. 'Réunions 2013') est facultative. 
Si elle existe, elle sera ajoutée à l'en-tête du fichier iCal exporté.</p>
<p><b>Filtres</b>: Les événements à extraire peuvent être filtrés par:</p>
<ul>
<li>Propriétaire</li>
<li>Catégorie</li>
<li>Date début</li>
<li>Date d'ajout/modification</li>
</ul>
<p>Chaque filtre est facultatif. Une date vide = aucun filtre</p>
<br>
<p>Le contenu du fichier à exporter doit rencontrer le standard de l'Internet 
Engineering Task Force 
[<u><a href='http://tools.ietf.org/html/rfc5545' target='_blank'>RFC5545 standard</a></u>]</p>. 
<p>Lorsqu'on <b>télécharge</b> le fichier ical exporté, la date et l'heure sont ajoutées au nom 
du fichier téléchargé.</p>
<h5>Exemples de fichier iCal</h5>
<p>Des exemples de fichier iCalendar se trouvent dans le répertoire 'files/' de 
LuxCal.</p>"
);
?>
